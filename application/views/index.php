
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="<?= base_url(); ?>assets/img/cropped-1-2-32x32.png" sizes="32x32">
    <link rel="icon" href="<?= base_url(); ?>assets/img/cropped-1-2-192x192.png" sizes="192x192">
    <link rel="apple-touch-icon" href="<?= base_url(); ?>assets/img/cropped-1-2-180x180.png">
    <title class="font-mono">Rutan Kelas I Bandung</title>
    <link rel="stylesheet" href="<?= base_url(); ?>assets/index/css/tailwind.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/index/css/animate.min.css">
    <script src="<?= base_url(); ?>assets/index/js/wow.min.js"></script>
    <script>new WOW().init();</script>
    <style>
        html {
            scroll-behavior: smooth;
        }
    </style>
</head>
<body>
<header class="lg:px-8 flex items-center w-full justify-between flex-wrap bg-white fixed w-full z-10 top-0 py-5 mx-auto md:flex-row " id="navBar2">
    
    <div class="flex items-center flex-shrink-0 text-white mr-6">
    	<a href="#"
    		class="relative left-0 z-10 flex lg:items-center lg:leading-none select-none ml-4"><img src="<?= base_url(); ?>assets/img/rutan-bdg.svg" style="height: 25%;"></a>
    </div>
        
        
    <div class="block lg:hidden">
    	<button
    		class="flex items-right px-3 py-2 border rounded text-indigo-600 border-indigo-600 hover:indigo-700 hover:border-indigo-700 mr-4"
    		id="navbar-btn">
    		<svg class="fill-current h-3 w-3" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
    			<title>Menu</title>
    			<path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z" />
    		</svg>
    	</button>
    </div>
    <div class="w-full block flex-grow lg:flex lg:items-center lg:w-auto block lg:hidden" id="navbar">
    	<div class="text-sm lg:flex-grow  text-center lg:text-right">
    		<a href="<?php echo base_url(); ?>"
    			class="block mt-4 lg:inline-block lg:mt-0 text-gray-700 hover:text-indigo-600 lg:mr-3 text-lg">
    			Home
    		</a>
    		<a href="<?php echo base_url().'#cara'; ?>"
    			class="block mt-4 lg:inline-block lg:mt-0 text-gray-700 hover:text-indigo-600 lg:mr-3 text-lg">
    			Cara Penggunaan
    		</a>
    		<a href="<?php echo base_url().'#tentang'; ?>"
    			class="block mt-4 lg:inline-block lg:mt-0 text-gray-700 hover:text-indigo-600 lg:mr-3 text-lg">
    			Tentang
    		</a>
    		<a href="<?php echo base_url().'track'; ?>"
    			class="block mt-4 lg:inline-block lg:mt-0 text-gray-700 hover:text-indigo-600 text-lg">
    			Lacak Dokumen
    		</a>
    		<a href="<?= base_url(); ?>login"
    			class="inline-block text-sm px-6 py-2 leading-none border rounded text-white text-base font-normal leading-6 bg-indigo-600 border-indigo-600 hover:border-indigo-700 hover:text-white hover:bg-indigo-700 mt-4 lg:mt-0 ml-3">Masuk</a>
    	</div>
    </div>
</header>

<section class="px-2 py-32 bg-white md:px-0 relative">
  <div class="container items-center max-w-6xl px-8 mx-auto xl:px-5">
    <div class="flex flex-wrap items-center sm:-mx-3">
      <div class="w-full md:w-1/2 md:px-3 wow fadeInLeft" data-wow-delay="0.2s">
        <div class="w-full pb-6 space-y-4 sm:max-w-md lg:max-w-lg md:space-y-2 lg:space-y-6 xl:space-y-7 sm:pr-5 lg:pr-0 md:pb-0">
          <h1 class="text-xl font-bold tracking-tight text-gray-900 sm:text-3xl md:text-2xl lg:text-3xl xl:text-4xl">
            <span class="block text-indigo-600 xl:inline">Adaptasi Kebiasaan Baru Hindari Tatap Muka</span>
          </h1>
          <p class="mx-auto text-base text-gray-500 sm:max-w-md lg:text-xl md:max-w-3xl">Ingin bertemu keluarga atau teman? Ingin daftar antrian tapi malas menunggu lama? Ambil antrian anda disini dan jadi <span class="block italic text-indigo-600 xl:inline">#SiapNewNormal</span></p>
          <div class="flex flex-col sm:flex-row sm:space-x-6">
            <a href="<?= base_url(); ?>login" class="flex items-center text-center w-full px-8 py-3 mb-3 text-lg text-indigo-600 border-2 border-indigo-600 bg-transparent rounded-md sm:mb-0 hover:bg-indigo-700 hover:text-white sm:w-auto">
              Buat Pernyataan
            </a>
          </div>
        </div>
      </div>
      <div class="w-full md:w-1/2 wow fadeInRight">
        <div class="w-full h-auto overflow-hidden">
            <img src="<?= base_url(); ?>assets/img/agree.svg">
        </div>
      </div>
    </div>
  </div>
  
  <div class="absolute bottom-20 left-10 px-10 mt-10 hidden lg:block wow fadeIn" data-wow-delay="0.5s">
    <img src="<?= base_url(); ?>assets/img/bulet.svg">
  </div>
</section>
<div class="hidden md:block md:bg-gray-100">
    <div class="max-w-screen-xl px-4 py-12 mx-auto sm:px-6 lg:px-8">
        <div class="grid grid-cols-2 gap-8 md:grid-cols-6 lg:grid-cols-5">
            <div class="flex items-center justify-center col-span-1 md:col-span-2 lg:col-span-1">
                <img class="object-center" src="<?= base_url(); ?>assets/img/profesional.svg">
            </div>
            <div class="flex items-center justify-center col-span-1 md:col-span-2 lg:col-span-1">
                <img class="object-center" src="<?= base_url(); ?>assets/img/akuntabel.svg">
            </div>
            <div class="flex items-center justify-center col-span-1 md:col-span-2 lg:col-span-1">
                <img class="object-center" src="<?= base_url(); ?>assets/img/sinergi.svg">
            </div>
            <div class="flex items-center justify-center col-span-1 md:col-span-2 lg:col-span-1">
                <img class="object-center" src="<?= base_url(); ?>assets/img/transparan.svg">
            </div>
            <div class="flex items-center justify-center col-span-1 md:col-span-2 lg:col-span-1">
                <img class="object-center" src="<?= base_url(); ?>assets/img/inovatif.svg">
            </div>
        </div>
    </div>
</div>
<section class="w-full bg-white pt-7 pb-7 md:pt-20 md:pb-24" id="cara">
    <div class="text-center pb-20 lg:pb-0 wow fadeInDown">
        <span class="text-indigo-600 text-xl font-semibold lg:text-3xl md:text-2xl">Cara Penggunaan</span>
        <img class="object-none object-center w-full" src="<?= base_url(); ?>assets/img/garis.svg">
    </div>
    
    <div class="box-border flex flex-col items-center content-center px-8 mx-auto leading-6 text-black border-0 border-gray-300 border-solid md:flex-row max-w-7xl lg:px-16 sm:pt-20">
        <!-- Image -->
        <div class="box-border relative w-full max-w-md px-4 mt-5 mb-4 -ml-5 text-center bg-no-repeat bg-contain border-solid md:ml-0 md:mt-0 md:max-w-none lg:mb-0 md:w-1/2 xl:pl-10 wow fadeInLeft" data-wow-delay="0.2s">
            <img src="<?= base_url(); ?>assets/img/daftar.svg" class="p-2 pl-6 pr-5 xl:pl-16 xl:pr-20 " />
        </div>

        <!-- Content -->
        <div class="box-border order-first w-full text-black border-solid md:w-1/2 md:pl-10 md:order-none wow fadeInRight" data-wow-delay="0.5s">
            <h2 class="m-0 text-xl font-semibold leading-tight border-0 border-gray-300 lg:text-3xl md:text-2xl">
                Langkah Pertama
            </h2>
            <p class="pt-4 pb-8 m-0 leading-7 text-gray-700 border-0 border-gray-300 sm:pr-12 xl:pr-32 lg:text-lg">
                Sebelum daftar, pastikan anda :
            </p>
            <ul class="p-0 m-0 leading-6 border-0 border-gray-300">
                <li class="box-border relative py-1 pl-0 text-left text-gray-500 border-solid">
                    <span class="inline-flex items-center justify-center w-6 h-6 mr-2 text-indigo-600 border border-indigo-600 rounded-full"><span class="text-sm font-bold">✓</span></span> Maximize productivity and growth
                </li>
                <li class="box-border relative py-1 pl-0 text-left text-gray-500 border-solid">
                    <span class="inline-flex items-center justify-center w-6 h-6 mr-2 text-indigo-600 border border-indigo-600 rounded-full"><span class="text-sm font-bold">✓</span></span> Speed past your competition
                </li>
                <li class="box-border relative py-1 pl-0 text-left text-gray-500 border-solid">
                    <span class="inline-flex items-center justify-center w-6 h-6 mr-2 text-indigo-600 border border-indigo-600 rounded-full"><span class="text-sm font-bold">✓</span></span> Learn the top techniques
                </li>
            </ul>
        </div>
        <!-- End  Content -->
    </div>
    <div class="box-border flex flex-col items-center content-center px-8 mx-auto mt-2 leading-6 text-black border-0 border-gray-300 border-solid md:mt-20 xl:mt-0 md:flex-row max-w-7xl lg:px-16">

        <!-- Content -->
        <div class="box-border w-full text-black border-solid md:w-1/2 md:pl-6 xl:pl-32 wow fadeInLeft" data-wow-delay="0.2s">
            <h2 class="m-0 text-xl font-semibold leading-tight border-0 border-gray-300 lg:text-3xl md:text-2xl">
                Langkah Kedua
            </h2>
            <p class="pt-4 pb-8 m-0 leading-7 text-gray-700 border-0 border-gray-300 sm:pr-10 lg:text-lg">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sit diam, mauris elit turpis elit sit morbi aliquam. Elit quam facilisi enim orci.
            </p>
        </div>
        <!-- End  Content -->

        <!-- Image -->
        <div class="box-border relative w-full max-w-md px-4 mt-10 mb-4 text-center bg-no-repeat bg-contain border-solid md:mt-0 md:max-w-none lg:mb-0 md:w-1/2 wow fadeInRight" data-wow-delay="0.5s">
            <img src="<?= base_url(); ?>assets/img/form.svg" class="pl-4 sm:pr-10 xl:pl-10 lg:pr-32" />
        </div>
    </div>
    <div class="box-border flex flex-col items-center content-center px-8 mx-auto leading-6 text-black border-0 border-gray-300 border-solid md:flex-row max-w-7xl lg:px-16">
        <!-- Image -->
        <div class="box-border relative w-full max-w-md px-4 mt-5 mb-4 -ml-5 text-center bg-no-repeat bg-contain border-solid md:ml-0 md:mt-0 md:max-w-none lg:mb-0 md:w-1/2 xl:pl-10 wow fadeInLeft" data-wow-delay="0.2s">
            <img src="<?= base_url(); ?>assets/img/tanggal.svg" class="p-2 pl-6 pr-5 xl:pl-16 xl:pr-20 " />
        </div>

        <!-- Content -->
        <div class="box-border order-first w-full text-black border-solid md:w-1/2 md:pl-10 md:order-none wow fadeInRight" data-wow-delay="0.5s">
            <h2 class="m-0 text-xl font-semibold leading-tight border-0 border-gray-300 lg:text-3xl md:text-2xl">
                Langkah Ketiga
            </h2>
            <p class="pt-4 pb-8 m-0 leading-7 text-gray-700 border-0 border-gray-300 sm:pr-12 xl:pr-32 lg:text-lg">
                Sebelum daftar, pastikan anda :
            </p>
            <ul class="p-0 m-0 leading-6 border-0 border-gray-300">
                <li class="box-border relative py-1 pl-0 text-left text-gray-500 border-solid">
                    <span class="inline-flex items-center justify-center w-6 h-6 mr-2 text-indigo-600 border border-indigo-600 rounded-full"><span class="text-sm font-bold">✓</span></span> Maximize productivity and growth
                </li>
                <li class="box-border relative py-1 pl-0 text-left text-gray-500 border-solid">
                    <span class="inline-flex items-center justify-center w-6 h-6 mr-2 text-indigo-600 border border-indigo-600 rounded-full"><span class="text-sm font-bold">✓</span></span> Speed past your competition
                </li>
                <li class="box-border relative py-1 pl-0 text-left text-gray-500 border-solid">
                    <span class="inline-flex items-center justify-center w-6 h-6 mr-2 text-indigo-600 border border-indigo-600 rounded-full"><span class="text-sm font-bold">✓</span></span> Learn the top techniques
                </li>
            </ul>
        </div>
        <!-- End  Content -->
    </div>
</section>
<footer class="bg-gray-100" id="tentang">
      <div class="container mx-auto px-6 pt-10 pb-6">
        <div class="flex flex-wrap items-center text-center">
          <div class="w-full md:w-1/3 text-center md:text-center justify-center mb-4">
            <div class="flex flex-row justify-center">
                <div class="items-center content-center px-3">
                    <img class="object-center" src="<?= base_url(); ?>assets/img/pemasyarakatan.svg" alt="Logo Pemasyarakatan">
                </div>
                <div class="items-center content-center px-3">
                    <img class="object-center" src="<?= base_url(); ?>assets/img/pengayoman.svg" alt="Logo Pengayoman">
                </div>
            </div>
            <!-- <div class="absolute pt-0 mr-5"></div>
            <div class="absolute pl-20 pt-0"></div> -->
          </div>
          <div class="w-full md:w-1/3 text-center md:text-center justify-center ">
            <h5 class="uppercase mb-6 font-bold">Rutan Kelas I Bandung</h5>
            <p class="text-left md:mx-20 font-regular text-gray-700 pb-10">Jl. Jakarta No 29 Kel. Kebonwaru, Kec. Batununggal, Kota Bandung, Jawa Barat 40272</p>
          </div>
          <div class="w-full md:w-1/3 text-center md:text-center justify-end">
            <h5 class="uppercase mb-6 font-bold">Hubungi Kami</h5>
            <div class="flex flex-wrap justify-between">
                <div class="md:w-1/2">
                    <ul class="mb-4 md:ml-20">
                        <li class="mt-2">
                            <a href="tel:0227275770" class="hover:underline text-gray-600 hover:text-orange-500"><img src="<?= base_url(); ?>assets/img/tel.svg"></a>
                        </li>
                        <li class="mt-2">
                            <a href="mailto:humas@rutankelas1bandung.com" class="hover:underline text-gray-600 hover:text-orange-500"><img src="<?= base_url(); ?>assets/img/mail.svg"></a>
                        </li>
                    </ul>
                </div>
                <div class="md:w-1/2">
                    <ul class="mb-4 md:ml-8">
                        <li class="mt-2">
                            <a href="https://www.facebook.com/rutanklas1bandung" class="hover:underline text-gray-600 hover:text-orange-500"><img src="<?= base_url(); ?>assets/img/fb.svg"></a>
                        </li>
                        <li class="mt-2">
                            <a href="https://www.instagram.com/rutanbandungjuara/" class="hover:underline text-gray-600 hover:text-orange-500"><img src="<?= base_url(); ?>assets/img/ig.svg"></a>
                        </li>
                        <li class="mt-2">
                            <a href="https://twitter.com/rutanbdg" class="hover:underline text-gray-600 hover:text-orange-500"><img src="<?= base_url(); ?>assets/img/tw.svg"></a>
                        </li>
                    </ul>
                </div>
            </div>
          </div>
        </div>
      </div>
      <div class="bg-indigo-600 py-5">
        <p class="lg:text-base lg:leading-6 text-center text-indigo-100">
            &copy; 2021 Rutan Kelas I Bandung. All rights reserved.
        </p>
      </div>
    </footer>

    <script src="<?= base_url(); ?>assets/index/js/alpine.js"></script>
    <script src="<?= base_url(); ?>assets/index/js/jquery.min.js"></script>
    <script src="<?= base_url(); ?>assets/index/js/jquery-ui.min.js"></script>
    <script>
        $(window).scroll(function() {
            if ($(window).scrollTop() > 10) {
                $('#navBar2').addClass('shadow-md');
            } else {
                $('#navBar2').removeClass('shadow-md');
            }
        });
    </script>
    <script>
    $(document).ready(function () {

		    var $window = $(window);

		    function checkWindowWidth() {
		    	
		        var windowsize = $window.width();

		        if (windowsize > 1000) {

		            $('#navbar').css('display','block');

		        } else {

		            $('#navbar').css('display','none');

		        }
		    }

		    checkWindowWidth();

		    $(window).resize(checkWindowWidth);

		});


      $('#navbar-btn').click(function() {
        $('#navbar').toggle();
      });
    </script>
</body>
</html>