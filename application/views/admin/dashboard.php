<div class="d-sm-flex align-items-center justify-content-between mb-4">

    <!-- Earnings (Monthly) Card Example -->
    <div class="col-xl-6 col-md-8 mb-4">
        <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                            Jumlah Pengguna</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $u_count['countu'];?></div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-users fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Earnings (Monthly) Card Example -->
    <div class="col-xl-6 col-md-8 mb-4">
        <div class="card border-left-success shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                            Jumlah Dokumen</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $f_count['countf'];?></div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-file fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Content Row -->

<div class="row">

    <!-- Area Chart -->
    <div class="col-xl-8 col-lg-7">
        <div class="card shadow mb-4">
        	<div class="card-header py-3">
        		<h6 class="m-0 font-weight-bold text-primary">Pengguna Tiap Bulan</h6>
        	</div>
        	<div class="card-body">
        		<div class="chart-bar">
        			<canvas id="myBarChart"></canvas>
        		</div>
        	</div>
        </div>
    </div>

    <!-- Pie Chart -->
    <div class="col-xl-4 col-lg-5">
        <div class="card shadow mb-4">
            <!-- Card Header - Dropdown -->
            <div
                class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Persentase Permohonan</h6>
            </div>
            <!-- Card Body -->
            <div class="card-body">
                <div class="chart-pie pt-4 pb-2">
                    <canvas id="myPieChart"></canvas>
                </div>
                <div class="mt-4 text-center small">
                    <span class="mr-2">
                        <i class="fas fa-circle text-primary"></i> Asimilasi
                    </span>
                    <span class="mr-2">
                        <i class="fas fa-circle text-success"></i> Pembebasan Bersyarat
                    </span>
                    <span class="mr-2">
                        <i class="fas fa-circle text-info"></i> Cuti Menjelang Bebas
                    </span>
                    <span class="mr-2">
                        <i class="fas fa-circle text-warning"></i> Cuti Bersyarat
                    </span>
                    <span class="mr-2">
                        <i class="fas fa-circle text-danger"></i> Justice Collaborator
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>

          