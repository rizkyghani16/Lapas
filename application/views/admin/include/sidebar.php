<ul class="navbar-nav bg-gradient-info sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?php echo base_url('admin/main'); ?>">
        <div class="sidebar-brand-text mx-3">Rutan Kelas I Bandung</div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url('admin/main/'); ?>">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url('admin/main/users'); ?>">
          <i class="fas fa-fw fa-users"></i>
          <span>Pengguna</span></a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url('admin/main/permohonan'); ?>">
          <i class="fas fa-fw fa-cog"></i>
          <span>Permohonan</span></a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url('admin/main/narapidana'); ?>">
          <i class="fas fa-fw fa-user-secret"></i>
          <span>Narapidana</span></a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url('admin/main/dokumen'); ?>">
          <i class="fas fa-fw fa-folder-open"></i>
          <span>Dokumen</span></a>
      </li>

      <!-- Divider -->
      <!-- <hr class="sidebar-divider"> -->

      <!-- Heading -->
      <!-- <div class="sidebar-heading">
        Borang
      </div> -->

      <!-- Nav Item - Pages Collapse Menu -->
     

      

      <!-- Divider -->
      <!-- <hr class="sidebar-divider"> -->

      <!-- Sidebar Toggler (Sidebar) -->

      <!-- <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div> -->

    </ul>