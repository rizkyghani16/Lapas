<div class="d-sm-flex align-items-center justify-content-between mb-4">
	<h1 class="h3 mb-0 text-gray-800">Pengguna</h1>

    <button data-toggle="modal" data-target="#modal_add_new" class="btn btn-outline-success btn-md float-right"><i class="fas fa-fw fa-plus"></i> Tambah</button>
</div>
	
<?= $this->session->flashdata('message') ?>
	<div class="card shadow mb-4">
		<div class="card-body">
			<div class="table-responsive">
				<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th>Nama</th>
							<th>Umur</th>
							<th>Email</th>
							<th>Pekerjaan</th>
							<th>Telepon</th>
							<th class="text-center">Aksi</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
                            <th>Nama</th>
							<th>Umur</th>
							<th>Email</th>
							<th>Pekerjaan</th>
							<th>Telepon</th>
							<th class="text-center">Aksi</th>
						</tr>
					</tfoot>
					<tbody>
                        <?php
                        // $no = 1;
                        foreach ($users->result_array() as $i) :
                            $users_id = $i['users_id'];
                            $fullname = $i['fullname'];
                            $age = $i['age'];
                            $email = $i['email'];
                            $job = $i['job'];
                            $tel = $i['tel'];
                        ?>
                            <tr>
                                <td><?php echo strtoupper($fullname); ?></td>
                                <td class="text-center"><?php echo $age; ?></td>
                                <td><?php echo $email; ?></td>
                                <td><?php echo strtoupper($job); ?></td>
                                <td><?php echo $tel; ?></td>
                                <td class="text-center"><a data-toggle="modal" data-target="#modal_hapus<?php echo $users_id; ?>" class="btn btn-sm btn-danger text-white"><i class="fas fa-trash-alt"></i> Hapus</a> <a data-toggle="modal" data-target="#modal_detail<?php echo $users_id; ?>" class="btn btn-sm btn-info text-white"><i class="fas fa-info-circle"></i> Detail</a> <a data-toggle="modal" data-target="#modal_password<?php echo $users_id; ?>" class="btn btn-sm btn-warning text-white"><i class="fas fa-key"></i> Password</a></td>
                        <?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>

<!-- ============ MODAL ADD =============== -->
<div class="modal fade" id="modal_add_new" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="modal_add_new">Tambah Pengguna</h3>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
            </div>
            <form action="<?php echo base_url() . 'admin/main/add_user'; ?>" method="POST">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="fullname">Nama Lengkap</label>
                        <input type="text" class="form-control" id="fullname" name="fullname" placeholder="Nama lengkap pengguna" required>
                    </div>

                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="Email" required>
                    </div>

                    <div class="form-row">
                    	<div class="form-group col-md-6">
                    		<label for="password">Kata Sandi</label>
                    		<input type="password" class="form-control" id="password" name="password" placeholder="Kata sandi pengguna" required>
                    	</div>
                    	<div class="form-group col-md-6">
                    		<label for="password2">Konfirmasi Kata Sandi</label>
                    		<input type="password" class="form-control" id="password2" name="password2" placeholder="Kata sandi pengguna" required>
                    	</div>
                    </div>

                    <div class="form-group">
                    	<label for="gender">Jenis Kelamin</label>
                    	<select class="form-control" id="gender" name="gender" required>
                    		<option disabled selected value="">Pilih</option>
                    		<option>Laki - Laki</option>
                    		<option>Perempuan</option>
                    	</select>
                    </div>

                    <div class="form-row">
                    	<div class="form-group col-md-4">
                    		<label for="job">Pekerjaan</label>
                    		<input type="text" class="form-control" id="job" name="job" placeholder="Pekerjaan pengguna" required>
                    	</div>
                    	<div class="form-group col-md-4">
                    		<label for="age">Umur</label>
                    		<input type="text" class="form-control" id="age" name="age" placeholder="Umur pengguna" required>
                    	</div>
                    	<div class="form-group col-md-4">
                    		<label for="tel">Telepon</label>
                    		<input type="text" class="form-control" id="tel" name="tel" placeholder="Telpon pengguna" required>
                    	</div>
                    </div>

                    <div class="form-group">
                    	<label for="address">Alamat</label>
                    	<textarea class="form-control" id="address" name="address" rows="3" placeholder="Alamat pengguna" required></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                    <button class="btn btn-info">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!--END MODAL ADD-->

<!-- ============ MODAL HAPUS =============== -->
<?php
foreach ($users->result_array() as $i) :
    $users_id = $i['users_id'];
    $fullname = $i['fullname'];
    $age = $i['age'];
    $email = $i['email'];
    $job = $i['job'];
    $tel = $i['tel'];
?>
    <div class="modal fade" id="modal_hapus<?php echo $users_id; ?>" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="modal_hapus">Persetujuan</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                </div>
                <form action="<?php echo base_url() . 'admin/main/hapus_user'; ?>" method="post">
                    <div class="modal-body">

                        <input type="text" class="form-control" id="users_id" name="users_id" value="<?php echo $users_id; ?>" hidden="true">
                        <h6>Apakah anda yakin menghapus pengguna bernama <b><?php echo strtoupper($fullname); ?></b> ?</h6>
                    </div>

                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal" aria-hidden="true">Tidak</button>
                        <button class="btn btn-info">Yakin</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

<?php endforeach; ?>
<!--END MODAL HAPUS-->

<!-- ============ MODAL DETAIL =============== -->
<?php
foreach ($users->result_array() as $i) :
    $users_id = $i['users_id'];
    $fullname = $i['fullname'];
    $age = $i['age'];
    $email = $i['email'];
    $job = $i['job'];
    $tel = $i['tel'];
    $gender = $i['gender'];
    $address = $i['address'];
?>
    <div class="modal fade" id="modal_detail<?php echo $users_id; ?>" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="modal_detail">Detail Pengguna</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                </div>
                <form action="<?php echo base_url() . 'admin/main/update_user'; ?>" method="post">
                    <div class="modal-body">
                        <input type="text" class="form-control" id="users_id" name="users_id" value="<?php echo $users_id; ?>" hidden="true">
                        <div class="form-group">
                            <label for="fullname">Nama Lengkap</label>
                            <input type="text" class="form-control" id="fullname" name="fullname" placeholder="Nama lengkap pengguna" value="<?php echo strtoupper($fullname); ?>" required>
                        </div>

                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="<?php echo strtolower($email); ?>" required>
                        </div>

                        <div class="form-group">
                            <label for="gender">Jenis Kelamin</label>
                            <select class="form-control" id="gender" name="gender" required>
                                <option disabled value="">Pilih</option>
                                <option value="Laki - Laki" <?php if($gender=="Laki - Laki") echo 'selected="selected"'; ?> >Laki - Laki</option>
                                <option value="Perempuan" <?php if($gender=="Perempuan") echo 'selected="selected"'; ?> >Perempuan</option>
                            </select>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="job">Pekerjaan</label>
                                <input type="text" class="form-control" id="job" name="job" placeholder="Pekerjaan pengguna" value="<?php echo strtoupper($job); ?>" required>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="age">Umur</label>
                                <input type="text" class="form-control" id="age" name="age" placeholder="Umur pengguna" value="<?php echo ($age); ?>" required>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="tel">Telepon</label>
                                <input type="text" class="form-control" id="tel" name="tel" placeholder="Telpon pengguna" value="<?php echo ($tel); ?>" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="address">Alamat</label>
                            <textarea class="form-control" id="address" name="address" rows="3" placeholder="Alamat pengguna" required><?php echo strtoupper($address); ?></textarea>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal" aria-hidden="true">Batal</button>
                        <button class="btn btn-info">Ubah</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

<?php endforeach; ?>
<!--END MODAL DETAIL-->

<!-- ============ MODAL PASSWORD =============== -->
<?php
foreach ($users->result_array() as $i) :
    $users_id = $i['users_id'];
    $fullname = $i['fullname'];
    $age = $i['age'];
    $email = $i['email'];
    $job = $i['job'];
    $tel = $i['tel'];
    $gender = $i['gender'];
    $address = $i['address'];
?>
    <div class="modal fade" id="modal_password<?php echo $users_id; ?>" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="modal_password">Ubah Kata Sandi Pengguna</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                </div>
                <form action="<?php echo base_url() . 'admin/main/update_password'; ?>" method="post">
                    <div class="modal-body">
                        <input type="text" class="form-control" id="users_id" name="users_id" value="<?php echo $users_id; ?>" hidden="true">
                        <div class="form-group">
                            <label for="password">Kata Sandi</label>
                            <input type="text" class="form-control" id="password" name="password" placeholder="Kata sandi pengguna" required>
                        </div>

                        <div class="form-group">
                            <label for="password2">Konfirmasi Kata Sandi</label>
                            <input type="text" class="form-control" id="password2" name="password2" placeholder="Kata sandi pengguna" required>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal" aria-hidden="true">Batal</button>
                        <button class="btn btn-info">Ubah</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

<?php endforeach; ?>
<!--END MODAL PASSWORD-->



