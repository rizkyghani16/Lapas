<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <link rel="icon" href="https://rutankelas1bandung.com/sopefuh/2019/12/cropped-1-2-32x32.png" sizes="32x32">
  <link rel="icon" href="https://rutankelas1bandung.com/sopefuh/2019/12/cropped-1-2-192x192.png" sizes="192x192">
  <link rel="apple-touch-icon" href="https://rutankelas1bandung.com/sopefuh/2019/12/cropped-1-2-180x180.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Rutan Kelas I Bandung | Dashboard</title>

  <!-- Custom fonts for this template-->
  <link href="<?php echo base_url(); ?>assets/admin/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<?php echo base_url(); ?>assets/admin/css/sb-admin-2.min.css" rel="stylesheet">

  <link href="<?php echo base_url(); ?>assets/admin/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/bootstrap-select.min.css">

  <style>
  .emp-profile {
  	padding: 3%;
  	margin-top: 3%;
  	margin-bottom: 3%;
  	border-radius: 0.5rem;
  	background: #fff;
  }

  .profile-img {
  	text-align: center;
  }

  .profile-img img {
  	width: 70%;
  }

  .profile-img .file {
  	position: relative;
  	overflow: hidden;
  	margin-top: -20%;
  	width: 70%;
  	border: none;
  	border-radius: 0;
  	font-size: 15px;
  	background: #212529b8;
  }

  .profile-img .file input {
  	position: absolute;
  	opacity: 0;
  	right: 0;
  	top: 0;
  }

  .profile-head h4 {
  	color: #333;
  }

  .profile-head h5 {
  	color: #0062cc;
  }

  .profile-edit-btn {
  	border: none;
  	border-radius: 1.5rem;
  	width: 70%;
  	padding: 2%;
  	font-weight: 600;
  	color: #6c757d;
  	cursor: pointer;
  }

  .proile-rating {
  	font-size: 12px;
  	color: #818182;
  	margin-top: 5%;
  }

  .proile-rating span {
  	color: #495057;
  	font-size: 15px;
  	font-weight: 600;
  }

  .profile-head .nav-tabs {
  	/* margin-bottom: 5%; */
  }

  .profile-head .nav-tabs .nav-link {
  	font-weight: 600;
  	border: none;
  }

  .profile-head .nav-tabs .nav-link.active {
  	border: none;
  	border-bottom: 2px solid #0062cc;
  }

  .profile-work {
  	padding: 14%;
  	margin-top: -15%;
  }

  .profile-work p {
  	font-size: 12px;
  	color: #818182;
  	font-weight: 600;
  	margin-top: 10%;
  }

  .profile-work a {
  	text-decoration: none;
  	color: #495057;
  	font-weight: 600;
  	font-size: 14px;
  }

  .profile-work ul {
  	list-style: none;
  }

  .profile-tab label {
  	font-weight: 600;
  }

  .profile-tab p {
  	font-weight: 600;
  	color: #0062cc;
  }
  </style>
  <style>
    .tracking-detail {
        padding: 3rem 0
    }

    #tracking {
        margin-bottom: 1rem
    }

    [class*=tracking-status-] p {
        margin: 0;
        font-size: 1.1rem;
        color: #fff;
        text-transform: uppercase;
        text-align: center
    }

    [class*=tracking-status-] {
        padding: 1.6rem 0
    }

    .tracking-status-intransit {
        background-color: #65aee0
    }

    .tracking-status-outfordelivery {
        background-color: #f5a551
    }

    .tracking-status-deliveryoffice {
        background-color: #f7dc6f
    }

    .tracking-status-delivered {
        background-color: #4cbb87
    }

    .tracking-status-attemptfail {
        background-color: #b789c7
    }

    .tracking-status-error,
    .tracking-status-exception {
        background-color: #d26759
    }

    .tracking-status-expired {
        background-color: #616e7d
    }

    .tracking-status-pending {
        background-color: #ccc
    }

    .tracking-status-inforeceived {
        background-color: #214977
    }

    .tracking-list {
        border: 1px solid #e5e5e5
    }

    .tracking-item {
        border-left: 1px solid #e5e5e5;
        position: relative;
        padding: 3rem 1.5rem .5rem 2.5rem;
        font-size: .9rem;
        margin-left: 3rem;
        min-height: 5rem
    }

    .tracking-item:last-child {
        padding-bottom: 2rem
    }

    .tracking-item .tracking-date {
        margin-bottom: .5rem
    }

    .tracking-item .tracking-date span {
        color: #888;
        font-size: 85%;
        padding-left: .4rem
    }

    .tracking-item .tracking-content {
        padding: .5rem .8rem;
        background-color: #f4f4f4;
        border-radius: .5rem
    }

    .tracking-item .tracking-content span {
        display: block;
        color: #888;
        font-size: 85%
    }

    .tracking-item .tracking-icon {
        line-height: 2.6rem;
        position: absolute;
        left: -1.3rem;
        /* padding-top: 0.4rem; */
        width: 2.6rem;
        height: 2.6rem;
        text-align: center;
        border-radius: 50%;
        font-size: 1.1rem;
        background-color: #fff;
        color: #fff
    }

    .tracking-item .tracking-icon.status-sponsored {
        background-color: #f68
    }

    .tracking-item .tracking-icon.status-delivered {
        background-color: #4cbb87
    }

    .tracking-item .tracking-icon.status-outfordelivery {
        background-color: #f5a551
    }

    .tracking-item .tracking-icon.status-deliveryoffice {
        background-color: #f7dc6f
    }

    .tracking-item .tracking-icon.status-attemptfail {
        background-color: #b789c7
    }

    .tracking-item .tracking-icon.status-exception {
        background-color: #d26759
    }

    .tracking-item .tracking-icon.status-inforeceived {
        background-color: #214977
    }

    .tracking-item .tracking-icon.status-intransit {
        color: #e5e5e5;
        font-size: .6rem
    }

    @media(min-width:992px) {
        .tracking-item {
            margin-left: 10rem
        }

        .tracking-item .tracking-date {
            position: absolute;
            left: -10rem;
            width: 7.5rem;
            text-align: right
        }

        .tracking-item .tracking-date span {
            display: block
        }

        .tracking-item .tracking-content {
            padding: 0;
            background-color: transparent
        }
    }
</style>

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <?php include 'include/sidebar.php'; ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <?php include 'include/topbar.php'; ?>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <?php $this->load->view($view); ?>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Rutan Kelas I Bandung <?php echo date('Y'); ?></span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Anda yakin?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Pilih "Keluar" jika anda yakin akan keluar</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Tidak</button>
          <a class="btn btn-primary" href="<?php echo base_url(); ?>index.php/main/logout">Keluar</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo base_url(); ?>assets/admin/vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/admin/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?php echo base_url(); ?>assets/admin/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?php echo base_url(); ?>assets/admin/js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="<?php echo base_url(); ?>assets/admin/vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <!-- <script src="<?php //echo base_url();?>assets/admin/js/demo/chart-bar-demo.js"></script> -->
  <!-- <script src="<?php //echo base_url();?>assets/admin/js/demo/chart-pie-demo.js"></script> -->

  <!-- Page level plugins -->
  <script src="<?php echo base_url(); ?>assets/admin/vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/admin/vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="<?php echo base_url(); ?>assets/admin/js/demo/datatables-demo.js"></script>

  <script src="<?php echo base_url(); ?>assets/dist/js/bootstrap-select.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/dist/js/i18n/defaults-*.min.js"></script>
  
  <!-- MODAL PDF  -->
  <script>
  (function(a){a.createModal=function(b){defaults={title:"",closeButton:true,scrollable:false};var b=a.extend({},defaults,b);var c=(b.scrollable===true)?'style="max-height: 420px;overflow-y: auto;"':"";html='<div class="modal fade" id="myModal">';html+='<div class="modal-dialog">';html+='<div class="modal-content">';html+='<div class="modal-header">';html+='<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';if(b.title.length>0){html+='<h4 class="modal-title">'+b.title+"</h4>"}html+="</div>";html+='<div class="modal-body" '+c+">";html+=b.message;html+="</div>";html+="</div>";html+="</div>";html+="</div>";html+="</div>";a("body").prepend(html);a("#myModal").modal().on("hidden.bs.modal",function(){a(this).remove()})}})(jQuery);
  $(function () {
  	$('.view-surat').on('click', function () {
  		var pdf_link = $(this).attr('href');
  		//var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>'
  		//var iframe = '<object data="'+pdf_link+'" type="application/pdf"><embed src="'+pdf_link+'" type="application/pdf" /></object>'        
  		var iframe = '<object type="application/pdf" data="' + pdf_link + '" width="100%" height="500">No Support</object>'
  		$.createModal({
  			message: iframe,
  			closeButton: false,
  			scrollable: false
  		});
  		return false;
  	});
  })
  </script>
  <!-- END MODAL PDF  -->

  <!-- MODAL KK  -->
  <script>
  (function(a){a.createModal=function(b){defaults={title:"",closeButton:true,scrollable:false};var b=a.extend({},defaults,b);var c=(b.scrollable===true)?'style="max-height: 420px;overflow-y: auto;"':"";html='<div class="modal fade" id="myModal">';html+='<div class="modal-dialog">';html+='<div class="modal-content">';html+='<div class="modal-header">';html+='<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';if(b.title.length>0){html+='<h4 class="modal-title">'+b.title+"</h4>"}html+="</div>";html+='<div class="modal-body" '+c+">";html+=b.message;html+="</div>";html+="</div>";html+="</div>";html+="</div>";html+="</div>";a("body").prepend(html);a("#myModal").modal().on("hidden.bs.modal",function(){a(this).remove()})}})(jQuery);
  $(function () {
  	$('.view-kk').on('click', function () {
  		var pdf_link = $(this).attr('href');
  		//var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>'
  		//var iframe = '<object data="'+pdf_link+'" type="application/pdf"><embed src="'+pdf_link+'" type="application/pdf" /></object>'        
  		var iframe = '<object type="application/pdf" data="' + pdf_link + '" width="100%" height="500">No Support</object>'
  		$.createModal({
  			message: iframe,
  			closeButton: false,
  			scrollable: false
  		});
  		return false;
  	});
  })
  </script>
  <!-- END MODAL KK  -->

  <!-- PIE CHART -->
  <script>
    // Set new default font family and font color to mimic Bootstrap's default styling
    Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
    Chart.defaults.global.defaultFontColor = '#858796';

    // Pie Chart Example
    var ctx = document.getElementById("myPieChart");
    var asi = <?php echo json_encode($f_asi['countp']); ?>;
    var ber = <?php echo json_encode($f_ber['countp']); ?>;
    var cmb = <?php echo json_encode($f_cmb['countp']); ?>;
    var cbe = <?php echo json_encode($f_cbe['countp']); ?>;
    var jco = <?php echo json_encode($f_jco['countp']); ?>;
    var myPieChart = new Chart(ctx, {
    	type: 'doughnut',
    	data: {
    		labels: ["Asimilasi", "Pembebasan Bersyarat", "Cuti Menjelang Bebas", "Cuti Bersyarat", "Justice Collaborator"],
    		datasets: [{
    			data: [asi, ber, cmb, cbe, jco],
    			backgroundColor: ['#4e73df', '#1cc88a', '#36b9cc', '#f6c23e', '#e74a3b'],
    			hoverBackgroundColor: ['#2e59d9', '#17a673', '#2c9faf', '#a88732', '#a1342a'],
    			hoverBorderColor: "rgba(234, 236, 244, 1)",
    		}],
    	},
    	options: {
    		maintainAspectRatio: false,
    		tooltips: {
    			backgroundColor: "rgb(255,255,255)",
    			bodyFontColor: "#858796",
    			borderColor: '#dddfeb',
    			borderWidth: 1,
    			xPadding: 15,
    			yPadding: 15,
    			displayColors: false,
    			caretPadding: 10,
    		},
    		legend: {
    			display: false
    		},
    		cutoutPercentage: 80,
    	},
    });

  </script>
  <!-- END PIE CHART -->

  <!-- BAR CHART -->
  <script>
    // Set new default font family and font color to mimic Bootstrap's default styling
    Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
    Chart.defaults.global.defaultFontColor = '#858796';

    function number_format(number, decimals, dec_point, thousands_sep) {
    	// *     example: number_format(1234.56, 2, ',', ' ');
    	// *     return: '1 234,56'
    	number = (number + '').replace(',', '').replace(' ', '');
    	var n = !isFinite(+number) ? 0 : +number,
    		prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
    		sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
    		dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
    		s = '',
    		toFixedFix = function (n, prec) {
    			var k = Math.pow(10, prec);
    			return '' + Math.round(n * k) / k;
    		};
    	// Fix for IE parseFloat(0.55).toFixed(0) = 0;
    	s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    	if (s[0].length > 3) {
    		s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    	}
    	if ((s[1] || '').length < prec) {
    		s[1] = s[1] || '';
    		s[1] += new Array(prec - s[1].length + 1).join('0');
    	}
    	return s.join(dec);
    }

    // Bar Chart Example
    var ctx = document.getElementById("myBarChart");
    var max = <?php echo json_encode($u_thn['countu']); ?>;
    var jan = <?php echo json_encode($u_jan['countu']); ?>;
    var feb = <?php echo json_encode($u_feb['countu']); ?>;
    var mar = <?php echo json_encode($u_mar['countu']); ?>;
    var apr = <?php echo json_encode($u_apr['countu']); ?>;
    var mei = <?php echo json_encode($u_mei['countu']); ?>;
    var jun = <?php echo json_encode($u_jun['countu']); ?>;
    var jul = <?php echo json_encode($u_jul['countu']); ?>;
    var agu = <?php echo json_encode($u_agu['countu']); ?>;
    var sep = <?php echo json_encode($u_sep['countu']); ?>;
    var okt = <?php echo json_encode($u_okt['countu']); ?>;
    var nov = <?php echo json_encode($u_nov['countu']); ?>;
    var des = <?php echo json_encode($u_des['countu']); ?>;
    var myBarChart = new Chart(ctx, {
    	type: 'bar',
    	data: {
    		labels: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
    		datasets: [{
    			label: "Sebanyak ",
    			backgroundColor: "#4e73df",
    			hoverBackgroundColor: "#2e59d9",
    			borderColor: "#4e73df",
    			data: [jan, feb, mar, apr, mei, jun, jul, agu, sep, okt, nov, des],
    		}],
    	},
    	options: {
    		maintainAspectRatio: false,
    		layout: {
    			padding: {
    				left: 10,
    				right: 25,
    				top: 25,
    				bottom: 0
    			}
    		},
    		scales: {
    			xAxes: [{
    				time: {
    					unit: 'month'
    				},
    				gridLines: {
    					display: false,
    					drawBorder: false
    				},
    				ticks: {
    					maxTicksLimit: 6
    				},
    				maxBarThickness: 25,
    			}],
    			yAxes: [{
    				ticks: {
    					min: 0,
    					max: max,
    					maxTicksLimit: 5,
    					padding: 10,
    					// Include a dollar sign in the ticks
    					callback: function (value, index, values) {
    						return number_format(value) + ' User';
    					}
    				},
    				gridLines: {
    					color: "rgb(234, 236, 244)",
    					zeroLineColor: "rgb(234, 236, 244)",
    					drawBorder: false,
    					borderDash: [2],
    					zeroLineBorderDash: [2]
    				}
    			}],
    		},
    		legend: {
    			display: false
    		},
    		tooltips: {
    			titleMarginBottom: 10,
    			titleFontColor: '#6e707e',
    			titleFontSize: 14,
    			backgroundColor: "rgb(255,255,255)",
    			bodyFontColor: "#858796",
    			borderColor: '#dddfeb',
    			borderWidth: 1,
    			xPadding: 15,
    			yPadding: 15,
    			displayColors: false,
    			caretPadding: 10,
    			callbacks: {
    				label: function (tooltipItem, chart) {
    					var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
    					return datasetLabel + number_format(tooltipItem.yLabel) + ' User';
    				}
    			}
    		},
    	}
    });
  </script>
  <!-- END BAR CHART -->

  <script>
      $(function () {
      	var pass1 = $('input.form-control[id=new_password]');
      	var pass2 = $('input.form-control[id=new_password2]');
      	var nums = $('input.form-control[id=new_password]');
      	pass2.on('change', function (e) {
      		var map = {};
            if (pass1.val() !== pass2.val()) {
                alert("Maaf, password baru tidak sama");
                document.getElementById('new_password2').value = "";
            } else {

            }
      		// nums.each(function (i, el) {
      		// 	if (el.value.replace(/\s/g, '')) {
      		// 		/* trim whitespace */
      		// 		if (undefined == map[el.value]) {
            //             alert("Maaf, password baru tidak sama");
      		// 		} else {
            //             map[el.value] = 1;
      		// 		}
      		// 	}
      		// });
      	});
      });
  </script>
  <script>
  $(".alert").delay(4000).slideDown(500, function() {
    $(this).alert('close');
  });
</script>
  <script>
     var suratEx = [".pdf"];    
function Surat(sInput) {
    if (sInput.type == "file") {
        var sFileName = sInput.value;
        var sFileSize = sInput.files[0].size;
         if (sFileName.length > 0) {
            var blnValid = false;
            for (var j = 0; j < suratEx.length; j++) {
                var sCurExtension = suratEx[j];
                if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                    if (sFileSize > 2097152 ) {
                        alert("Maaf, file Surat Penjamin maximal hanya 2 MB");
                        sInput.value = "";
                        return false;
                        break;
                    } else {
                        blnValid = true;
                        break;
                    }
                }
            }
             
            if (!blnValid) {
                alert("Maaf, file Surat Penjamin harus berformat " + suratEx.join(", "));
                sInput.value = "";
                return false;
            }
        }
    }
    return true;
}
    </script>

  <script>
     var kkEx = [".pdf"];    
function KartuKeluarga(kInput) {
    if (kInput.type == "file") {
        var sFileName = kInput.value;
        var sFileSize = kInput.files[0].size;
         if (sFileName.length > 0) {
            var blnValid = false;
            for (var j = 0; j < kkEx.length; j++) {
                var sCurExtension = kkEx[j];
                if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                    if (sFileSize > 2097152 ) {
                        alert("Maaf, file Kartu Keluarga maximal hanya 2 MB");
                        kInput.value = "";
                        return false;
                        break;
                    } else {
                        blnValid = true;
                        break;
                    }
                }
            }
             
            if (!blnValid) {
                alert("Maaf, file Kartu Keluarga harus berformat " + kkEx.join(", "));
                kInput.value = "";
                return false;
            }
        }
    }
    return true;
}
    </script>
  <script>
     var _validFileExtensions = [".png", ".jpg", ".jpeg"];    
function Foto(fInput) {
    if (fInput.type == "file") {
        var sFileName = fInput.value;
        var sFileSize = fInput.files[0].size;
         if (sFileName.length > 0) {
            var blnValid = false;
            for (var j = 0; j < _validFileExtensions.length; j++) {
                var sCurExtension = _validFileExtensions[j];
                if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                    if (sFileSize > 2097152 ) {
                        alert("Maaf, file Foto Penjamin maximal hanya 2 MB");
                        fInput.value = "";
                        return false;
                        break;
                    } else {
                        blnValid = true;
                        break;
                    }
                }
            }
             
            if (!blnValid) {
                alert("Maaf, file Foto Penjamin harus berformat " + _validFileExtensions.join(", "));
                fInput.value = "";
                return false;
            }
        }
    }
    return true;
}
    </script>

  <script>
     var _validFileExtensions = [".png", ".jpg", ".jpeg"];    
function Ktp(kInput) {
    if (kInput.type == "file") {
        var kFileName = kInput.value;
        var kFileSize = kInput.files[0].size;
         if (kFileName.length > 0) {
            var blnValid = false;
            for (var j = 0; j < _validFileExtensions.length; j++) {
                var sCurExtension = _validFileExtensions[j];
                if (kFileName.substr(kFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                    if (kFileSize > 2097152 ) {
                        alert("Maaf, file KTP Penjamin maximal hanya 2 MB");
                        kInput.value = "";
                        return false;
                        break;
                    } else {
                        blnValid = true;
                        break;
                    }
                }
            }
             
            if (!blnValid) {
                alert("Maaf, file KTP Penjamin harus berformat " + _validFileExtensions.join(", "));
                kInput.value = "";
                return false;
            }
        }
    }
    return true;
}
    </script>


  <script Type="text/javascript">
            function setInputFilter(textbox, inputFilter) {
            ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
                textbox.addEventListener(event, function() {
                if (inputFilter(this.value)) {
                    this.oldValue = this.value;
                    this.oldSelectionStart = this.selectionStart;
                    this.oldSelectionEnd = this.selectionEnd;
                } else if (this.hasOwnProperty("oldValue")) {
                    this.value = this.oldValue;
                    this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                } else {
                    this.value = "";
                }
                });
            });
            }

            setInputFilter(document.getElementById("umur"), function(value) {
            return /^-?\d*$/.test(value); });
        </script>
  <script>
    $("#founding_date").datepicker({
    format: "yyyy",
    viewMode: "years", 
    minViewMode: "years"
    });
  </script>
  <script>
    // Add the following code if you want the name of the file appear on select
    $(".custom-file-input").on("change", function() {
    var fileName = $(this).val().split("\\").pop();
    $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });
  </script>
  <script>
      $('#bologna-list a').on('click', function (e) {
  e.preventDefault()
  $(this).tab('show')
})
  </script>

  <!-- AJAX AKTA -->
  <script type="text/javascript">
        $(document).on('submit', '#modal-akta', function(a){
            var formData = new FormData($(this)[0]);
            $.ajax({
            method: 'POST',
            url: "<?= base_url('index.php/User/main/add_akta'); ?>",
            data: formData,
            processData: false,
            contentType: false
            }).done(function(data) {
                var out = jQuery.parseJSON(data);
                if (out.status == true) {
                    if(window.confirm(out.message)) {
                        window.location.replace("<?= base_url('index.php/User/main/dokumen'); ?>");
                    }
                } else {
                    window.confirm(out.message);
                }
            })
            a.preventDefault();
        });
    </script>

  <!-- AJAX KEMENKUMHAM -->
  <script type="text/javascript">
        $(document).on('submit', '#modal-kemenkumham', function(a){
            var formData = new FormData($(this)[0]);
            $.ajax({
            method: 'POST',
            url: "<?= base_url('index.php/User/main/add_kemenkumham'); ?>",
            data: formData,
            processData: false,
            contentType: false
            }).done(function(data) {
                var out = jQuery.parseJSON(data);
                if (out.status == true) {
                    if(window.confirm(out.message)) {
                        window.location.replace("<?= base_url('index.php/User/main/dokumen'); ?>");
                    }
                } else {
                    window.confirm(out.message);
                }
            })
            a.preventDefault();
        });
    </script>

  <!-- AJAX SIUP -->
  <script type="text/javascript">
        $(document).on('submit', '#modal-siup', function(a){
            var formData = new FormData($(this)[0]);
            $.ajax({
            method: 'POST',
            url: "<?= base_url('index.php/User/main/add_siup'); ?>",
            data: formData,
            processData: false,
            contentType: false
            }).done(function(data) {
                var out = jQuery.parseJSON(data);
                if (out.status == true) {
                    if(window.confirm(out.message)) {
                        window.location.replace("<?= base_url('index.php/User/main/dokumen'); ?>");
                    }
                } else {
                    window.confirm(out.message);
                }
            })
            a.preventDefault();
        });
    </script>

  <!-- AJAX TDP -->
  <script type="text/javascript">
        $(document).on('submit', '#modal-tdp', function(a){
            var formData = new FormData($(this)[0]);
            $.ajax({
            method: 'POST',
            url: "<?= base_url('index.php/User/main/add_tdp'); ?>",
            data: formData,
            processData: false,
            contentType: false
            }).done(function(data) {
                var out = jQuery.parseJSON(data);
                if (out.status == true) {
                    if(window.confirm(out.message)) {
                        window.location.replace("<?= base_url('index.php/User/main/dokumen'); ?>");
                    }
                } else {
                    window.confirm(out.message);
                }
            })
            a.preventDefault();
        });
    </script>

  <!-- AJAX NPWP -->
  <script type="text/javascript">
        $(document).on('submit', '#modal-npwp', function(a){
            var formData = new FormData($(this)[0]);
            $.ajax({
            method: 'POST',
            url: "<?= base_url('index.php/User/main/add_npwp'); ?>",
            data: formData,
            processData: false,
            contentType: false
            }).done(function(data) {
                var out = jQuery.parseJSON(data);
                if (out.status == true) {
                    if(window.confirm(out.message)) {
                        window.location.replace("<?= base_url('index.php/User/main/dokumen'); ?>");
                    }
                } else {
                    window.confirm(out.message);
                }
            })
            a.preventDefault();
        });
    </script>

  <!-- AJAX SPPKP -->
  <script type="text/javascript">
        $(document).on('submit', '#modal-sppkp', function(a){
            var formData = new FormData($(this)[0]);
            $.ajax({
            method: 'POST',
            url: "<?= base_url('index.php/User/main/add_sppkp'); ?>",
            data: formData,
            processData: false,
            contentType: false
            }).done(function(data) {
                var out = jQuery.parseJSON(data);
                if (out.status == true) {
                    if(window.confirm(out.message)) {
                        window.location.replace("<?= base_url('index.php/User/main/dokumen'); ?>");
                    }
                } else {
                    window.confirm(out.message);
                }
            })
            a.preventDefault();
        });
    </script>

  <!-- AJAX SITU -->
  <script type="text/javascript">
        $(document).on('submit', '#modal-situ', function(a){
            var formData = new FormData($(this)[0]);
            $.ajax({
            method: 'POST',
            url: "<?= base_url('index.php/User/main/add_situ'); ?>",
            data: formData,
            processData: false,
            contentType: false
            }).done(function(data) {
                var out = jQuery.parseJSON(data);
                if (out.status == true) {
                    if(window.confirm(out.message)) {
                        window.location.replace("<?= base_url('index.php/User/main/dokumen'); ?>");
                    }
                } else {
                    window.confirm(out.message);
                }
            })
            a.preventDefault();
        });
    </script>

    <!-- AJAX BANK -->
    <script type="text/javascript">
        $(document).on('submit', '#modal-ref', function(a){
            var formData = new FormData($(this)[0]);
            $.ajax({
            method: 'POST',
            url: "<?= base_url('index.php/User/main/add_ref'); ?>",
            data: formData,
            processData: false,
            contentType: false
            }).done(function(data) {
                var out = jQuery.parseJSON(data);
                if (out.status == true) {
                    if(window.confirm(out.message)) {
                        window.location.replace("<?= base_url('index.php/User/main/dokumen'); ?>");
                    }
                } else {
                    window.confirm(out.message);
                }
            })
            a.preventDefault();
        });
    </script>

    <!-- AJAX AUDIT -->
    <script type="text/javascript">
        $(document).on('submit', '#modal-audit', function(a){
            var formData = new FormData($(this)[0]);
            $.ajax({
            method: 'POST',
            url: "<?= base_url('index.php/User/main/add_audit'); ?>",
            data: formData,
            processData: false,
            contentType: false
            }).done(function(data) {
                var out = jQuery.parseJSON(data);
                if (out.status == true) {
                    if(window.confirm(out.message)) {
                        window.location.replace("<?= base_url('index.php/User/main/dokumen'); ?>");
                    }
                } else {
                    window.confirm(out.message);
                }
            })
            a.preventDefault();
        });
    </script>

    <!-- AJAX MITRA -->
    <script type="text/javascript">
        $(document).on('submit', '#modal-mitra', function(a){
            var formData = new FormData($(this)[0]);
            $.ajax({
            method: 'POST',
            url: "<?= base_url('index.php/User/main/add_mitra'); ?>",
            data: formData,
            processData: false,
            contentType: false
            }).done(function(data) {
                var out = jQuery.parseJSON(data);
                if (out.status == true) {
                    if(window.confirm(out.message)) {
                        window.location.replace("<?= base_url('index.php/User/main/dokumen'); ?>");
                    }
                } else {
                    window.confirm(out.message);
                }
            })
            a.preventDefault();
        });
    </script>

    <!-- AJAX PERNYATAAN -->
    <script type="text/javascript">
        $(document).on('submit', '#modal-pernyataan', function(a){
            var formData = new FormData($(this)[0]);
            $.ajax({
            method: 'POST',
            url: "<?= base_url('index.php/User/main/add_pernyataan'); ?>",
            data: formData,
            processData: false,
            contentType: false
            }).done(function(data) {
                var out = jQuery.parseJSON(data);
                if (out.status == true) {
                    if(window.confirm(out.message)) {
                        window.location.replace("<?= base_url('index.php/User/main/dokumen'); ?>");
                    }
                } else {
                    window.confirm(out.message);
                }
            })
            a.preventDefault();
        });
    </script>

    <!-- AJAX PI -->
    <script type="text/javascript">
        $(document).on('submit', '#modal-pi', function(a){
            var formData = new FormData($(this)[0]);
            $.ajax({
            method: 'POST',
            url: "<?= base_url('index.php/User/main/add_pi'); ?>",
            data: formData,
            processData: false,
            contentType: false
            }).done(function(data) {
                var out = jQuery.parseJSON(data);
                if (out.status == true) {
                    if(window.confirm(out.message)) {
                        window.location.replace("<?= base_url('index.php/User/main/dokumen'); ?>");
                    }
                } else {
                    window.confirm(out.message);
                }
            })
            a.preventDefault();
        });
    </script>

    <!-- AJAX PTPP -->
    <script type="text/javascript">
        $(document).on('submit', '#modal-ptpp', function(a){
            var formData = new FormData($(this)[0]);
            $.ajax({
            method: 'POST',
            url: "<?= base_url('index.php/User/main/add_ptpp'); ?>",
            data: formData,
            processData: false,
            contentType: false
            }).done(function(data) {
                var out = jQuery.parseJSON(data);
                if (out.status == true) {
                    if(window.confirm(out.message)) {
                        window.location.replace("<?= base_url('index.php/User/main/dokumen'); ?>");
                    }
                } else {
                    window.confirm(out.message);
                }
            })
            a.preventDefault();
        });
    </script>

    <!-- AJAX KEMAMPUAN -->
    <script type="text/javascript">
        $(document).on('submit', '#modal-kemampuan', function(a){
            var formData = new FormData($(this)[0]);
            $.ajax({
            method: 'POST',
            url: "<?= base_url('index.php/User/main/add_kemampuan'); ?>",
            data: formData,
            processData: false,
            contentType: false
            }).done(function(data) {
                var out = jQuery.parseJSON(data);
                if (out.status == true) {
                    if(window.confirm(out.message)) {
                        window.location.replace("<?= base_url('index.php/User/main/dokumen'); ?>");
                    }
                } else {
                    window.confirm(out.message);
                }
            })
            a.preventDefault();
        });
    </script>

    <!-- AJAX CSMS -->
    <script type="text/javascript">
        $(document).on('submit', '#modal-csms', function(a){
            var formData = new FormData($(this)[0]);
            $.ajax({
            method: 'POST',
            url: "<?= base_url('index.php/User/main/add_csms'); ?>",
            data: formData,
            processData: false,
            contentType: false
            }).done(function(data) {
                var out = jQuery.parseJSON(data);
                if (out.status == true) {
                    if(window.confirm(out.message)) {
                        window.location.replace("<?= base_url('index.php/User/main/dokumen'); ?>");
                    }
                } else {
                    window.confirm(out.message);
                }
            })
            a.preventDefault();
        });
    </script>

    <!-- AJAX CP -->
    <script type="text/javascript">
        $(document).on('submit', '#modal-cp', function(a){
            var formData = new FormData($(this)[0]);
            $.ajax({
            method: 'POST',
            url: "<?= base_url('index.php/User/main/add_cp'); ?>",
            data: formData,
            processData: false,
            contentType: false
            }).done(function(data) {
                var out = jQuery.parseJSON(data);
                if (out.status == true) {
                    if(window.confirm(out.message)) {
                        window.location.replace("<?= base_url('index.php/User/main/dokumen'); ?>");
                    }
                } else {
                    window.confirm(out.message);
                }
            })
            a.preventDefault();
        });
    </script>

    <!-- AJAX LOGO -->
    <script type="text/javascript">
        $(document).on('submit', '#modal-logo', function(a){
            var formData = new FormData($(this)[0]);
            $.ajax({
            method: 'POST',
            url: "<?= base_url('index.php/User/main/add_logo'); ?>",
            data: formData,
            processData: false,
            contentType: false
            }).done(function(data) {
                var out = jQuery.parseJSON(data);
                if (out.status == true) {
                    if(window.confirm(out.message)) {
                        window.location.replace("<?= base_url('index.php/User/main/dokumen'); ?>");
                    }
                } else {
                    window.confirm(out.message);
                }
            })
            a.preventDefault();
        });
    </script>

    <!-- AJAX EDIT AKTA -->
  <script type="text/javascript">
        $(document).on('submit', '#modal-edit-akta', function(a){
            var formData = new FormData($(this)[0]);
            $.ajax({
            method: 'POST',
            url: "<?= base_url('index.php/User/main/edit_akta'); ?>",
            data: formData,
            processData: false,
            contentType: false
            }).done(function(data) {
                var out = jQuery.parseJSON(data);
                if (out.status == true) {
                    if(window.confirm(out.message)) {
                        window.location.replace("<?= base_url('index.php/User/main/dokumen'); ?>");
                    }
                } else {
                    window.confirm(out.message);
                }
            })
            a.preventDefault();
        });
    </script>

  <!-- AJAX EDIT KEMENKUMHAM -->
  <script type="text/javascript">
        $(document).on('submit', '#modal-edit-kemenkumham', function(a){
            var formData = new FormData($(this)[0]);
            $.ajax({
            method: 'POST',
            url: "<?= base_url('index.php/User/main/edit_kemenkumham'); ?>",
            data: formData,
            processData: false,
            contentType: false
            }).done(function(data) {
                var out = jQuery.parseJSON(data);
                if (out.status == true) {
                    if(window.confirm(out.message)) {
                        window.location.replace("<?= base_url('index.php/User/main/dokumen'); ?>");
                    }
                } else {
                    window.confirm(out.message);
                }
            })
            a.preventDefault();
        });
    </script>

  <!-- AJAX EDIT SIUP -->
  <script type="text/javascript">
        $(document).on('submit', '#modal-edit-siup', function(a){
            var formData = new FormData($(this)[0]);
            $.ajax({
            method: 'POST',
            url: "<?= base_url('index.php/User/main/edit_siup'); ?>",
            data: formData,
            processData: false,
            contentType: false
            }).done(function(data) {
                var out = jQuery.parseJSON(data);
                if (out.status == true) {
                    if(window.confirm(out.message)) {
                        window.location.replace("<?= base_url('index.php/User/main/dokumen'); ?>");
                    }
                } else {
                    window.confirm(out.message);
                }
            })
            a.preventDefault();
        });
    </script>

  <!-- AJAX EDIT TDP -->
  <script type="text/javascript">
        $(document).on('submit', '#modal-edit-tdp', function(a){
            var formData = new FormData($(this)[0]);
            $.ajax({
            method: 'POST',
            url: "<?= base_url('index.php/User/main/edit_tdp'); ?>",
            data: formData,
            processData: false,
            contentType: false
            }).done(function(data) {
                var out = jQuery.parseJSON(data);
                if (out.status == true) {
                    if(window.confirm(out.message)) {
                        window.location.replace("<?= base_url('index.php/User/main/dokumen'); ?>");
                    }
                } else {
                    window.confirm(out.message);
                }
            })
            a.preventDefault();
        });
    </script>

  <!-- AJAX EDIT NPWP -->
  <script type="text/javascript">
        $(document).on('submit', '#modal-edit-npwp', function(a){
            var formData = new FormData($(this)[0]);
            $.ajax({
            method: 'POST',
            url: "<?= base_url('index.php/User/main/edit_npwp'); ?>",
            data: formData,
            processData: false,
            contentType: false
            }).done(function(data) {
                var out = jQuery.parseJSON(data);
                if (out.status == true) {
                    if(window.confirm(out.message)) {
                        window.location.replace("<?= base_url('index.php/User/main/dokumen'); ?>");
                    }
                } else {
                    window.confirm(out.message);
                }
            })
            a.preventDefault();
        });
    </script>

  <!-- AJAX EDIT SPPKP -->
  <script type="text/javascript">
        $(document).on('submit', '#modal-edit-sppkp', function(a){
            var formData = new FormData($(this)[0]);
            $.ajax({
            method: 'POST',
            url: "<?= base_url('index.php/User/main/edit_sppkp'); ?>",
            data: formData,
            processData: false,
            contentType: false
            }).done(function(data) {
                var out = jQuery.parseJSON(data);
                if (out.status == true) {
                    if(window.confirm(out.message)) {
                        window.location.replace("<?= base_url('index.php/User/main/dokumen'); ?>");
                    }
                } else {
                    window.confirm(out.message);
                }
            })
            a.preventDefault();
        });
    </script>

  <!-- AJAX EDIT SITU -->
  <script type="text/javascript">
        $(document).on('submit', '#modal-edit-situ', function(a){
            var formData = new FormData($(this)[0]);
            $.ajax({
            method: 'POST',
            url: "<?= base_url('index.php/User/main/edit_situ'); ?>",
            data: formData,
            processData: false,
            contentType: false
            }).done(function(data) {
                var out = jQuery.parseJSON(data);
                if (out.status == true) {
                    if(window.confirm(out.message)) {
                        window.location.replace("<?= base_url('index.php/User/main/dokumen'); ?>");
                    }
                } else {
                    window.confirm(out.message);
                }
            })
            a.preventDefault();
        });
    </script>

    <!-- AJAX EDIT BANK -->
    <script type="text/javascript">
        $(document).on('submit', '#modal-edit-ref', function(a){
            var formData = new FormData($(this)[0]);
            $.ajax({
            method: 'POST',
            url: "<?= base_url('index.php/User/main/edit_ref'); ?>",
            data: formData,
            processData: false,
            contentType: false
            }).done(function(data) {
                var out = jQuery.parseJSON(data);
                if (out.status == true) {
                    if(window.confirm(out.message)) {
                        window.location.replace("<?= base_url('index.php/User/main/dokumen'); ?>");
                    }
                } else {
                    window.confirm(out.message);
                }
            })
            a.preventDefault();
        });
    </script>

    <!-- AJAX EDIT AUDIT -->
    <script type="text/javascript">
        $(document).on('submit', '#modal-edit-audit', function(a){
            var formData = new FormData($(this)[0]);
            $.ajax({
            method: 'POST',
            url: "<?= base_url('index.php/User/main/edit_audit'); ?>",
            data: formData,
            processData: false,
            contentType: false
            }).done(function(data) {
                var out = jQuery.parseJSON(data);
                if (out.status == true) {
                    if(window.confirm(out.message)) {
                        window.location.replace("<?= base_url('index.php/User/main/dokumen'); ?>");
                    }
                } else {
                    window.confirm(out.message);
                }
            })
            a.preventDefault();
        });
    </script>

    <!-- AJAX AWARDS -->
    <script type="text/javascript">
        $(document).on('submit', '#modal-awards', function(a){
            var formData = new FormData($(this)[0]);
            $.ajax({
            method: 'POST',
            url: "<?= base_url('index.php/User/main/add_awards'); ?>",
            data: formData,
            processData: false,
            contentType: false
            }).done(function(data) {
                var out = jQuery.parseJSON(data);
                if (out.status == true) {
                    if(window.confirm(out.message)) {
                        window.location.replace("<?= base_url('index.php/User/main/awards'); ?>");
                    }
                } else {
                    window.confirm(out.message);
                }
            })
            a.preventDefault();
        });
    </script>

    <!-- AJAX EDIT AWARDS -->
    <script type="text/javascript">
        $(document).on('submit', '#modal-edit-awards', function(a){
            var formData = new FormData($(this)[0]);
            $.ajax({
            method: 'POST',
            url: "<?= base_url('index.php/User/main/edit_awards'); ?>",
            data: formData,
            processData: false,
            contentType: false
            }).done(function(data) {
              window.location.replace("<?= base_url('index.php/User/main/awards'); ?>");
                // var out = jQuery.parseJSON(data);
                // if (out.status == true) {
                //     if(window.confirm(out.message)) {
                //         window.location.replace("<?= base_url('index.php/User/main/awards'); ?>");
                //     }
                // } else {
                //     window.confirm(out.message);
                // }
            })
            a.preventDefault();
        });
    </script>

    <script>
    function isNumberKey(evt)
		{
			var charCode = (evt.which) ? evt.which : evt.keyCode;
			if (charCode != 46 && charCode > 31 
			&& (charCode < 48 || charCode > 57))
			return false;
			return true;
		}  
    </script> 

</body>

</html>
