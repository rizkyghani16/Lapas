<div class="d-sm-flex align-items-center justify-content-between mb-4">
	<h1 class="h3 mb-0 text-gray-800">Permohonan</h1>

    <button data-toggle="modal" data-target="#modal_add_new" class="btn btn-outline-success btn-md float-right"><i class="fas fa-fw fa-plus"></i> Tambah</button>
</div>
	
<?= $this->session->flashdata('message') ?>
	<div class="card shadow mb-4">
		<div class="card-body">
			<div class="table-responsive">
				<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th style="width: 2rem;" class="text-center">No</th>
							<th>Jenis Permohonan</th>
							<th class="text-center">Aksi</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
                            <th style="width: 2rem;" class="text-center">No</th>
							<th>Jenis Permohonan</th>
							<th class="text-center">Aksi</th>
						</tr>
					</tfoot>
					<tbody>
                        <?php
                        $no = 1;
                        foreach ($per->result_array() as $i) :
                            $permo_id = $i['permo_id'];
                            $nama_permohonan = $i['nama_permohonan'];
                        ?>
                            <tr>
                                <td style="width: 2rem;" class="text-center"><?php echo $no++; ?></td>
                                <td><?php echo $nama_permohonan; ?></td>
                                <td class="text-center"><a data-toggle="modal" data-target="#modal_hapus<?php echo $permo_id; ?>" style="color: white;" class="btn btn-sm btn-danger"><i class="fas fa-trash-alt"></i> Hapus</a></td>
                        <?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>

<!-- ============ MODAL ADD =============== -->
<div class="modal fade" id="modal_add_new" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="modal_add_new">Tambah Permohonan</h3>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
            </div>
            <form action="<?php echo base_url() . 'admin/main/add_permo'; ?>" method="POST">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="nama_permohonan">Jenis Permohonan</label>
                        <input type="text" class="form-control" id="nama_permohonan" name="nama_permohonan" placeholder="Tuliskan jenis permohonan" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                    <button class="btn btn-info">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!--END MODAL ADD-->

<!-- ============ MODAL HAPUS =============== -->
<?php
foreach ($per->result_array() as $i) :
    $permo_id = $i['permo_id'];
    $nama_permohonan = $i['nama_permohonan'];
?>
    <div class="modal fade" id="modal_hapus<?php echo $permo_id; ?>" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="modal_hapus">Persetujuan</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                </div>
                <form action="<?php echo base_url() . 'admin/main/hapus_permo'; ?>" method="post">
                    <div class="modal-body">

                        <input type="text" class="form-control" id="permo_id" name="permo_id" value="<?php echo $permo_id; ?>" hidden="true">
                        <h6>Apakah anda yakin menghapus pengguna bernama <b><?php echo strtoupper($nama_permohonan); ?></b> ?</h6>
                    </div>

                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal" aria-hidden="true">Tidak</button>
                        <button class="btn btn-info">Yakin</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

<?php endforeach; ?>
<!--END MODAL HAPUS-->



