<div class="d-sm-flex align-items-center justify-content-between mb-4">
	<h1 class="h3 mb-0 text-gray-800">Detail Dokumen</h1>
</div>
	
	<div class="card shadow mb-4">
		<div class="card-body">
            <dl class="row">
                <dt class="col-sm-3">Nama Narapidana</dt>
                <dd class="col-sm-8"><?php echo strtoupper($nara['nama']);?></dd>

                <dt class="col-sm-3">Umur</dt>
                <dd class="col-sm-8"><?php echo strtoupper($nara['umur']);?> TAHUN</dd>

                <dt class="col-sm-3">Jenis Permohonan</dt>
                <dd class="col-sm-8"><?php echo strtoupper($nara['jenis']);?></dd>

                <dt class="col-sm-3">Nama Penjamin</dt>
                <dd class="col-sm-8"><?php echo strtoupper($nara['fullname']);?></dd>

                <dt class="col-sm-3">Dokumen Surat Penjamin</dt>
                <dd class="col-sm-8"><a class="btn btn-primary btn-sm view-surat w-25" href="<?php echo base_url() . 'uploads/surat/'.$file['surat']; ?>">Lihat Surat</a> <a class="btn btn-success btn-sm text-white" target="blank" href="<?php echo base_url() . 'uploads/surat/'.$file['surat']; ?>"><i class="fa fa-download"></i> Unduh</a></dd>
                
                <dt class="col-sm-3">Dokumen Foto Penjamin</dt>
                <dd class="col-sm-8"><a class="btn btn-primary btn-sm text-white w-25" data-toggle="modal" data-target="#modal_foto">Lihat Foto</a> <a class="btn btn-success btn-sm text-white" target="blank" href="<?php echo base_url() . 'uploads/foto/'.$file['foto']; ?>"><i class="fa fa-download"></i> Unduh</a></dd>
                
                <dt class="col-sm-3">Dokumen KTP Penjamin</dt>
                <dd class="col-sm-8"><a class="btn btn-primary btn-sm text-white w-25" data-toggle="modal" data-target="#modal_ktp">Lihat KTP</a> <a class="btn btn-success btn-sm text-white" target="blank" href="<?php echo base_url() . 'uploads/ktp/'.$file['ktp']; ?>"><i class="fa fa-download"></i> Unduh</a></dd>

                <dt class="col-sm-3">Dokumen Kartu Keluarga</dt>
                <dd class="col-sm-8"><a class="btn btn-primary btn-sm view-surat w-25" href="<?php echo base_url() . 'uploads/kk/'.$file['kk']; ?>">Lihat KK</a> <a class="btn btn-success btn-sm text-white" target="blank" href="<?php echo base_url() . 'uploads/kk/'.$file['kk']; ?>"><i class="fa fa-download"></i> Unduh</a></dd>
                
                <dt class="col-sm-3">Status Dokumen</dt>
                <dd class="col-sm-8"><?php if($file['status']=="Pending") echo 'PENDING'; ?><?php if($file['status']=="Revision") echo 'REVISI'; ?><?php if($file['status']=="Verified") echo 'TELAH DISETUJII'; ?></dd>
            </dl>
                <button class="btn btn-info btn-block" data-toggle="modal" data-target="#modal_tanggapi">Tanggapi</button>
		</div>
	</div>

<!-- ============ MODAL FOTO =============== -->

    <div class="modal fade" id="modal_foto" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                </div>
                    <div class="modal-body">

                        <img class="img-responsive text-center" src="<?php echo base_url() . 'uploads/foto/'.$file['foto']; ?>" style="max-width:450px;">
                    </div>
            </div>
        </div>
    </div>
<!--END MODAL FOTO-->

<!-- ============ MODAL KTP =============== -->

    <div class="modal fade" id="modal_ktp" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                </div>
                    <div class="modal-body">

                        <img class="img-responsive text-center" src="<?php echo base_url() . 'uploads/ktp/'.$file['ktp']; ?>" style="max-width:450px;">
                    </div>
            </div>
        </div>
    </div>
<!--END MODAL KTP-->

<!-- ============ MODAL TANGGAPI =============== -->

<div class="modal fade" id="modal_tanggapi" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="modal_detail">Tanggapi Dokumen</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                </div>
                <form action="<?php echo base_url() . 'admin/main/update_dokumen'; ?>" method="post">
                    <div class="modal-body">

                        <input type="text" class="form-control" id="file_id" name="file_id" value="<?php echo $file['file_id']; ?>" hidden="true">
                        <input type="text" class="form-control" id="users_id" name="users_id" value="<?php echo $file['users_id']; ?>" hidden="true">
                        
                        <div class="form-group">
                            <label for="status">Status Dokumen</label>
                            <select class="form-control" id="status" name="status" required>
                                <option disabled value="">Pilih</option>
                                <option value="Pending" <?php if($file['status']=="Pending") echo 'selected="selected"'; ?> >Pending</option>
                                <option value="Revision" <?php if($file['status']=="Revision") echo 'selected="selected"'; ?> >Revisi</option>
                                <option value="Verified" <?php if($file['status']=="Verified") echo 'selected="selected"'; ?> >Setujui</option>
                            </select>
                        </div>
                        <div class="form-group">
                        	<label for="revisi_desc">Pesan Revisi</label>
                        	<textarea class="form-control" id="revisi_desc" name="revisi_desc" rows="3"></textarea>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal" aria-hidden="true">Urungkan</button>
                        <button class="btn btn-info">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<!--END MODAL TANGGAPI-->