<div class="d-sm-flex align-items-center justify-content-between mb-4">
	<h1 class="h3 mb-0 text-gray-800">Dokumen Pending</h1>
</div>
	
<?= $this->session->flashdata('message') ?>
	<div class="card shadow mb-4">
		<div class="card-body">
			<div class="table-responsive">
				<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th>Kode Dokumen</th>
							<th>Nama Penjamin</th>
							<th>Status</th>
							<th class="text-center">Aksi</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
                            <th>Kode Dokumen</th>
							<th>Nama Penjamin</th>
							<th>Status</th>
							<th class="text-center">Aksi</th>
						</tr>
					</tfoot>
					<tbody>
                        <?php
                        // $no = 1;
                        foreach ($pending->result_array() as $i) :
                            $file_id = $i['file_id'];
                            $pemohon = $i['fullname'];
                            $status = $i['status'];
                            $review = $i['review'];
                            $users_id = $i['users_id'];
                        ?>
                            <tr>
                                <td><?php echo $file_id; ?></td>
                                <td><?php echo $pemohon; ?></td>
                                <td><?php if($status=="Pending") echo 'Pending'; ?><?php if($status=="Revision") echo 'Revisi'; ?><?php if($status=="Verified") echo 'Telah Disetujui'; ?></td>
                                <td class="text-center"><?php if ($review == "False") : ?><a data-toggle="modal" data-target="#modal_tinjau<?php echo $file_id; ?>" class="btn btn-sm btn-success text-white"><i class="fas fa-eye"></i> Tinjau</a> <a href="<?php echo base_url() . 'admin/main/detail_dokumen/'.$users_id; ?>" class="btn btn-sm btn-info text-white"><i class="fas fa-info-circle"></i> Detail</a><?php else :?><a href="<?php echo base_url() . 'admin/main/detail_dokumen/'.$users_id; ?>" class="btn btn-sm btn-info text-white"><i class="fas fa-info-circle"></i> Detail</a><?php endif; ?></td>
                        <?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>

<div class="d-sm-flex align-items-center justify-content-between mb-4">
	<h1 class="h3 mb-0 text-gray-800">Dokumen Revisi</h1>
</div>
	
<?= $this->session->flashdata('message') ?>
	<div class="card shadow mb-4">
		<div class="card-body">
			<div class="table-responsive">
				<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th>Kode Dokumen</th>
							<th>Nama Penjamin</th>
							<th>Status</th>
							<th class="text-center">Aksi</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
                            <th>Kode Dokumen</th>
							<th>Nama Penjamin</th>
							<th>Status</th>
							<th class="text-center">Aksi</th>
						</tr>
					</tfoot>
					<tbody>
                        <?php
                        // $no = 1;
                        foreach ($revisi->result_array() as $i) :
                            $file_id = $i['file_id'];
                            $pemohon = $i['fullname'];
                            $status = $i['status'];
                            $review = $i['review'];
                            $users_id = $i['users_id'];
                        ?>
                            <tr>
                                <td><?php echo $file_id; ?></td>
                                <td><?php echo $pemohon; ?></td>
                                <td><?php if($status=="Pending") echo 'Pending'; ?><?php if($status=="Revision") echo 'Revisi'; ?><?php if($status=="Verified") echo 'Telah Disetujui'; ?></td>
                                <td class="text-center"><?php if ($review == "False") : ?><a data-toggle="modal" data-target="#modal_tinjau<?php echo $file_id; ?>" class="btn btn-sm btn-success text-white"><i class="fas fa-eye"></i> Tinjau</a> <a href="<?php echo base_url() . 'admin/main/detail_dokumen/'.$users_id; ?>" class="btn btn-sm btn-info text-white"><i class="fas fa-info-circle"></i> Detail</a><?php else :?><a href="<?php echo base_url() . 'admin/main/detail_dokumen/'.$users_id; ?>" class="btn btn-sm btn-info text-white"><i class="fas fa-info-circle"></i> Detail</a><?php endif; ?></td>
                        <?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>

<div class="d-sm-flex align-items-center justify-content-between mb-4">
	<h1 class="h3 mb-0 text-gray-800">Dokumen Disetujui</h1>
</div>
	
<?= $this->session->flashdata('message') ?>
	<div class="card shadow mb-4">
		<div class="card-body">
			<div class="table-responsive">
				<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th>Kode Dokumen</th>
							<th>Nama Penjamin</th>
							<th>Status</th>
							<th class="text-center">Aksi</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
                            <th>Kode Dokumen</th>
							<th>Nama Penjamin</th>
							<th>Status</th>
							<th class="text-center">Aksi</th>
						</tr>
					</tfoot>
					<tbody>
                        <?php
                        // $no = 1;
                        foreach ($verif->result_array() as $i) :
                            $file_id = $i['file_id'];
                            $pemohon = $i['fullname'];
                            $status = $i['status'];
                            $review = $i['review'];
                            $users_id = $i['users_id'];
                        ?>
                            <tr>
                                <td><?php echo $file_id; ?></td>
                                <td><?php echo $pemohon; ?></td>
                                <td><?php if($status=="Pending") echo 'Pending'; ?><?php if($status=="Revision") echo 'Revisi'; ?><?php if($status=="Verified") echo 'Telah Disetujui'; ?></td>
                                <td class="text-center"><?php if ($review == "False") : ?><a data-toggle="modal" data-target="#modal_tinjau<?php echo $file_id; ?>" class="btn btn-sm btn-success text-white"><i class="fas fa-eye"></i> Tinjau</a> <a href="<?php echo base_url() . 'admin/main/detail_dokumen/'.$users_id; ?>" class="btn btn-sm btn-info text-white"><i class="fas fa-info-circle"></i> Detail</a><?php else :?><a href="<?php echo base_url() . 'admin/main/detail_dokumen/'.$users_id; ?>" class="btn btn-sm btn-info text-white"><i class="fas fa-info-circle"></i> Detail</a><?php endif; ?></td>
                        <?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>

<!-- ============ MODAL TINJAU =============== -->
<?php
foreach ($pending->result_array() as $i) :
    $file_id = $i['file_id'];
    $pemohon = $i['fullname'];
?>
    <div class="modal fade" id="modal_tinjau<?php echo $file_id; ?>" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="modal_tinjau">Persetujuan</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                </div>
                <form action="<?php echo base_url() . 'admin/main/tinjau_dokumen'; ?>" method="post">
                    <div class="modal-body">

                        <input type="text" class="form-control" id="file_id" name="file_id" value="<?php echo $file_id; ?>" hidden="true">
                        <h6>Apakah anda yakin akan meninjau dokumen milik <b><?php echo strtoupper($pemohon); ?></b> ?</h6>
                    </div>

                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal" aria-hidden="true">Tidak</button>
                        <button class="btn btn-info">Yakin</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

<?php endforeach; ?>
<!--END MODAL TINJAU-->

<!-- ============ MODAL TINJAU =============== -->
<?php
foreach ($revisi->result_array() as $i) :
    $file_id = $i['file_id'];
    $pemohon = $i['fullname'];
?>
    <div class="modal fade" id="modal_tinjau<?php echo $file_id; ?>" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="modal_tinjau">Persetujuan</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                </div>
                <form action="<?php echo base_url() . 'admin/main/tinjau_dokumen'; ?>" method="post">
                    <div class="modal-body">

                        <input type="text" class="form-control" id="file_id" name="file_id" value="<?php echo $file_id; ?>" hidden="true">
                        <h6>Apakah anda yakin akan meninjau dokumen milik <b><?php echo strtoupper($pemohon); ?></b> ?</h6>
                    </div>

                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal" aria-hidden="true">Tidak</button>
                        <button class="btn btn-info">Yakin</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

<?php endforeach; ?>
<!--END MODAL TINJAU-->

<!-- ============ MODAL TINJAU =============== -->
<?php
foreach ($verif->result_array() as $i) :
    $file_id = $i['file_id'];
    $pemohon = $i['fullname'];
?>
    <div class="modal fade" id="modal_tinjau<?php echo $file_id; ?>" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="modal_tinjau">Persetujuan</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                </div>
                <form action="<?php echo base_url() . 'admin/main/tinjau_dokumen'; ?>" method="post">
                    <div class="modal-body">

                        <input type="text" class="form-control" id="file_id" name="file_id" value="<?php echo $file_id; ?>" hidden="true">
                        <h6>Apakah anda yakin akan meninjau dokumen milik <b><?php echo strtoupper($pemohon); ?></b> ?</h6>
                    </div>

                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal" aria-hidden="true">Tidak</button>
                        <button class="btn btn-info">Yakin</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

<?php endforeach; ?>
<!--END MODAL TINJAU-->



