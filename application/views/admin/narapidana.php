<div class="d-sm-flex align-items-center justify-content-between mb-4">
	<h1 class="h3 mb-0 text-gray-800">Narapidana</h1>
</div>
	
<?= $this->session->flashdata('message') ?>
	<div class="card shadow mb-4">
		<div class="card-body">
			<div class="table-responsive">
				<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th>Nama Narapidana</th>
							<th>Nama Penjamin</th>
							<th>Hubungan</th>
							<th>Jenis Permohonan</th>
							<th class="text-center">Aksi</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
                            <th>Nama Narapidana</th>
							<th>Nama Penjamin</th>
							<th>Hubungan</th>
							<th>Jenis Permohonan</th>
							<th class="text-center">Aksi</th>
						</tr>
					</tfoot>
					<tbody>
                        <?php
                        // $no = 1;
                        foreach ($nara->result_array() as $i) :
                            $nara_id = $i['nara_id'];
                            $nama_narapidana = $i['nama'];
                            $nara_umur = $i['umur'];
                            $nara_hubungan = $i['hubungan'];
                            $nara_jenis = $i['jenis'];
                            $pemohon = $i['fullname'];
                        ?>
                            <tr>
                                <td><?php echo $nama_narapidana; ?></td>
                                <td><?php echo $pemohon; ?></td>
                                <td><?php echo $nara_hubungan; ?></td>
                                <td><?php echo $nara_jenis; ?></td>
                                <td class="text-center"><a data-toggle="modal" data-target="#modal_hapus<?php echo $nara_id; ?>" class="btn btn-sm btn-danger text-white"><i class="fas fa-trash-alt"></i> Hapus</a> <a data-toggle="modal" data-target="#modal_detail<?php echo $nara_id; ?>" class="btn btn-sm btn-info text-white"><i class="fas fa-info-circle"></i> Detail</a></td>
                        <?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>

<!-- ============ MODAL HAPUS =============== -->
<?php
foreach ($nara->result_array() as $i) :
    $nara_id = $i['nara_id'];
    $nama_narapidana = $i['nama'];
    $nara_umur = $i['umur'];
    $nara_hubungan = $i['hubungan'];
    $nara_jenis = $i['jenis'];
    $pemohon = $i['fullname'];
?>
    <div class="modal fade" id="modal_hapus<?php echo $nara_id; ?>" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="modal_hapus">Persetujuan</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                </div>
                <form action="<?php echo base_url() . 'admin/main/hapus_nara'; ?>" method="post">
                    <div class="modal-body">

                        <input type="text" class="form-control" id="nara_id" name="nara_id" value="<?php echo $nara_id; ?>" hidden="true">
                        <h6>Apakah anda yakin menghapus narapidana <b><?php echo strtoupper($nama_narapidana); ?></b> ?</h6>
                    </div>

                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal" aria-hidden="true">Tidak</button>
                        <button class="btn btn-info">Yakin</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

<?php endforeach; ?>
<!--END MODAL HAPUS-->

<!-- ============ MODAL DETAIL =============== -->
<?php
foreach ($nara->result_array() as $i) :
    $nara_id = $i['nara_id'];
    $nama_narapidana = $i['nama'];
    $nara_umur = $i['umur'];
    $nara_hubungan = $i['hubungan'];
    $nara_jenis = $i['jenis'];
    $pemohon = $i['fullname'];
?>
    <div class="modal fade" id="modal_detail<?php echo $nara_id; ?>" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="modal_detail">Detail Narapidana</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                </div>
                <form action="<?php echo base_url() . 'admin/main/update_nara'; ?>" method="post">
                    <div class="modal-body">

                        <input type="text" class="form-control" id="nara_id" name="nara_id" value="<?php echo $nara_id; ?>" hidden="true">
                        <div class="form-group">
                            <label for="nama">Nama Narapidana</label>
                            <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama lengkap narapidana" value="<?php echo strtoupper($nama_narapidana); ?>" required>
                        </div>
                        <div class="form-row">
                        	<div class="form-group col-12">
                        		<label for="umur">Umur</label>
                        		<div class="input-group">
                        			<input type="text" class="form-control" id="umur" name="umur"
                        				placeholder="Contoh : 35" value="<?php echo ($nara_umur); ?>" aria-label="Umur"
                        				aria-describedby="basic-addon2">
                        			<div class="input-group-append">
                        				<span class="input-group-text" id="basic-addon2">Tahun</span>
                        			</div>
                        		</div>
                        	</div>
                        </div>
                        <div class="form-group">
                            <label for="pemohon">Nama Penjamin</label>
                            <input type="text" class="form-control" id="pemohon" name="pemohon" placeholder="Nama lengkap narapidana" value="<?php echo strtoupper($pemohon); ?>" required readonly>
                        </div>
                        <div class="form-group">
                            <label for="hubungan">Hubungan</label>
                            <select class="form-control" id="hubungan" name="hubungan" required>
                                <option disabled value="">Pilih</option>
                                <option value="Keluarga" <?php if($nara_hubungan=="Keluarga") echo 'selected="selected"'; ?> >Keluarga</option>
                                <option value="Kerabat" <?php if($nara_hubungan=="Kerabat") echo 'selected="selected"'; ?> >Kerabat</option>
                                <option value="Wali" <?php if($nara_hubungan=="Wali") echo 'selected="selected"'; ?> >Wali</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="jenis">Jenis Permohonan</label>
                            <select class="form-control" id="jenis" name="jenis" required>
                                <option disabled value="">Pilih</option>
                                <option value="Pembebasan Bersyarat" <?php if($nara_jenis=="Pembebasan Bersyarat") echo 'selected="selected"'; ?> >Pembebasan Bersyarat</option>
                                <option value="Cuti Menjelang Bebas" <?php if($nara_jenis=="Cuti Menjelang Bebas") echo 'selected="selected"'; ?> >Cuti Menjelang Bebas</option>
                                <option value="Cuti Bersyarat" <?php if($nara_jenis=="Cuti Bersyarat") echo 'selected="selected"'; ?> >Cuti Bersyarat</option>
                                <option value="Justice Collaborator" <?php if($nara_jenis=="Justice Collaborator") echo 'selected="selected"'; ?> >Justice Collaborator</option>
                                <option value="Asimilasi" <?php if($nara_jenis=="Asimilasi") echo 'selected="selected"'; ?> >Asimilasi</option>
                            </select>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal" aria-hidden="true">Tidak</button>
                        <button class="btn btn-info">Ubah</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

<?php endforeach; ?>
<!--END MODAL DETAIL-->



