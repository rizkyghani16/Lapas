
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width">
        <link rel="icon" href="<?= base_url(); ?>assets/img/cropped-1-2-32x32.png" sizes="32x32">
        <link rel="icon" href="<?= base_url(); ?>assets/img/cropped-1-2-192x192.png" sizes="192x192">
        <link rel="apple-touch-icon" href="<?= base_url(); ?>assets/img/cropped-1-2-180x180.png">
        <title class="font-mono">Rutan Kelas I Bandung</title>
        <link rel="stylesheet" href="<?= base_url(); ?>assets/index/css/tailwind.min.css">
        <link rel="stylesheet" href="<?= base_url(); ?>assets/index/css/animate.min.css">
        <script src="<?= base_url(); ?>assets/index/js/wow.min.js"></script>
        <link rel="stylesheet" href="<?= base_url(); ?>assets/index/css/all.min.css">
        <script>new WOW().init();</script>
    </head>
    <body class>
        <div class="lg:flex">
            <div class="lg:w-1/3 xl:max-w-screen-sm">
                <div class="py-12 bg-indigo-100 lg:bg-white flex justify-center lg:justify-start lg:px-12">
                    <div class="cursor-pointer flex items-center">
                        <div>
                            <!-- <svg class="w-10 text-indigo-500" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 225 225" style="enable-background:new 0 0 225 225;" xml:space="preserve">
                                <style type="text/css">
                                    .st0{fill:none;stroke:currentColor;stroke-width:20;stroke-linecap:round;stroke-miterlimit:3;}
                                </style>
                                <g transform="matrix( 1, 0, 0, 1, 0,0) ">
                                <g>
                                <path id="Layer0_0_1_STROKES" class="st0" d="M173.8,151.5l13.6-13.6 M35.4,89.9l29.1-29 M89.4,34.9v1 M137.4,187.9l-0.6-0.4     M36.6,138.7l0.2-0.2 M56.1,169.1l27.7-27.6 M63.8,111.5l74.3-74.4 M87.1,188.1L187.6,87.6 M110.8,114.5l57.8-57.8"/>
                                </g>
                                </g>
                            </svg> -->
                        </div>
                        <div class="text-2xl text-indigo-600 tracking-wide ml-2 font-semibold"><a href="<?= base_url(); ?>">Rutan Kelas I Bandung</a></div>
                    </div>
                </div>
                <div class="mt-10 px-8 sm:px-20 md:px-44 lg:px-8 lg:mt-16 xl:px-20 xl:max-w-2xl">
                    <h2 class="text-center text-4xl text-indigo-600 font-display font-semibold lg:text-left xl:text-5xl
                    xl:text-bold">Masuk</h2>
                    <p class="text-left text-gray-500 font-regular tracking-widest leading-loose pt-4">Masuk untuk mengelola dokumen anda</p>
                    <div class="mt-12">
                        <form action="<?= base_url(); ?>proses_login" method="POST" enctype="multipart/form-data">
                            <div>
                                <div class="text-sm font-bold text-gray-700 tracking-wide">Email</div>
                                <input class="w-full text-lg py-2 border-b border-gray-300 focus:outline-none focus:border-indigo-500" type="email" name="email" placeholder="contoh@contoh.com">
                            </div>
                            <div class="mt-8 relative">
                                <div class="flex justify-between items-center">
                                    <div class="text-sm font-bold text-gray-700 tracking-wide">
                                        Kata Sandi
                                    </div>
                                    <div>
                                        <a href="<?= base_url(); ?>lupa-kata-sandi" class="text-xs font-display font-semibold text-indigo-600 hover:text-indigo-800
                                        cursor-pointer">
                                            Lupa Kata Sandi?
                                        </a>
                                    </div>
                                </div>
                                <input class="w-full text-lg py-2 border-b border-gray-300 focus:outline-none focus:border-indigo-500" type="password" id="password" name="password" placeholder="Masukan kata sandi">
                                <div class="absolute inset-y-0 right-0 flex items-center px-4 mt-5">
                                    <i class="far fa-eye text-gray-700" id="togglePassword"></i>
                                </div>
                            </div>
                            <div class="mt-10">
                                <button class="bg-indigo-600 text-gray-100 p-4 w-full rounded-full tracking-wide
                                font-semibold font-display focus:outline-none focus:shadow-outline hover:bg-indigo-600
                                shadow-lg">
                                    Masuk
                                </button>
                            </div>
                            <div class="mt-4 text-sm font-display font-semibold text-gray-700 text-center">
                            Belum punya akun ? <a href="<?= base_url(); ?>register" class="cursor-pointer text-indigo-600 hover:text-indigo-800">Daftar disini</a>
                            </div>
                        </form>
                        
                    </div>
                </div>
            </div>
            <div class="hidden lg:flex items-center justify-center bg-gray-200 flex-1 h-screen">
                <div class="max-w-4xl transform duration-200 hover:scale-110 cursor-pointer wow fadeInDown">
                    <img src="<?= base_url(); ?>assets/img/login2.svg" alt="Masuk">
                </div>
            </div>
        </div>

        <script Type="text/javascript">
            togglePassword.addEventListener('click', function (e) {
                // toggle the type attribute
                const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
                password.setAttribute('type', type);
                // toggle the eye slash icon
                this.classList.toggle('fa-eye-slash');
            });
        </script>
    </body>
</html>
