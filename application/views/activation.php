
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="<?= base_url(); ?>assets/img/cropped-1-2-32x32.png" sizes="32x32">
    <link rel="icon" href="<?= base_url(); ?>assets/img/cropped-1-2-192x192.png" sizes="192x192">
    <link rel="apple-touch-icon" href="<?= base_url(); ?>assets/img/cropped-1-2-180x180.png">
    <title class="font-mono">Rutan Kelas I Bandung</title>
    <link rel="stylesheet" href="<?= base_url(); ?>assets/index/css/tailwind.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/index/css/animate.min.css">
    <script src="<?= base_url(); ?>assets/index/js/wow.min.js"></script>
    
    <script>new WOW().init();</script>
    <style>
        html {
            scroll-behavior: smooth;
        }
    </style>
</head>
<body>

<header class="lg:px-8 flex items-center w-full justify-between flex-wrap bg-white fixed w-full z-10 top-0 py-5 mx-auto md:flex-row " id="navBar2">
    
    <div class="flex items-center flex-shrink-0 text-white mr-6">
    	<a href="#"
    		class="relative left-0 z-10 flex lg:items-center lg:leading-none select-none ml-4"><img src="<?= base_url(); ?>assets/img/rutan-bdg.svg" style="height: 25%;"></a>
    </div>
        
        
    <div class="block lg:hidden">
    	<button
    		class="flex items-right px-3 py-2 border rounded text-indigo-600 border-indigo-600 hover:indigo-700 hover:border-indigo-700 mr-4"
    		id="navbar-btn">
    		<svg class="fill-current h-3 w-3" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
    			<title>Menu</title>
    			<path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z" />
    		</svg>
    	</button>
    </div>
    <div class="w-full block flex-grow lg:flex lg:items-center lg:w-auto block lg:hidden" id="navbar">
    	<div class="text-sm lg:flex-grow  text-center lg:text-right">
    		<a href="<?php echo base_url(); ?>"
    			class="block mt-4 lg:inline-block lg:mt-0 text-gray-700 hover:text-indigo-600 lg:mr-3 text-lg">
    			Home
    		</a>
    		<a href="<?php echo base_url().'#cara'; ?>"
    			class="block mt-4 lg:inline-block lg:mt-0 text-gray-700 hover:text-indigo-600 lg:mr-3 text-lg">
    			Cara Penggunaan
    		</a>
    		<a href="<?php echo base_url().'#tentang'; ?>"
    			class="block mt-4 lg:inline-block lg:mt-0 text-gray-700 hover:text-indigo-600 lg:mr-3 text-lg">
    			Tentang
    		</a>
    		<a href="<?php echo base_url().'track'; ?>"
    			class="block mt-4 lg:inline-block lg:mt-0 text-gray-700 hover:text-indigo-600 text-lg">
    			Lacak Dokumen
    		</a>
    		<a href="<?= base_url(); ?>login"
    			class="inline-block text-sm px-6 py-2 leading-none border rounded text-white text-base font-normal leading-6 bg-indigo-600 border-indigo-600 hover:border-indigo-700 hover:text-white hover:bg-indigo-700 mt-4 lg:mt-0 ml-3">Masuk</a>
    	</div>
    </div>
</header>

<section class="px-2 py-32 bg-white md:px-0 relative items-center justify-center">
    <div class="text-center md:pb-20 pb-8 lg:pb-0">
        <span class="text-indigo-600 text-xl font-semibold lg:text-3xl md:text-2xl">Aktifasi Akun</span>
        <img class="object-none object-center w-full" src="<?= base_url(); ?>assets/img/garis.svg">
    </div>
    
    <div class="flex items-center justify-center md:mt-10">
    	<form class="w-full max-w-lg items-center justify-center" action="<?php echo base_url().'activate'; ?>" method="post">

    		<div class="flex flex-wrap mx-2 mb-6">
    			<div class="w-full md:w-4/5 md:px-3 mb-4 md:mb-0">
    				
                    <input
        				class="bg-gray-200 w-full appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
        				id="u_reg" name="u_reg" type="text" placeholder="Masukan kode aktifasi anda disini">
    			</div>
    			<div class="w-full md:w-1/5">
                    <button
        				class="shadow w-full bg-indigo-600 hover:bg-indigo-700 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded"
        				type="submit">
        				Submit
        			</button>
    			</div>
    		</div>

    	</form>
    </div>
</section>

<footer class="bg-gray-100" id="tentang">
      <div class="container mx-auto px-6 pt-10 pb-6">
        <div class="flex flex-wrap items-center text-center">
          <div class="w-full md:w-1/3 text-center md:text-center justify-center mb-4">
            <div class="flex flex-row justify-center">
                <div class="items-center content-center px-3">
                    <img class="object-center" src="<?= base_url(); ?>assets/img/pemasyarakatan.svg" alt="Logo Pemasyarakatan">
                </div>
                <div class="items-center content-center px-3">
                    <img class="object-center" src="<?= base_url(); ?>assets/img/pengayoman.svg" alt="Logo Pengayoman">
                </div>
            </div>
            <!-- <div class="absolute pt-0 mr-5"></div>
            <div class="absolute pl-20 pt-0"></div> -->
          </div>
          <div class="w-full md:w-1/3 text-center md:text-center justify-center ">
            <h5 class="uppercase mb-6 font-bold">Rutan Kelas I Bandung</h5>
            <p class="text-left md:mx-20 font-regular text-gray-700 pb-10">Jl. Jakarta No 29 Kel. Kebonwaru, Kec. Batununggal, Kota Bandung, Jawa Barat 40272</p>
          </div>
          <div class="w-full md:w-1/3 text-center md:text-center justify-end">
            <h5 class="uppercase mb-6 font-bold">Hubungi Kami</h5>
            <div class="flex flex-wrap justify-between">
                <div class="md:w-1/2">
                    <ul class="mb-4 md:ml-20">
                        <li class="mt-2">
                            <a href="tel:0227275770" class="hover:underline text-gray-600 hover:text-orange-500"><img src="<?= base_url(); ?>assets/img/tel.svg"></a>
                        </li>
                        <li class="mt-2">
                            <a href="mailto:humas@rutankelas1bandung.com" class="hover:underline text-gray-600 hover:text-orange-500"><img src="<?= base_url(); ?>assets/img/mail.svg"></a>
                        </li>
                    </ul>
                </div>
                <div class="md:w-1/2">
                    <ul class="mb-4 md:ml-8">
                        <li class="mt-2">
                            <a href="https://www.facebook.com/rutanklas1bandung" class="hover:underline text-gray-600 hover:text-orange-500"><img src="<?= base_url(); ?>assets/img/fb.svg"></a>
                        </li>
                        <li class="mt-2">
                            <a href="https://www.instagram.com/rutanbandungjuara/" class="hover:underline text-gray-600 hover:text-orange-500"><img src="<?= base_url(); ?>assets/img/ig.svg"></a>
                        </li>
                        <li class="mt-2">
                            <a href="https://twitter.com/rutanbdg" class="hover:underline text-gray-600 hover:text-orange-500"><img src="<?= base_url(); ?>assets/img/tw.svg"></a>
                        </li>
                    </ul>
                </div>
            </div>
          </div>
        </div>
      </div>
      <div class="bg-indigo-600 py-5">
        <p class="lg:text-base lg:leading-6 text-center text-indigo-100">
            &copy; 2021 Rutan Kelas I Bandung. All rights reserved.
        </p>
      </div>
    </footer>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/alpinejs/2.8.0/alpine.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <script>
        $(window).scroll(function() {
            if ($(window).scrollTop() > 10) {
                $('#navBar2').addClass('shadow-md');
            } else {
                $('#navBar2').removeClass('shadow-md');
            }
        });
    </script>
    <script>
    $(document).ready(function () {

		    var $window = $(window);

		    function checkWindowWidth() {
		    	
		        var windowsize = $window.width();

		        if (windowsize > 1000) {

		            $('#navbar').css('display','block');

		        } else {

		            $('#navbar').css('display','none');

		        }
		    }

		    checkWindowWidth();

		    $(window).resize(checkWindowWidth);

		});


      $('#navbar-btn').click(function() {
        $('#navbar').toggle();
      });
    </script>
</body>
</html>