<div class="d-sm-flex align-items-center justify-content-between mb-4">
	<h1 class="h3 mb-0 text-gray-800">Profil</h1>
</div>

<div class="card shadow h-100 mb-5">
	<?= $this->session->flashdata('message'); ?>
	<div class="container emp-profile">
		<div class="row">
			<div class="col-md-3">
				<div class="profile-img">
					<img class="img-profile img-center" src="<?php echo base_url(); ?>assets/img/default.jpg">
				</div>
			</div>
			<div class="col-md-8">
				<div class="profile-head mt-sm-3">
					<h4 class="text-center text-md-left">
						<?php echo $user['fullname']; ?>
					</h4>
					<h5 class="text-center text-md-left">
						<?php echo $user['job']; ?>
					</h5>
					<ul class="nav nav-tabs mt-md-5" id="myTab" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" id="tentang-tab" data-toggle="tab" href="#tentang" role="tab"
								aria-controls="tentang" aria-selected="true">Tentang</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="password-tab" data-toggle="tab" href="#password" role="tab"
								aria-controls="password" aria-selected="false">Password</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">

			</div>
			<div class="col-md-8">
				<div class="tab-content profile-tab mt-2" id="myTabContent">
					<div class="tab-pane fade show active" id="tentang" role="tabpanel" aria-labelledby="tentang-tab">
						<div class="row">
							<div class="col-md-3">
								<label>Email</label>
							</div>
							<div class="col-md-6">
								<p><?php echo $user['email']; ?></p>
							</div>
						</div>
						<div class="row">
							<div class="col-md-3">
								<label>Jenis Kelamin</label>
							</div>
							<div class="col-md-6">
								<p><?php echo $user['gender']; ?></p>
							</div>
						</div>
						<div class="row">
							<div class="col-md-3">
								<label>Umur</label>
							</div>
							<div class="col-md-6">
								<p><?php echo $user['age']; ?> Tahun</p>
							</div>
						</div>
						<div class="row">
							<div class="col-md-3">
								<label>Alamat</label>
							</div>
							<div class="col-md-6">
								<p><?php echo $user['address']; ?></p>
							</div>
						</div>
						<div class="row">
							<div class="col-md-3">
								<label>Telephone</label>
							</div>
							<div class="col-md-6">
								<p><?php echo $user['tel']; ?></p>
							</div>
						</div>

					</div>
					<div class="tab-pane fade" id="password" role="tabpanel" aria-labelledby="password-tab">
						<div class="container">
                            <form action="<?php echo base_url().'ganti_katasandi'; ?>" method="post">
							    <div class="row justify-content-md-left">
								    <div class="col-sm-auto col-lg-12">
									
										<div class="form-group">
											<label for="old_password">Password Lama</label>
                                            <input type="password" class="form-control" id="old_password" name="old_password" placeholder="Masukan password lama anda" required>
										</div>
										<div class="form-group">
											<label for="new_password">Password Baru</label>
                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <input type="password" class="form-control" id="new_password" name="new_password" placeholder="Masukan password baru anda" required>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <input type="password" class="form-control" id="new_password2" name="new_password2" placeholder="Masukan kembali password baru anda" required>
                                                </div>
                                            </div>
										</div>
										
								    </div>
							    </div>
                                <button type="submit" class="btn btn-primary btn-lg btn-block mb-4">Simpan</button>
                            </form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>