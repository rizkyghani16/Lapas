<div class="d-sm-flex align-items-center justify-content-between mb-4">
	<h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
</div>

<div class="card shadow h-100 mb-md-4">
	<?= $this->session->flashdata('message'); ?>
	<div class="card-header">
		<ul class="nav nav-tabs card-header-tabs justify-content-center" id="bologna-list" role="tablist">
			<li class="nav-item mx-md-3">
				<a class="nav-link active" href="#petunjuk" role="tab" aria-controls="petunjuk"
					aria-selected="true">Petunjuk</a>
			</li>
			<li class="nav-item mx-md-3">
				<a class="nav-link" href="#narapidana" role="tab" aria-controls="narapidana"
					aria-selected="false">Narapidana</a>
			</li>
			<li class="nav-item mx-md-3">
				<?php if (empty($file['file_id'])) : ?>
					<a class="nav-link" href="#surat" role="tab" aria-controls="surat" aria-selected="false">Dokumen</a>
				<?php else :?>
					<?php if ($file['status'] == 'Revision') : ?>
						<a class="nav-link" href="#revisi" role="tab" aria-controls="revisi" aria-selected="false">Revisi Dokumen</a>
					<?php else :?>
						<a class="nav-link disabled" href="#surat" role="tab" aria-controls="surat" aria-selected="false">Dokumen</a>
					<?php endif; ?>
				<?php endif; ?>
			</li>
		</ul>
	</div>
	<div class="card-body">

		<div class="tab-content mt-3">
			<div class="tab-pane fade show active" id="petunjuk" role="tabpanel">
				<?php if($file['status'] == 'Verified') :?>
					<h4 class="text-center mb-4">Kode dokumen anda adalah <b class="text-success"><?php echo $file['file_id'];?></b></h4>
				<?php elseif($file['status'] == 'Revision') :?>
					<h4 class="text-center mb-4">Silahkan Revisi Dokumen Anda</h4>
				<?php else :?>
					<h4 class="text-center mb-4">Petunjuk Penggunaan</h4>
				<?php endif; ?>
				<div class="container">
					<div class="row justify-content-md-center">
						<div class="col-md-auto">
              				<?php if (empty($file['file_id'])) : ?>
								<p class="">1. Mengisi terlebih dahulu data narapidana pada panel <b>Narapidana</b>.</p>
								<p class="">2. Unduh surat pada panel<b> Narapidana</b> & unggah surat dengan format pdf di panel<b> Dokumen</b>.</p>
								<p class="">3. Simpan dokumen asli untuk tahap selanjutnya.</p>
              				<?php else :?>
								<?php if($file['status'] == 'Verified') :?>
									<p>Silahkan kirim berkas anda dengan amplop <b>berwarna coklat</b> menggunakan expedisi <b>Pos Indonesia</b> ke alamat : </p>
									<h5 class="text-center font-italic font-weight-bold">Rutan Kelas 1 Bandung</h5>
									<p class="text-center font-italic">Jl. Jakarta, Kebonwaru, Kec. Batununggal, Kota Bandung, Jawa Barat 40272</p>
									<p>Lampirkan berkas : </p>
									<p class="ml-5">1. Dokumen asli surat pernyataan yang telah ditanda tangani oleh penjamin dan kepala lurah/desa.</p>
									<p class="ml-5">2. Fotocopy Kartu Keluarga.</p>
									<p class="ml-5">3. Fotocopy KTP Penjamin.</p>
									<p class="ml-5">4. Foto penjamin terbaru dengan ukuran 4x6 sebanyak 2 lembar.</p>
									<p>Dengan subject : Surat Penjamin nama_narapidana kode_dokumen</p>
									<p>Contoh subject : <i class="font-weight-bold">Surat Penjamin Fulan bin Fulan FILE000000000001</i></p>
									<p>Jika surat telah dikirim, dimohon untuk mengisi nomor resi pengiriman dokumen pada tombol <b>Isi Resi</b>.</p>
									<?php if(empty($resi['resi_id'])) :?>
										<center>
											<a data-toggle="modal" data-target="#modal_lacak<?php echo $log['logSurat_id']; ?>" style="color: white;" class="btn btn-md btn-warning my-4">Lacak Dokumen</a> <a data-toggle="modal" data-target="#modal_resi<?php echo $this->session->userdata('users_id'); ?>" style="color: white;" class="btn btn-md btn-primary my-4">Isi Resi</a> <a data-toggle="modal" data-target="#modal_qr<?php echo $file['file_id']; ?>" class="btn btn-md btn-info my-4 text-white">Kode QR</a>
										</center>
									<?php else :?>
										<center>
											<a data-toggle="modal" data-target="#modal_lacak<?php echo $log['logSurat_id']; ?>" style="color: white;" class="btn btn-md btn-warning my-4">Lacak Dokumen</a> <a data-toggle="modal" data-target="#modal_qr<?php echo $file['file_id']; ?>" class="btn btn-md btn-info my-4 text-white">Kode QR</a>
										</center>
									<?php endif; ?>
								<?php elseif($file['status'] == 'Revision') :?>
									<p class="text-center font-italic font-weight-bold"><?php echo $revisi['revisi_desc'];?></p>
									<p>Silahkan unggah kembali dokumen yang telah dibetulkan sesuai dengan perintah di atas pada panel <b>Revisi Dokumen</b></p>
									<center>
							  			<a data-toggle="modal" data-target="#modal_lacak<?php echo $log['logSurat_id']; ?>" style="color: white;" class="btn btn-md btn-warning my-4">Lacak Dokumen</a> <a data-toggle="modal" data-target="#modal_qr<?php echo $file['file_id']; ?>" class="btn btn-md btn-info my-4 text-white">Kode QR</a>
							  		</center>
								<?php else :?>
									<p class="text-center mb-4">Kode dokumen anda adalah <b class="text-success"><?php echo $file['file_id'];?></b></p>
									<p>Untuk melacak dokumen anda, silahkan menekan tombol <b>Lacak Dokumen</b></p>
									<center>
							  			<a data-toggle="modal" data-target="#modal_lacak<?php echo $log['logSurat_id']; ?>" style="color: white;" class="btn btn-md btn-warning my-4">Lacak Dokumen</a> <a data-toggle="modal" data-target="#modal_qr<?php echo $file['file_id']; ?>" class="btn btn-md btn-info my-4 text-white">Kode QR</a>
							  		</center>
								<?php endif; ?>
              				<?php endif; ?>
						</div>
					</div>
				</div>
			</div>

			<div class="tab-pane fade" id="narapidana" role="tabpanel" aria-labelledby="narapidana-tab">
				<h4 class="text-center mb-4">Data Narapidana</h4>
				<div class="container">
					<div class="row justify-content-md-center">
						<div class="col-sm-auto col-lg-8">
							<form action="<?php echo base_url().'tambah_narapidana'; ?>" method="post">
								<div class="form-group">
									<label for="nama">Nama Lengkap</label>
									<input type="text" class="form-control" id="nama" name="nama" value="<?= $nara['nama']; ?>"
										placeholder="Contoh : Fulan Fulan" <?php if (!empty($nara['nama'])) {
    echo 'readonly';
} ?>>
								</div>
								<div class="form-row">
									<div class="form-group col-md-6">
										<label for="umur">Umur</label>
										<div class="input-group">
											<input type="text" class="form-control" id="umur" name="umur" placeholder="Contoh : 35"
												value="<?= $nara['umur']; ?>" aria-label="Umur" aria-describedby="basic-addon2" <?php if (!empty($nara['umur'])) {
    echo 'readonly';
} ?>>
											<div class="input-group-append">
												<span class="input-group-text" id="basic-addon2">Tahun</span>
											</div>
										</div>
									</div>
									<div class="form-group col-md-6">
										<label for="hubungan">Hubungan</label>
										<?php if (empty($nara['hubungan'])) : ?>
										<select id="hubungan" name="hubungan" class="form-control border">
											<option selected disabled>Pilih</option>
											<option>Keluarga</option>
											<option>Kerabat</option>
											<option>Wali</option>
										</select>
										<?php else :?>
										<input type="text" class="form-control" id="hubungan" name="hubungan"
											value="<?= $nara['hubungan']; ?>" placeholder="Contoh : Fulan Fulan" readonly>
										<?php endif; ?>
									</div>
								</div>
								<div class="form-group">
									<label for="jenis">Jenis Permohonan</label>
									<?php if (empty($nara['jenis'])) : ?>
									<select id="jenis" name="jenis" class="form-control border">
										<option disabled selected>Pilih</option>
										<?php foreach ($permohonan as $row) : ?>
										<option value="<?php echo $row->nama_permohonan; ?>"><?php echo $row->nama_permohonan; ?></option>
										<?php endforeach; ?>
									</select>
									<?php else :?>
									<input type="text" class="form-control" id="jenis" name="jenis" value="<?= $nara['jenis']; ?>"
										placeholder="Contoh : Fulan Fulan" readonly>
									<?php endif; ?>
								</div>

								<?php if (empty($nara['nama'])) : ?>
								<button type="submit" class="btn btn-primary btn-lg btn-block mb-4">Simpan</button>
								<?php else :?>
								<a href="<?php echo base_url().'surat'; ?>" target="blank"
									class="btn btn-success btn-lg btn-block mb-4">Unduh Surat</a>
								<?php endif; ?>
							</form>
						</div>
					</div>
				</div>
			</div>

			<div class="tab-pane fade" id="surat" role="tabpanel" aria-labelledby="surat-tab">
				<h4 class="text-center mb-4">Unggah File</h4>
				<div class="container">
					<div class="row justify-content-md-center">
						<div class="col-sm-auto col-lg-8">
							<?php echo form_open_multipart('tambah_dokumen');?>
							<label>Surat Penjamin</label>
							<div class="form-group mb-3">
								<div class="custom-file">
									<input type="file" class="custom-file-input" id="surat" name="surat" aria-describedby="surat"
										onchange="Surat(this);" />
									<label class="custom-file-label" for="surat">Pilih File Surat</label>
									<small id="surat" class="form-text text-muted">File surat harus berformat PDF maximal 2
										MB</small>
								</div>
							</div>
							<label>Kartu Keluarga</label>
							<div class="form-group mb-3">
								<div class="custom-file">
									<input type="file" class="custom-file-input" id="kk" name="kk" aria-describedby="kk"
										onchange="KartuKeluarga(this);" />
									<label class="custom-file-label" for="kk">Pilih File Kartu Keluarga</label>
									<small id="kk" class="form-text text-muted">File kartu keluarga harus berformat PDF maximal 2
										MB</small>
								</div>
							</div>
							<label>KTP Penjamin</label>
							<div class="form-group mb-3">
								<div class="custom-file">
									<input type="file" class="custom-file-input" id="ktp" name="ktp" aria-describedby="ktp"
										onchange="Ktp(this);">
									<label class="custom-file-label" for="ktp">Pilih File KTP Penjamin</label>
									<small id="ktp" class="form-text text-muted">File KTP berformat PNG/JPG/JPEG maximal 2
										MB</small>
								</div>
							</div>
							<label>Foto Penjamin</label>
							<div class="form-group mb-3">
								<div class="custom-file">
									<input type="file" class="custom-file-input" id="foto" name="foto" aria-describedby="foto"
										onchange="Foto(this);">
									<label class="custom-file-label" for="foto">Pilih File Foto Penjamin</label>
									<small id="foto" class="form-text text-muted">File foto berformat PNG/JPG/JPEG maximal 2
										MB</small>
								</div>
							</div>
							<button type="submit" class="btn btn-primary btn-lg btn-block mb-4">Unggah</button>
							</form>
						</div>
					</div>
				</div>
			</div>

			<div class="tab-pane fade" id="revisi" role="tabpanel" aria-labelledby="revisi-tab">
				<h4 class="text-center mb-4">Unggah File</h4>
				<div class="container">
					<div class="row justify-content-md-center">
						<div class="col-sm-auto col-lg-8">
							<?php echo form_open_multipart('revisi_dokumen');?>
							<label>Surat Penjamin</label>
							<div class="form-group mb-3">
								<div class="custom-file">
									<input type="file" class="custom-file-input" id="surat" name="surat" aria-describedby="surat"
										onchange="Surat(this);" />
									<label class="custom-file-label" for="surat">Pilih File Surat</label>
									<small id="surat" class="form-text text-muted">File surat harus berformat PDF maximal 2
										MB</small>
								</div>
							</div>
							<label>Kartu Keluarga</label>
							<div class="form-group mb-3">
								<div class="custom-file">
									<input type="file" class="custom-file-input" id="kk" name="kk" aria-describedby="kk"
										onchange="KartuKeluarga(this);" />
									<label class="custom-file-label" for="kk">Pilih File Kartu Keluarga</label>
									<small id="kk" class="form-text text-muted">File kartu keluarga harus berformat PDF maximal 2
										MB</small>
								</div>
							</div>
							<label>KTP Penjamin</label>
							<div class="form-group mb-3">
								<div class="custom-file">
									<input type="file" class="custom-file-input" id="ktp" name="ktp" aria-describedby="ktp"
										onchange="Ktp(this);">
									<label class="custom-file-label" for="ktp">Pilih File KTP Penjamin</label>
									<small id="ktp" class="form-text text-muted">File KTP berformat PNG/JPG/JPEG maximal 2
										MB</small>
								</div>
							</div>
							<label>Foto Penjamin</label>
							<div class="form-group mb-3">
								<div class="custom-file">
									<input type="file" class="custom-file-input" id="foto" name="foto" aria-describedby="foto"
										onchange="Foto(this);">
									<label class="custom-file-label" for="foto">Pilih File Foto Penjamin</label>
									<small id="foto" class="form-text text-muted">File foto berformat PNG/JPG/JPEG maximal 2
										MB</small>
								</div>
							</div>
							<button type="submit" class="btn btn-primary btn-lg btn-block mb-4">Unggah</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- ============ MODAL LACAK =============== -->
        <div class="modal fade" id="modal_lacak<?php echo $log['logSurat_id']?>" tabindex="-1" role="dialog"
        	aria-labelledby="largeModal" aria-hidden="true">
        	<div class="modal-dialog">
        		<div class="modal-content">
        			<div class="modal-header">
        				<h3 class="modal-title" id="modal_hapus">Dokumen Anda</h3>
        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        			</div>
        			<div class="container">
						<div class="row">
							<div class="col-md-12 col-lg-12">
								<div id="tracking-pre"></div>
								<div id="tracking">
									<!-- <div class="tracking-list"> -->
									<?php foreach($logs as $key=>$value): ?>
									<div class="tracking-item">
										<div class="tracking-icon">
											<img src="<?php echo base_url(); ?>assets/rounded.svg" alt="" width="30px">
											<!-- <i class="fas fa-circle"></i> -->
										</div>
										<div class="tracking-date"><?php echo date("d-M-Y", strtotime($value->log_time)); ?><span><?php echo date("g:i A", strtotime($value->log_time)); ?></span></div>
										<div class="tracking-content"><?php echo $value->log_title;?><span><?php echo $value->log_desc;?></span></div>
									</div>
									<?php endforeach; ?>
									<!-- </div> -->
								</div>
							</div>
						</div>
					</div>
        		</div>
        	</div>
        </div>
    <!--END MODAL LACAK-->

<!-- ============ MODAL RESI =============== -->
        <div class="modal fade" id="modal_resi<?php echo $this->session->userdata('users_id'); ?>" tabindex="-1" role="dialog"
        	aria-labelledby="largeModal" aria-hidden="true">
        	<div class="modal-dialog">
        		<div class="modal-content">
        			<div class="modal-header">
        				<h3 class="modal-title" id="modal_resi">Kode Resi Dokumen</h3>
        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        			</div>
        			<div class="container">
						<div class="row">
							<div class="col-md-12 col-lg-12">
							<form action="<?php echo base_url(). 'isi_resi'; ?>" method="post">
								<div class="modal-body">

									<div class="form-group">
										<label for="general_type">Resi Dokumen</label>
										<input type="text" class="form-control" id="kode_resi" name="kode_resi"
											placeholder="Masukan Kode Resi Anda">
									</div>

								</div>

								<div class="modal-footer">
									<button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
									<button class="btn btn-info">Simpan</button>
								</div>
							</form>
							</div>
						</div>
					</div>
        		</div>
        	</div>
        </div>
    <!--END MODAL RESI-->

<!-- ============ MODAL QR =============== -->
        <div class="modal fade" id="modal_qr<?php echo $file['file_id']; ?>" tabindex="-1" role="dialog"
        	aria-labelledby="largeModal" aria-hidden="true">
        	<div class="modal-dialog">
        		<div class="modal-content">
					<div class="modal-header">
        				<h3 class="modal-title" id="modal_qr">Kode QR Anda</h3><br>
        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        			</div>
        			<div class="container">
						<div class="row">
							<div class="col-md-12 col-lg-12">
								<div class="modal-body text-center">
										
									<p>Tunjukan kode qr ini pada petugas Rutan Kelas 1 Bandung</p>
									<img src="<?php echo base_url(); ?>uploads/qrcode/<?php echo $file['qrcode']; ?>" alt="Kode QR dokumen Anda" width="95%">

								</div>
							</div>
						</div>
					</div>
        		</div>
        	</div>
        </div>
    <!--END MODAL QR-->