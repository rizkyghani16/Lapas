
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width">
        <link rel="icon" href="<?= base_url(); ?>assets/img/cropped-1-2-32x32.png" sizes="32x32">
        <link rel="icon" href="<?= base_url(); ?>assets/img/cropped-1-2-192x192.png" sizes="192x192">
        <link rel="apple-touch-icon" href="<?= base_url(); ?>assets/img/cropped-1-2-180x180.png">
        <title class="font-mono">Rutan Kelas I Bandung</title>
        <link rel="stylesheet" href="<?= base_url(); ?>assets/index/css/tailwind.min.css">
        <link rel="stylesheet" href="<?= base_url(); ?>assets/index/css/animate.min.css">
        <script src="<?= base_url(); ?>assets/index/js/wow.min.js"></script>
        <link rel="stylesheet" href="<?= base_url(); ?>assets/index/css/all.min.css">
        <script>new WOW().init();</script>
        <style type="text/css">
        #loader {
        	display: none;
        	position: fixed;
        	top: 0;
        	left: 0;
        	right: 0;
        	bottom: 0;
        	width: 100%;
        	background: rgba(0, 0, 0, 0.75) url('<?php echo base_url(); ?>assets/img/spin.gif') no-repeat center center;
            /* background-size: 30% 30%; */
        	z-index: 10000;
        }
        </style>
    </head>
    <body class="">
        <div class="lg:flex">
            <div class="lg:w-1/3 xl:max-w-screen-sm">
                <div class="py-12 bg-indigo-100 lg:bg-white flex justify-center lg:justify-start lg:px-12">
                    <div class="cursor-pointer flex items-center">
                        <div>
                            <!-- <svg class="w-10 text-indigo-500" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 225 225" style="enable-background:new 0 0 225 225;" xml:space="preserve">
                                <style type="text/css">
                                    .st0{fill:none;stroke:currentColor;stroke-width:20;stroke-linecap:round;stroke-miterlimit:3;}
                                </style>
                                <g transform="matrix( 1, 0, 0, 1, 0,0) ">
                                <g>
                                <path id="Layer0_0_1_STROKES" class="st0" d="M173.8,151.5l13.6-13.6 M35.4,89.9l29.1-29 M89.4,34.9v1 M137.4,187.9l-0.6-0.4     M36.6,138.7l0.2-0.2 M56.1,169.1l27.7-27.6 M63.8,111.5l74.3-74.4 M87.1,188.1L187.6,87.6 M110.8,114.5l57.8-57.8"/>
                                </g>
                                </g>
                            </svg> -->
                        </div>
                        <div class="text-2xl text-indigo-600 tracking-wide ml-2 font-semibold"><a href="<?= base_url(); ?>">Rutan Kelas I Bandung</a></div>
                    </div>
                </div>
                <div class="mt-10 px-8 sm:px-20 md:px-44 lg:px-8 lg:mt-16 xl:px-20 xl:max-w-2xl">
                    <h2 class="text-center text-4xl text-indigo-600 font-display font-semibold lg:text-left xl:text-5xl
                    xl:text-bold">Registrasi Akun</h2>
                    <p class="text-center text-gray-500 font-regular tracking-widest leading-loose lg:text-left pt-4">Registrasikan diri anda untuk membuat surat jaminan kesanggupan keluarga</p>
                    <div class="mt-12 mb-12">
                        <form id="register-form" method="POST" enctype="multipart/form-data">
                        <input type="text" id="u_reg" name="u_reg" value="<?php echo strtoupper($kode); ?>" hidden="true">
                            <div class="flex flex-wrap">
                                <div class="w-full">
                                  <label class="text-sm font-semibold text-gray-700 tracking-wide" for="fullname">Nama Lengkap</label>
                                  <input class="w-full h-10 py-2 px-3 text-base border rounded-lg focus:outline-none focus:border-indigo-600" type="text" id="fullname" name="fullname" placeholder="Masukan nama lengkap anda"/>
                                </div>
                            </div>
                            <div class="flex flex-wrap relative mt-5">
                                <label class="text-sm font-semibold text-gray-700 tracking-wide" for="gender">Jenis Kelamin</label>
                                <select class="w-full h-10 py-2 px-3 pl-3 pr-6 text-base border rounded-lg appearance-none focus:outline-none focus:border-indigo-600" id="gender" name="gender" placeholder="Jenis Kelamin">
                                    <option value="">Pilih</option>
                                    <?php foreach ($GetAllGender as $value) { ?>
                                    <option value="<?= $value; ?>"><?= $value; ?></option>
                                    <?php } ?>
                                </select>
                                <div class="absolute inset-y-0 right-0 flex items-center px-2 pointer-events-none">
                                  <svg class="w-4 h-4 fill-current mt-5" viewBox="0 0 20 20"><path d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" fill-rule="evenodd"></path></svg>
                                </div>
                            </div>
                            <div class="flex flex-wrap mt-5">
                                <div class="w-full">
                                  <label class="text-sm font-semibold text-gray-700 tracking-wide" for="email">Email</label>
                                  <input class="w-full h-10 py-2 px-3 text-base border rounded-lg focus:outline-none focus:border-indigo-600" type="email" id="email" name="email" placeholder="contoh@contoh.com"/>
                                </div>
                            </div>
                            <div class="flex flex-wrap mt-5">
                                <div class="w-full relative">
                                  <label class="text-sm font-semibold text-gray-700 tracking-wide" for="password">Kata Sandi</label>
                                    <input class="w-full h-10 py-2 px-3 text-base border rounded-lg focus:outline-none focus:border-indigo-600" pattern=".{6,}"   required title="Minimal 6 karakter" type="password" id="password" name="password" placeholder="Gunakan 6 karakter atau lebih" aria-describedby="passwordHelp"/>
                                    <div class="absolute inset-y-0 right-0 flex items-center px-4 mt-5">
                                        <i class="far fa-eye text-gray-700" id="togglePassword"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="flex flex-wrap mt-5">
                                <div class="w-full relative">
                                  <label class="text-sm font-semibold text-gray-700 tracking-wide" for="password2">Konfirmasi Kata Sandi</label>
                                  <input class="w-full h-10 py-2 px-3 text-base border rounded-lg focus:outline-none focus:border-indigo-600" pattern=".{6,}"   required title="Minimal 6 karakter" type="password" id="password2" name="password2" placeholder="Konfirmasi ulang kata sandi" aria-describedby="passwordHelp"/>
                                  <div class="absolute inset-y-0 right-0 flex items-center px-4 mt-5">
                                        <i class="far fa-eye text-gray-700" id="togglePassword2"></i>
                                  </div>
                                </div>
                            </div>
                            <div class="flex flex-wrap -mx-2 space-y-4 md:space-y-0 mt-5">
                                <div class="w-full px-2 md:w-1/2">
                                  <label class="block mb-1" for="job">Pekerjaan</label>
                                  <input class="w-full h-10 px-3 text-base border rounded-lg focus:outline-none focus:border-indigo-600" type="text" id="job" name="job" placeholder="Wiraswasta"/>
                                </div>
                                <div class="w-full px-2 md:w-1/2">
                                  <label class="block mb-1" for="age">Umur</label>
                                  <input class="w-full h-10 px-3 text-base border rounded-lg focus:outline-none focus:border-indigo-600" type="text" id="age" name="age" placeholder="35"/>
                                </div>
                            </div>
                            <div class="flex flex-wrap mt-5">
                                <div class="w-full">
                                  <label class="text-sm font-semibold text-gray-700 tracking-wide" for="tel">No. Telp / HP</label>
                                  <input class="w-full h-10 py-2 px-3 text-base border rounded-lg focus:outline-none focus:border-indigo-600" type="text" id="tel" name="tel" placeholder="088 888 888 88"/>
                                </div>
                            </div>
                            <div class="flex flex-wrap mt-5">
                                <div class="w-full">
                                  <label class="text-sm font-semibold text-gray-700 tracking-wide" for="address">Alamat</label>
                                  <textarea class="w-full h-16 px-3 py-2 text-base border rounded-lg focus:outline-none focus:border-indigo-600" id="address" name="address" placeholder="Jalan 123 Bandung"></textarea>
                                </div>
                            </div>

                            <div class="mt-10">
                                <button class="bg-indigo-600 text-gray-100 p-4 w-full rounded-full tracking-wide
                                font-semibold font-display focus:outline-none focus:shadow-outline hover:bg-indigo-700
                                shadow-lg">
                                    Daftar
                                </button>
                            </div>
                            <div class="mt-4 text-sm font-display font-semibold text-gray-700 text-center">
                            Sudah punya akun ? <a href="<?= base_url(); ?>login" class="cursor-pointer text-indigo-600 hover:text-indigo-800">Masuk disini</a>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
            <div class="hidden lg:flex items-center justify-center bg-gray-200 flex-1 bg-repeat-y">
                <div class="max-w-4xl transform duration-200 hover:scale-110 cursor-pointer wow fadeInDown">
                    <img src="<?= base_url(); ?>assets/img/login2.svg" alt="Masuk">
                </div>
            </div>
        </div>
        <div id="loader"></div>

        <script src="<?= base_url(); ?>assets/public/vendor/jquery/jquery-3.2.1.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/admin/vendor/jquery/jquery.min.js"></script>
        <script Type="text/javascript">
            togglePassword.addEventListener('click', function (e) {
                // toggle the type attribute
                const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
                password.setAttribute('type', type);
                // toggle the eye slash icon
                this.classList.toggle('fa-eye-slash');
            });
        </script>
        <script Type="text/javascript">
            togglePassword2.addEventListener('click', function (e) {
                // toggle the type attribute
                const type = password2.getAttribute('type') === 'password' ? 'text' : 'password';
                password2.setAttribute('type', type);
                // toggle the eye slash icon
                this.classList.toggle('fa-eye-slash');
            });
        </script>
        <script Type="text/javascript">
            function setInputFilter(textbox, inputFilter) {
            ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
                textbox.addEventListener(event, function() {
                if (inputFilter(this.value)) {
                    this.oldValue = this.value;
                    this.oldSelectionStart = this.selectionStart;
                    this.oldSelectionEnd = this.selectionEnd;
                } else if (this.hasOwnProperty("oldValue")) {
                    this.value = this.oldValue;
                    this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                } else {
                    this.value = "";
                }
                });
            });
            }

            setInputFilter(document.getElementById("tel"), function(value) {
            return /^-?\d*$/.test(value); });
        </script>
        <script type="text/javascript">
            var spinner = $('#loader');
            $(document).on('submit', '#register-form', function(a){
                var formData = new FormData($(this)[0]);
                spinner.show();
                $.ajax({
                method: 'POST',
                url: "<?= base_url('index.php/main/register_process'); ?>",
                data: formData,
                processData: false,
                contentType: false
                }).done(function(data) {
                    var out = jQuery.parseJSON(data);
                    spinner.hide();
                    if (out.status == true) {
                        if(window.confirm(out.message)) {
                            window.location.replace("<?= base_url('aktifasi'); ?>");
                        }
                    } else {
                        window.confirm(out.message);
                    }
                })
                a.preventDefault();
            });
        </script>
    </body>
</html>
