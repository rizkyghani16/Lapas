<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// use PHPMailer\PHPMailer\PHPMailer;
// use PHPMailer\PHPMailer\Exception;
// use PHPMailer\PHPMailer\SMTP;

class Main extends CI_Controller {

    function __construct(){
		parent::__construct();		
		$this->load->model('admin_model');
		$this->load->library('encrypt');
		// require APPPATH.'libraries/phpmailer/src/Exception.php';
        // require APPPATH.'libraries/phpmailer/src/PHPMailer.php';
        // require APPPATH.'libraries/phpmailer/src/SMTP.php';

        if ($this->session->userdata('level') == 'Admin') {
			
		} else {
			redirect(base_url("index.php/main/logout"));
		}
	}
	public function index()
	{
		date_default_timezone_set('Asia/Jakarta');
		$date = date('Y');
		$data['u_count'] = $this->db->query("SELECT COUNT(users_id) as countu FROM users")->row_array();
		$data['f_count'] = $this->db->query("SELECT COUNT(file_id) as countf FROM file")->row_array();
		$data['u_thn'] = $this->db->query("SELECT COUNT(users_id) as countu FROM users WHERE YEAR(reg_time) = '$date'")->row_array();
		$data['f_asi'] = $this->db->query("SELECT COUNT(nara_id) as countp FROM narapidana WHERE jenis = 'Asimilasi'")->row_array();
		$data['f_ber'] = $this->db->query("SELECT COUNT(nara_id) as countp FROM narapidana WHERE jenis = 'Pembebasan Bersyarat'")->row_array();
		$data['f_cmb'] = $this->db->query("SELECT COUNT(nara_id) as countp FROM narapidana WHERE jenis = 'Cuti Menjelang Bebas'")->row_array();
		$data['f_cbe'] = $this->db->query("SELECT COUNT(nara_id) as countp FROM narapidana WHERE jenis = 'Cuti Bersyarat'")->row_array();
		$data['f_jco'] = $this->db->query("SELECT COUNT(nara_id) as countp FROM narapidana WHERE jenis = 'Justice Collaborator'")->row_array();
		$data['u_jan'] = $this->db->query("SELECT COUNT(users_id) as countu FROM users WHERE YEAR(reg_time) = '$date' AND MONTH(reg_time) = '01'")->row_array();
		$data['u_feb'] = $this->db->query("SELECT COUNT(users_id) as countu FROM users WHERE YEAR(reg_time) = '$date' AND MONTH(reg_time) = '02'")->row_array();
		$data['u_mar'] = $this->db->query("SELECT COUNT(users_id) as countu FROM users WHERE YEAR(reg_time) = '$date' AND MONTH(reg_time) = '03'")->row_array();
		$data['u_apr'] = $this->db->query("SELECT COUNT(users_id) as countu FROM users WHERE YEAR(reg_time) = '$date' AND MONTH(reg_time) = '04'")->row_array();
		$data['u_mei'] = $this->db->query("SELECT COUNT(users_id) as countu FROM users WHERE YEAR(reg_time) = '$date' AND MONTH(reg_time) = '05'")->row_array();
		$data['u_jun'] = $this->db->query("SELECT COUNT(users_id) as countu FROM users WHERE YEAR(reg_time) = '$date' AND MONTH(reg_time) = '06'")->row_array();
		$data['u_jul'] = $this->db->query("SELECT COUNT(users_id) as countu FROM users WHERE YEAR(reg_time) = '$date' AND MONTH(reg_time) = '07'")->row_array();
		$data['u_agu'] = $this->db->query("SELECT COUNT(users_id) as countu FROM users WHERE YEAR(reg_time) = '$date' AND MONTH(reg_time) = '08'")->row_array();
		$data['u_sep'] = $this->db->query("SELECT COUNT(users_id) as countu FROM users WHERE YEAR(reg_time) = '$date' AND MONTH(reg_time) = '09'")->row_array();
		$data['u_okt'] = $this->db->query("SELECT COUNT(users_id) as countu FROM users WHERE YEAR(reg_time) = '$date' AND MONTH(reg_time) = '10'")->row_array();
		$data['u_nov'] = $this->db->query("SELECT COUNT(users_id) as countu FROM users WHERE YEAR(reg_time) = '$date' AND MONTH(reg_time) = '11'")->row_array();
		$data['u_des'] = $this->db->query("SELECT COUNT(users_id) as countu FROM users WHERE YEAR(reg_time) = '$date' AND MONTH(reg_time) = '12'")->row_array();
		$data['view'] = 'admin/dashboard';
		$this->load->view('admin/layout', $data);
	}

	public function users()
	{
		$data['users'] = $this->db->query("SELECT * FROM users WHERE level = 'User' ORDER BY users_id DESC");
		$data['view'] = 'admin/users';
		$this->load->view('admin/layout', $data);
	}

	public function hapus_user()
    {
        $this->form_validation->set_rules('users_id','ID','required');
 
		if($this->form_validation->run() != false){
            $users_id = $this->input->post('users_id');
            $this->db->query("DELETE FROM users WHERE users_id = '$users_id'");
            $this->session->set_flashdata('message', '<div class="alert alert-success" id="success-alert" role="alert">Data pengguna berhasil dihapus<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
            redirect(base_url("admin/main/users"));
		}else{
            $this->session->set_flashdata('message', '<div class="alert alert-danger" id="danger-alert" role="alert">Data pengguna gagal dihapus<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
			redirect(base_url("admin/main/users"));
		}
    }

	public function add_user()
	{
		$this->form_validation->set_rules('fullname', 'Nama Lengkap', 'trim|required');
		$this->form_validation->set_rules('gender', 'Jenis Kelamin', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required');
		$this->form_validation->set_rules('password', 'Kata Sandi', 'trim|required');
		$this->form_validation->set_rules('password2', 'Konfirmasi Kata Sandi', 'trim|required');
		$this->form_validation->set_rules('address', 'Alamat', 'trim|required');
		$this->form_validation->set_rules('job', 'Pekerjaan', 'trim|required');
		$this->form_validation->set_rules('age', 'Umur', 'trim|required');
		$this->form_validation->set_rules('tel', 'No. Telp / HP', 'trim|required');

		if($this->form_validation->run() == TRUE) {
			if($this->input->post('password') == $this->input->post('password2')) {
				date_default_timezone_set('Asia/Jakarta');
				$data = array(
					'users_id' => '',
					'fullname' => $this->input->post('fullname'),
					'email' => $this->input->post('email'),
					'password' => md5($this->input->post('password')),
					'gender' => $this->input->post('gender'),
					'address' => $this->input->post('address'),
					'job' => $this->input->post('job'),
					'age' => $this->input->post('age'),
					'tel' => $this->input->post('tel'),
					'reg_time' => date('Y-m-d H:i:s'),
					'up_time' => '',
					'level' => 'User'
				);

			$this->admin_model->add($data, 'users');
			$this->session->set_flashdata('message', '<div class="alert alert-success" id="success-alert" role="alert">Data
				pengguna berhasil disimpan<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
						aria-hidden="true">&times;</span></button></div>');
			redirect(base_url('admin/main/users'));
			} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger" id="danger-alert" role="alert">Kata sandi tidak sama<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
						aria-hidden="true">&times;</span></button></div>');
			redirect(base_url('admin/main/users'));
			}
		} else {
		$this->session->set_flashdata('message', '<div class="alert alert-danger" id="danger-alert" role="alert">Data
			pengguna gagal disimpan<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
					aria-hidden="true">&times;</span></button></div>');
		redirect(base_url('admin/main/users'));
		}
	}

	public function update_user()
	{
		$this->form_validation->set_rules('users_id', 'ID', 'trim|required');
		$this->form_validation->set_rules('fullname', 'Nama Lengkap', 'trim|required');
		$this->form_validation->set_rules('gender', 'Jenis Kelamin', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required');
		$this->form_validation->set_rules('address', 'Alamat', 'trim|required');
		$this->form_validation->set_rules('job', 'Pekerjaan', 'trim|required');
		$this->form_validation->set_rules('age', 'Umur', 'trim|required');
		$this->form_validation->set_rules('tel', 'No. Telp / HP', 'trim|required');

		if($this->form_validation->run() == TRUE) {
			
			date_default_timezone_set('Asia/Jakarta');
			
			$users_id = $this->input->post('users_id');
			$fullname = $this->input->post('fullname');
			$email = $this->input->post('email');
			$gender = $this->input->post('gender');
			$address = $this->input->post('address');
			$job = $this->input->post('job');
			$age = $this->input->post('age');
			$tel = $this->input->post('tel');
			$up_time = date('Y-m-d H:i:s');
				
			$this->db->query("UPDATE users SET fullname = '$fullname', email = '$email', gender = '$gender', address = '$address', job = '$job', age = '$age', tel = '$tel', up_time = '$up_time' WHERE users_id = '$users_id'");
			$this->session->set_flashdata('message', '<div class="alert alert-success" id="success-alert" role="alert">Data
				pengguna berhasil diperbaharui<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
						aria-hidden="true">&times;</span></button></div>');
			redirect(base_url('admin/main/users'));
			
		} else {
		$this->session->set_flashdata('message', '<div class="alert alert-danger" id="danger-alert" role="alert">Data
			pengguna gagal diperbaharui<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
					aria-hidden="true">&times;</span></button></div>');
		redirect(base_url('admin/main/users'));
		}
	}

	public function update_password()
	{
		$this->form_validation->set_rules('users_id', 'ID', 'trim|required');
		$this->form_validation->set_rules('password', 'Kata Sandi', 'trim|required');
		$this->form_validation->set_rules('password2', 'Konfirmasi Kata Sandi', 'trim|required');

		if($this->form_validation->run() == TRUE) {
			if($this->input->post('password') == $this->input->post('password2')) {
				date_default_timezone_set('Asia/Jakarta');
				
				$users_id = $this->input->post('users_id');
				$password = md5($this->input->post('password'));
				$up_time = date('Y-m-d H:i:s');
					
				$this->db->query("UPDATE users SET password = '$password', up_time = '$up_time' WHERE users_id = '$users_id'");
				$this->session->set_flashdata('message', '<div class="alert alert-success" id="success-alert" role="alert">Kata sandi
					pengguna berhasil diperbaharui<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
							aria-hidden="true">&times;</span></button></div>');
				redirect(base_url('admin/main/users'));
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger" id="danger-alert" role="alert">Kata sandi tidak sama<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
							aria-hidden="true">&times;</span></button></div>');
				redirect(base_url('admin/main/users'));
			}
			
		} else {
		$this->session->set_flashdata('message', '<div class="alert alert-danger" id="danger-alert" role="alert">Kata sandi
			pengguna gagal diperbaharui<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
					aria-hidden="true">&times;</span></button></div>');
		redirect(base_url('admin/main/users'));
		}
	}

	public function permohonan()
	{
		$data['per'] = $this->db->query("SELECT * FROM permohonan ORDER BY permo_id DESC");
		$data['view'] = 'admin/permohonan';
		$this->load->view('admin/layout', $data);
	}

	public function hapus_permo()
    {
        $this->form_validation->set_rules('permo_id','ID','required');
 
		if($this->form_validation->run() != false){
            $permo_id = $this->input->post('permo_id');
            $this->db->query("DELETE FROM permohonan WHERE permo_id = '$permo_id'");
            $this->session->set_flashdata('message', '<div class="alert alert-success" id="success-alert" role="alert">Jenis permohonan berhasil dihapus<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
            redirect(base_url("admin/main/permohonan"));
		}else{
            $this->session->set_flashdata('message', '<div class="alert alert-danger" id="danger-alert" role="alert">Jenis permohonan gagal dihapus<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
			redirect(base_url("admin/main/permohonan"));
		}
    }

	public function add_permo()
	{
		$this->form_validation->set_rules('nama_permohonan', 'Jenis Permohonan', 'trim|required');

		if($this->form_validation->run() == TRUE) {
			$data = array(
				'permo_id' => '',
				'nama_permohonan' => $this->input->post('nama_permohonan')
			);

			$this->admin_model->add($data, 'permohonan');
			$this->session->set_flashdata('message', '<div class="alert alert-success" id="success-alert" role="alert">Jenis
				permohonan berhasil disimpan<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
						aria-hidden="true">&times;</span></button></div>');
			redirect(base_url('admin/main/permohonan'));
		} else {
		$this->session->set_flashdata('message', '<div class="alert alert-danger" id="danger-alert" role="alert">Jenis
			permohonan gagal disimpan<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
					aria-hidden="true">&times;</span></button></div>');
		redirect(base_url('admin/main/permohonan'));
		}
	}

	public function narapidana()
	{
		$data['nara'] = $this->db->query("SELECT * FROM narapidana  JOIN users on narapidana.users_id = users.users_id ORDER BY nara_id DESC");
		$data['view'] = 'admin/narapidana';
		$this->load->view('admin/layout', $data);
	}

	public function hapus_nara()
    {
        $this->form_validation->set_rules('nara_id','ID','required');
 
		if($this->form_validation->run() != false){
            $nara_id = $this->input->post('nara_id');
            $this->db->query("DELETE FROM narapidana WHERE nara_id = '$nara_id'");
            $this->session->set_flashdata('message', '<div class="alert alert-success" id="success-alert" role="alert">Data narapidana berhasil dihapus<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
            redirect(base_url("admin/main/narapidana"));
		}else{
            $this->session->set_flashdata('message', '<div class="alert alert-danger" id="danger-alert" role="alert">Data narapidana gagal dihapus<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
			redirect(base_url("admin/main/narapidana"));
		}
    }

	public function update_nara()
    {
        $this->form_validation->set_rules('nara_id','ID','required');
        $this->form_validation->set_rules('nama','Nama Narapidana','required');
        $this->form_validation->set_rules('umur','Umur Narapidana','required');
        $this->form_validation->set_rules('hubungan','Hubungan Narapidana','required');
        $this->form_validation->set_rules('jenis','Jenis Permohonan Narapidana','required');
 
		if($this->form_validation->run() != false){
            $nara_id = $this->input->post('nara_id');
            $nama = $this->input->post('nama');
            $umur = $this->input->post('umur');
            $hubungan = $this->input->post('hubungan');
            $jenis = $this->input->post('jenis');

            $this->db->query("UPDATE narapidana SET nama = '$nama', umur = '$umur', hubungan = '$hubungan', jenis = '$jenis' WHERE nara_id = '$nara_id'");
            $this->session->set_flashdata('message', '<div class="alert alert-success" id="success-alert" role="alert">Data narapidana berhasil diperbaharui<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
            redirect(base_url("admin/main/narapidana"));
		}else{
            $this->session->set_flashdata('message', '<div class="alert alert-danger" id="danger-alert" role="alert">Data narapidana gagal diperbaharui<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
			redirect(base_url("admin/main/narapidana"));
		}
    }

	public function dokumen()
	{
		$data['pending'] = $this->db->query("SELECT * FROM file JOIN users on file.users_id = users.users_id WHERE file.status = 'Pending' ORDER BY file.file_id DESC");
		$data['revisi'] = $this->db->query("SELECT * FROM file JOIN users on file.users_id = users.users_id WHERE file.status = 'Revision' ORDER BY file.file_id DESC");
		$data['verif'] = $this->db->query("SELECT * FROM file JOIN users on file.users_id = users.users_id WHERE file.status = 'Verified' ORDER BY file.file_id DESC");
		$data['view'] = 'admin/dokumen';
		$this->load->view('admin/layout', $data);
	}

	public function tinjau_dokumen()
	{
		$this->form_validation->set_rules('file_id','ID','required');
 
		if($this->form_validation->run() != false){
			date_default_timezone_set('Asia/Jakarta');

      		$file_id = $this->input->post('file_id');
			$cek = $this->db->query("SELECT users_id FROM file WHERE file_id = '$file_id'")->row_array();
			$users_id = $cek['users_id'];
			$users = $this->db->query("SELECT * FROM users WHERE users_id = '$users_id'")->row_array();

			$config = [
				'mailtype'  => 'html',
				'charset'   => 'utf-8',
				'protocol'  => 'smtp',
				'smtp_host' => 'smtp.googlemail.com',
				'smtp_user' => 'stormxzero1@gmail.com',  // Email gmail
				'smtp_pass'   => 'minuman123',  // Password gmail
				'smtp_crypto' => 'ssl',
				'smtp_port'   => 465,
				'crlf'    => "\r\n",
				'newline' => "\r\n"
			];

			// $response = false;
            // $mail = new PHPMailer(true);
            // $mail->isSMTP();
            // $mail->Mailer = "smtp";
            // $mail->Host     = 'smtp.googlemail.com'; 
            // $mail->SMTPAuth = true;
            // $mail->Username = 'stormxzero1@gmail.com';
            // $mail->Password = 'minuman123'; 
            // $mail->Priority = 1;
			// $mail->SMTPDebug = 1;
			// $mail->SMTPSecure = 'tls';
            // $mail->Port     = 465;
			// $mail->SMTPOptions = array(
			// 	'ssl' => array(
			// 		'verify_peer' => false,
			// 		'verify_peer_name' => false,
			// 		'allow_self_signed' => true
			// 	)
			// );

			$body_email = '<!doctype html>
            <html>
              <head>
                <meta name="viewport" content="width=device-width" />
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                <title>Notifikasi Sistem Surat Penjamin Rutan Kelas 1 Bandung</title>
                <style>
                  /* -------------------------------------
                      GLOBAL RESETS
                  ------------------------------------- */
                  
                  /*All the styling goes here*/
                  
                  img {
                    border: none;
                    -ms-interpolation-mode: bicubic;
                    max-width: 100%; 
                  }
            
                  body {
                    background-color: #f6f6f6;
                    font-family: sans-serif;
                    -webkit-font-smoothing: antialiased;
                    font-size: 14px;
                    line-height: 1.4;
                    margin: 0;
                    padding: 0;
                    -ms-text-size-adjust: 100%;
                    -webkit-text-size-adjust: 100%; 
                  }
            
                  table {
                    border-collapse: separate;
                    mso-table-lspace: 0pt;
                    mso-table-rspace: 0pt;
                    width: 100%; }
                    table td {
                      font-family: sans-serif;
                      font-size: 14px;
                      vertical-align: top; 
                  }
            
                  /* -------------------------------------
                      BODY & CONTAINER
                  ------------------------------------- */
            
                  .body {
                    background-color: #f6f6f6;
                    width: 100%; 
                  }
            
                  /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
                  .container {
                    display: block;
                    margin: 0 auto !important;
                    /* makes it centered */
                    max-width: 580px;
                    padding: 10px;
                    width: 580px; 
                  }
            
                  /* This should also be a block element, so that it will fill 100% of the .container */
                  .content {
                    box-sizing: border-box;
                    display: block;
                    margin: 0 auto;
                    max-width: 580px;
                    padding: 10px; 
                  }
            
                  /* -------------------------------------
                      HEADER, FOOTER, MAIN
                  ------------------------------------- */
                  .main {
                    background: #ffffff;
                    border-radius: 3px;
                    width: 100%; 
                  }
            
                  .wrapper {
                    box-sizing: border-box;
                    padding: 20px; 
                  }
            
                  .content-block {
                    padding-bottom: 10px;
                    padding-top: 10px;
                  }
            
                  .footer {
                    clear: both;
                    margin-top: 10px;
                    text-align: center;
                    width: 100%; 
                  }
                    .footer td,
                    .footer p,
                    .footer span,
                    .footer a {
                      color: #999999;
                      font-size: 12px;
                      text-align: center; 
                  }
            
                  /* -------------------------------------
                      TYPOGRAPHY
                  ------------------------------------- */
                  h1,
                  h2,
                  h3,
                  h4 {
                    color: #000000;
                    font-family: sans-serif;
                    font-weight: 400;
                    line-height: 1.4;
                    margin: 0;
                    margin-bottom: 30px; 
                  }
            
                  h1 {
                    font-size: 35px;
                    font-weight: 300;
                    text-align: center;
                    text-transform: capitalize; 
                  }
            
                  p,
                  ul,
                  ol {
                    font-family: sans-serif;
                    font-size: 14px;
                    font-weight: normal;
                    margin: 0;
                    margin-bottom: 15px; 
                  }
                    p li,
                    ul li,
                    ol li {
                      list-style-position: inside;
                      margin-left: 5px; 
                  }
            
                  a {
                    color: #3498db;
                    text-decoration: underline; 
                  }
            
                  /* -------------------------------------
                      BUTTONS
                  ------------------------------------- */
                  .btn {
                    box-sizing: border-box;
                    width: 100%; }
                    .btn > tbody > tr > td {
                      padding-bottom: 15px; }
                    .btn table {
                      width: auto; 
                  }
                    .btn table td {
                      background-color: #ffffff;
                      border-radius: 5px;
                      text-align: center; 
                  }
                    .btn a {
                      background-color: #ffffff;
                      border: solid 1px #3498db;
                      border-radius: 5px;
                      box-sizing: border-box;
                      color: #3498db;
                      cursor: pointer;
                      display: inline-block;
                      font-size: 14px;
                      font-weight: bold;
                      margin: 0;
                      padding: 12px 25px;
                      text-decoration: none;
                      text-transform: capitalize; 
                  }
            
                  .btn-primary table td {
                    background-color: #3498db; 
                  }
            
                  .btn-primary a {
                    background-color: #3498db;
                    border-color: #3498db;
                    color: #ffffff; 
                  }
            
                  /* -------------------------------------
                      OTHER STYLES THAT MIGHT BE USEFUL
                  ------------------------------------- */
                  .last {
                    margin-bottom: 0; 
                  }
            
                  .first {
                    margin-top: 0; 
                  }
            
                  .align-center {
                    text-align: center; 
                  }
            
                  .align-right {
                    text-align: right; 
                  }
            
                  .align-left {
                    text-align: left; 
                  }
            
                  .clear {
                    clear: both; 
                  }
            
                  .mt0 {
                    margin-top: 0; 
                  }
            
                  .mb0 {
                    margin-bottom: 0; 
                  }
            
                  .preheader {
                    color: transparent;
                    display: none;
                    height: 0;
                    max-height: 0;
                    max-width: 0;
                    opacity: 0;
                    overflow: hidden;
                    mso-hide: all;
                    visibility: hidden;
                    width: 0; 
                  }
            
                  .powered-by a {
                    text-decoration: none; 
                  }
            
                  hr {
                    border: 0;
                    border-bottom: 1px solid #f6f6f6;
                    margin: 20px 0; 
                  }
            
                  /* -------------------------------------
                      RESPONSIVE AND MOBILE FRIENDLY STYLES
                  ------------------------------------- */
                  @media only screen and (max-width: 620px) {
                    table[class=body] h1 {
                      font-size: 28px !important;
                      margin-bottom: 10px !important; 
                    }
                    table[class=body] p,
                    table[class=body] ul,
                    table[class=body] ol,
                    table[class=body] td,
                    table[class=body] span,
                    table[class=body] a {
                      font-size: 16px !important; 
                    }
                    table[class=body] .wrapper,
                    table[class=body] .article {
                      padding: 10px !important; 
                    }
                    table[class=body] .content {
                      padding: 0 !important; 
                    }
                    table[class=body] .container {
                      padding: 0 !important;
                      width: 100% !important; 
                    }
                    table[class=body] .main {
                      border-left-width: 0 !important;
                      border-radius: 0 !important;
                      border-right-width: 0 !important; 
                    }
                    table[class=body] .btn table {
                      width: 100% !important; 
                    }
                    table[class=body] .btn a {
                      width: 100% !important; 
                    }
                    table[class=body] .img-responsive {
                      height: auto !important;
                      max-width: 100% !important;
                      width: auto !important; 
                    }
                  }
            
                  /* -------------------------------------
                      PRESERVE THESE STYLES IN THE HEAD
                  ------------------------------------- */
                  @media all {
                    .ExternalClass {
                      width: 100%; 
                    }
                    .ExternalClass,
                    .ExternalClass p,
                    .ExternalClass span,
                    .ExternalClass font,
                    .ExternalClass td,
                    .ExternalClass div {
                      line-height: 100%; 
                    }
                    .apple-link a {
                      color: inherit !important;
                      font-family: inherit !important;
                      font-size: inherit !important;
                      font-weight: inherit !important;
                      line-height: inherit !important;
                      text-decoration: none !important; 
                    }
                    #MessageViewBody a {
                      color: inherit;
                      text-decoration: none;
                      font-size: inherit;
                      font-family: inherit;
                      font-weight: inherit;
                      line-height: inherit;
                    }
                    .btn-primary table td:hover {
                      background-color: #34495e !important; 
                    }
                    .btn-primary a:hover {
                      background-color: #34495e !important;
                      border-color: #34495e !important; 
                    } 
                  }
            
                </style>
              </head>
              <body class="">
                <span class="preheader">This is preheader text. Some clients will show this text as a preview.</span>
                <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="body">
                  <tr>
                    <td>&nbsp;</td>
                    <td class="container">
                      <div class="content">
            
                        <!-- START CENTERED WHITE CONTAINER -->
                        <table role="presentation" class="main">
            
                          <!-- START MAIN CONTENT AREA -->
                          <tr>
                            <td class="wrapper">
                              <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                  <td>
                                    <p>Hai '.strtoupper($users['fullname']).',</p>
                                    <p>Dokumen anda sedang diproses oleh petugas kami, dimohon untuk menunggu hingga pemberitahuan selanjutnya.</p><br>
                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0" align="center">
                                      <tbody>
                                        <tr>
                                          <td align="center">
                                            <table role="presentation" border="0" cellpadding="0" cellspacing="0" align="center">
                                              <tbody>
                                                <tr>
                                                  <td></td>
                                                </tr>
                                              </tbody>
                                            </table>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </td>
                                </tr>
                              </table>
                            </td>
                          </tr>
            
                        <!-- END MAIN CONTENT AREA -->
                        </table>
                        <!-- END CENTERED WHITE CONTAINER -->
            
                        <!-- START FOOTER -->
                        <div class="footer">
                          <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                              <td class="content-block">
                                <span class="apple-link">Rutan Kelas I Bandung '.date("Y").'</span>
                              </td>
                            </tr>
                            <tr>
                              <td class="content-block powered-by">
                                Powered by <a href="https://greetings.id">Greetings Indonesia</a>
                              </td>
                            </tr>
                          </table>
                        </div>
                        <!-- END FOOTER -->
            
                      </div>
                    </td>
                    <td>&nbsp;</td>
                  </tr>
                </table>
              </body>
            </html>';

            $this->load->library('email');
			      $this->email->initialize($config);
            // $mail->setFrom('no-reply@rutankelas1bdg.com', 'no-reply@rutankelas1bdg.com');
            // $mail->addReplyTo('no-reply@rutankelas1bdg.com', 'no-reply@rutankelas1bdg.com'); //user email
            // $mail->addAddress($users['email']);
            // // $this->email->attach('#');
            // $mail->Subject = 'Dokumen "'.$file_id.'" anda sedang diproses';
            // $mail->isHTML(true);
            // $mail->Body = $body_email;

            $this->email->from('no-reply@rutankelas1bdg.com', 'no-reply@rutankelas1bdg.com');
            $this->email->to($users['email']);
            $this->email->subject('Dokumen "'.$file_id.'" anda sedang diproses');
            $this->email->message($body_email);

            if ($this->email->send()) {
              $log = [
                'logSurat_id' => '',
                'users_id' => $users_id,
                'log_title' => 'Berkas anda sedang diproses',
                'log_desc' => 'Harap menunggu, berkas anda sedang diproses',
                'log_time' => date('Y-m-d H:i:s'),
              ];
                $this->db->query("UPDATE file SET review = 'True' WHERE file_id = '$file_id'");
				$this->admin_model->log($log, 'logSurat');
				$this->session->set_flashdata('message', '<div class="alert alert-success" id="success-alert" role="alert">Dokumen yang anda pilih sedang ditinjau<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
				redirect(base_url("admin/main/dokumen"));
            } else {
				echo $this->email->print_debugger();
                $this->session->set_flashdata('message', '<div class="alert alert-danger" id="danger-alert" role="alert">Dokumen yang anda pilih gagal ditinjau<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
				redirect(base_url("admin/main/dokumen"));
            }

		}else{
            $this->session->set_flashdata('message', '<div class="alert alert-danger" id="danger-alert" role="alert">Dokumen yang anda pilih gagal ditinjau<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
			redirect(base_url("admin/main/dokumen"));
		}
	}

  public function detail_dokumen($id)
  {
    $this->load->helper('url');
    $id = $this->uri->segment(4);
    $data['nara'] = $this->db->query("SELECT * FROM narapidana JOIN users ON narapidana.users_id = users.users_id WHERE narapidana.users_id = '$id'")->row_array();
    $data['file'] = $this->db->query("SELECT * FROM file WHERE users_id = '$id'")->row_array();

    $data['view'] = 'admin/detail_dok';
		$this->load->view('admin/layout', $data);
  }

  public function update_dokumen()
	{
		$this->form_validation->set_rules('file_id','ID File','required');
		$this->form_validation->set_rules('users_id','ID User','required');
		$this->form_validation->set_rules('status','Status','required');
 
		if($this->form_validation->run() != false){
			date_default_timezone_set('Asia/Jakarta');
      $file_id = $this->input->post('file_id');
      $users_id = $this->input->post('users_id');
      $revisi_desc = $this->input->post('revisi_desc');
      $status = $this->input->post('status');
			$users = $this->db->query("SELECT * FROM users WHERE users_id = '$users_id'")->row_array();
			$cek = $this->db->query("SELECT * FROM file WHERE file_id = '$file_id'")->row_array();

			$config = [
				'mailtype'  => 'html',
				'charset'   => 'utf-8',
				'protocol'  => 'smtp',
				'smtp_host' => 'smtp.googlemail.com',
				'smtp_user' => 'stormxzero1@gmail.com',  // Email gmail
				'smtp_pass'   => 'minuman123',  // Password gmail
				'smtp_crypto' => 'ssl',
				'smtp_port'   => 465,
				'crlf'    => "\r\n",
				'newline' => "\r\n"
			];

			// $response = false;
            // $mail = new PHPMailer(true);
            // $mail->isSMTP();
            // $mail->Mailer = "smtp";
            // $mail->Host     = 'smtp.googlemail.com'; 
            // $mail->SMTPAuth = true;
            // $mail->Username = 'stormxzero1@gmail.com';
            // $mail->Password = 'minuman123'; 
            // $mail->Priority = 1;
			// $mail->SMTPDebug = 1;
			// $mail->SMTPSecure = 'tls';
            // $mail->Port     = 465;
			// $mail->SMTPOptions = array(
			// 	'ssl' => array(
			// 		'verify_peer' => false,
			// 		'verify_peer_name' => false,
			// 		'allow_self_signed' => true
			// 	)
			// );

      // STATUS REVISI
      if($status == 'Revision'){
      $body_revisi = '
      <!doctype html>
      <html>

      <head>
      	<meta name="viewport" content="width=device-width" />
      	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      	<title>Notifikasi Sistem Surat Penjamin Rutan Kelas 1 Bandung</title>
      	<style>
      		/* -------------------------------------
                    GLOBAL RESETS
                ------------------------------------- */

      		/*All the styling goes here*/

      		img {
      			border: none;
      			-ms-interpolation-mode: bicubic;
      			max-width: 100%;
      		}

      		body {
      			background-color: #f6f6f6;
      			font-family: sans-serif;
      			-webkit-font-smoothing: antialiased;
      			font-size: 14px;
      			line-height: 1.4;
      			margin: 0;
      			padding: 0;
      			-ms-text-size-adjust: 100%;
      			-webkit-text-size-adjust: 100%;
      		}

      		table {
      			border-collapse: separate;
      			mso-table-lspace: 0pt;
      			mso-table-rspace: 0pt;
      			width: 100%;
      		}

      		table td {
      			font-family: sans-serif;
      			font-size: 14px;
      			vertical-align: top;
      		}

      		/* -------------------------------------
                    BODY & CONTAINER
                ------------------------------------- */

      		.body {
      			background-color: #f6f6f6;
      			width: 100%;
      		}

      		/* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
      		.container {
      			display: block;
      			margin: 0 auto !important;
      			/* makes it centered */
      			max-width: 580px;
      			padding: 10px;
      			width: 580px;
      		}

      		/* This should also be a block element, so that it will fill 100% of the .container */
      		.content {
      			box-sizing: border-box;
      			display: block;
      			margin: 0 auto;
      			max-width: 580px;
      			padding: 10px;
      		}

      		/* -------------------------------------
                    HEADER, FOOTER, MAIN
                ------------------------------------- */
      		.main {
      			background: #ffffff;
      			border-radius: 3px;
      			width: 100%;
      		}

      		.wrapper {
      			box-sizing: border-box;
      			padding: 20px;
      		}

      		.content-block {
      			padding-bottom: 10px;
      			padding-top: 10px;
      		}

      		.footer {
      			clear: both;
      			margin-top: 10px;
      			text-align: center;
      			width: 100%;
      		}

      		.footer td,
      		.footer p,
      		.footer span,
      		.footer a {
      			color: #999999;
      			font-size: 12px;
      			text-align: center;
      		}

      		/* -------------------------------------
                    TYPOGRAPHY
                ------------------------------------- */
      		h1,
      		h2,
      		h3,
      		h4 {
      			color: #000000;
      			font-family: sans-serif;
      			font-weight: 400;
      			line-height: 1.4;
      			margin: 0;
      			margin-bottom: 30px;
      		}

      		h1 {
      			font-size: 35px;
      			font-weight: 300;
      			text-align: center;
      			text-transform: capitalize;
      		}

      		p,
      		ul,
      		ol {
      			font-family: sans-serif;
      			font-size: 14px;
      			font-weight: normal;
      			margin: 0;
      			margin-bottom: 15px;
      		}

      		p li,
      		ul li,
      		ol li {
      			list-style-position: inside;
      			margin-left: 5px;
      		}

      		a {
      			color: #3498db;
      			text-decoration: underline;
      		}

      		/* -------------------------------------
                    BUTTONS
                ------------------------------------- */
      		.btn {
      			box-sizing: border-box;
      			width: 100%;
      		}

      		.btn>tbody>tr>td {
      			padding-bottom: 15px;
      		}

      		.btn table {
      			width: auto;
      		}

      		.btn table td {
      			background-color: #ffffff;
      			border-radius: 5px;
      			text-align: center;
      		}

      		.btn a {
      			background-color: #ffffff;
      			border: solid 1px #3498db;
      			border-radius: 5px;
      			box-sizing: border-box;
      			color: #3498db;
      			cursor: pointer;
      			display: inline-block;
      			font-size: 14px;
      			font-weight: bold;
      			margin: 0;
      			padding: 12px 25px;
      			text-decoration: none;
      			text-transform: capitalize;
      		}

      		.btn-primary table td {
      			background-color: #3498db;
      		}

      		.btn-primary a {
      			background-color: #3498db;
      			border-color: #3498db;
      			color: #ffffff;
      		}

      		/* -------------------------------------
                    OTHER STYLES THAT MIGHT BE USEFUL
                ------------------------------------- */
      		.last {
      			margin-bottom: 0;
      		}

      		.first {
      			margin-top: 0;
      		}

      		.align-center {
      			text-align: center;
      		}

      		.align-right {
      			text-align: right;
      		}

      		.align-left {
      			text-align: left;
      		}

      		.clear {
      			clear: both;
      		}

      		.mt0 {
      			margin-top: 0;
      		}

      		.mb0 {
      			margin-bottom: 0;
      		}

      		.preheader {
      			color: transparent;
      			display: none;
      			height: 0;
      			max-height: 0;
      			max-width: 0;
      			opacity: 0;
      			overflow: hidden;
      			mso-hide: all;
      			visibility: hidden;
      			width: 0;
      		}

      		.powered-by a {
      			text-decoration: none;
      		}

      		hr {
      			border: 0;
      			border-bottom: 1px solid #f6f6f6;
      			margin: 20px 0;
      		}

      		/* -------------------------------------
                    RESPONSIVE AND MOBILE FRIENDLY STYLES
                ------------------------------------- */
      		@media only screen and (max-width: 620px) {
      			table[class=body] h1 {
      				font-size: 28px !important;
      				margin-bottom: 10px !important;
      			}

      			table[class=body] p,
      			table[class=body] ul,
      			table[class=body] ol,
      			table[class=body] td,
      			table[class=body] span,
      			table[class=body] a {
      				font-size: 16px !important;
      			}

      			table[class=body] .wrapper,
      			table[class=body] .article {
      				padding: 10px !important;
      			}

      			table[class=body] .content {
      				padding: 0 !important;
      			}

      			table[class=body] .container {
      				padding: 0 !important;
      				width: 100% !important;
      			}

      			table[class=body] .main {
      				border-left-width: 0 !important;
      				border-radius: 0 !important;
      				border-right-width: 0 !important;
      			}

      			table[class=body] .btn table {
      				width: 100% !important;
      			}

      			table[class=body] .btn a {
      				width: 100% !important;
      			}

      			table[class=body] .img-responsive {
      				height: auto !important;
      				max-width: 100% !important;
      				width: auto !important;
      			}
      		}

      		/* -------------------------------------
                    PRESERVE THESE STYLES IN THE HEAD
                ------------------------------------- */
      		@media all {
      			.ExternalClass {
      				width: 100%;
      			}

      			.ExternalClass,
      			.ExternalClass p,
      			.ExternalClass span,
      			.ExternalClass font,
      			.ExternalClass td,
      			.ExternalClass div {
      				line-height: 100%;
      			}

      			.apple-link a {
      				color: inherit !important;
      				font-family: inherit !important;
      				font-size: inherit !important;
      				font-weight: inherit !important;
      				line-height: inherit !important;
      				text-decoration: none !important;
      			}

      			#MessageViewBody a {
      				color: inherit;
      				text-decoration: none;
      				font-size: inherit;
      				font-family: inherit;
      				font-weight: inherit;
      				line-height: inherit;
      			}

      			.btn-primary table td:hover {
      				background-color: #34495e !important;
      			}

      			.btn-primary a:hover {
      				background-color: #34495e !important;
      				border-color: #34495e !important;
      			}
      		}
      	</style>
      </head>

      <body class="">
      	<span class="preheader">This is preheader text. Some clients will show this text as a preview.</span>
      	<table role="presentation" border="0" cellpadding="0" cellspacing="0" class="body">
      		<tr>
      			<td>&nbsp;</td>
      			<td class="container">
      				<div class="content">

      					<!-- START CENTERED WHITE CONTAINER -->
      					<table role="presentation" class="main">

      						<!-- START MAIN CONTENT AREA -->
      						<tr>
      							<td class="wrapper">
      								<table role="presentation" border="0" cellpadding="0" cellspacing="0">
      									<tr>
      										<td>
      											<p>Hai '.strtoupper($users['fullname']).',</p>
      											<p>Dokumen anda telah ditinjau dan ada beberapa dokumen yang harus diperbaiki.</p><br>
      											<p>Silahkan login pada sistem kami untuk melihat dokumen yang harus diperbaiki dan unggah
      												kembali file dokumen yang telah diperbaiki sebelum kami tinjau kembali.</p><br>
      											<table role="presentation" border="0" cellpadding="0" cellspacing="0" align="center">
      												<tbody>
      													<tr>
      														<td align="center">
      															<table role="presentation" border="0" cellpadding="0" cellspacing="0"
      																align="center">
      																<tbody>
      																	<tr>
      																		<td></td>
      																	</tr>
      																</tbody>
      															</table>
      														</td>
      													</tr>
      												</tbody>
      											</table>
      										</td>
      									</tr>
      								</table>
      							</td>
      						</tr>

      						<!-- END MAIN CONTENT AREA -->
      					</table>
      					<!-- END CENTERED WHITE CONTAINER -->

      					<!-- START FOOTER -->
      					<div class="footer">
      						<table role="presentation" border="0" cellpadding="0" cellspacing="0">
      							<tr>
      								<td class="content-block">
      									<span class="apple-link">Rutan Kelas I Bandung '.date("Y").'</span>
      								</td>
      							</tr>
      							<tr>
      								<td class="content-block powered-by">
      									Powered by <a href="https://greetings.id">Greetings Indonesia</a>
      								</td>
      							</tr>
      						</table>
      					</div>
      					<!-- END FOOTER -->

      				</div>
      			</td>
      			<td>&nbsp;</td>
      		</tr>
      	</table>
      </body>

      </html>';

      $this->load->library('email');
      $this->email->initialize($config);
      // $mail->setFrom('no-reply@rutankelas1bdg.com', 'no-reply@rutankelas1bdg.com');
      // $mail->addReplyTo('no-reply@rutankelas1bdg.com', 'no-reply@rutankelas1bdg.com'); //user email
      // $mail->addAddress($users['email']);
      // // $this->email->attach('#');
      // $mail->Subject = 'Dokumen "'.$file_id.'" anda sedang diproses';
      // $mail->isHTML(true);
      // $mail->Body = $body_email;

      $this->email->from('no-reply@rutankelas1bdg.com', 'no-reply@rutankelas1bdg.com');
      $this->email->to($users['email']);
      $this->email->subject('Dokumen "'.$file_id.'" anda harus diperbaiki');
      $this->email->message($body_revisi);

      if ($this->email->send()) {
      $log = [
      'logSurat_id' => '',
      'users_id' => $users_id,
      'log_title' => 'Berkas anda harus diperbaiki',
      'log_desc' => 'Harap melihat deskripsi revisi pada panel petunjuk dan mengunggah kembali dokumen yang telah
      diperbaiki',
      'log_time' => date('Y-m-d H:i:s'),
      ];
      $rev = [
      'revisi_id' => '',
      'users_id' => $users_id,
      'revisi_desc' => $revisi_desc
      ];
      $this->db->query("UPDATE file SET status = 'Revision' WHERE file_id = '$file_id'");
      $this->admin_model->log($log, 'logSurat');
      $this->admin_model->log($rev, 'revisi');
      $this->session->set_flashdata('message', '<div class="alert alert-success" id="success-alert" role="alert">
      	Dokumen yang anda pilih telah ditanggapi dengan revisi<button type="button" class="close" data-dismiss="alert"
      		aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
      redirect(base_url("admin/main/dokumen"));
      } else {
      echo $this->email->print_debugger();
      $this->session->set_flashdata('message', '<div class="alert alert-danger" id="danger-alert" role="alert">Dokumen
      	yang anda pilih gagal ditanggapi<button type="button" class="close" data-dismiss="alert"
      		aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
      redirect(base_url("admin/main/dokumen"));
      }
      // STATUS VERIF
      } elseif($status=='Verified'){
      $body_verif = '
      <!doctype html>
      <html>

      <head>
      	<meta name="viewport" content="width=device-width" />
      	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      	<title>Notifikasi Sistem Surat Penjamin Rutan Kelas 1 Bandung</title>
      	<style>
      		/* -------------------------------------
                    GLOBAL RESETS
                ------------------------------------- */

      		/*All the styling goes here*/

      		img {
      			border: none;
      			-ms-interpolation-mode: bicubic;
      			max-width: 100%;
      		}

      		body {
      			background-color: #f6f6f6;
      			font-family: sans-serif;
      			-webkit-font-smoothing: antialiased;
      			font-size: 14px;
      			line-height: 1.4;
      			margin: 0;
      			padding: 0;
      			-ms-text-size-adjust: 100%;
      			-webkit-text-size-adjust: 100%;
      		}

      		table {
      			border-collapse: separate;
      			mso-table-lspace: 0pt;
      			mso-table-rspace: 0pt;
      			width: 100%;
      		}

      		table td {
      			font-family: sans-serif;
      			font-size: 14px;
      			vertical-align: top;
      		}

      		/* -------------------------------------
                    BODY & CONTAINER
                ------------------------------------- */

      		.body {
      			background-color: #f6f6f6;
      			width: 100%;
      		}

      		/* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
      		.container {
      			display: block;
      			margin: 0 auto !important;
      			/* makes it centered */
      			max-width: 580px;
      			padding: 10px;
      			width: 580px;
      		}

      		/* This should also be a block element, so that it will fill 100% of the .container */
      		.content {
      			box-sizing: border-box;
      			display: block;
      			margin: 0 auto;
      			max-width: 580px;
      			padding: 10px;
      		}

      		/* -------------------------------------
                    HEADER, FOOTER, MAIN
                ------------------------------------- */
      		.main {
      			background: #ffffff;
      			border-radius: 3px;
      			width: 100%;
      		}

      		.wrapper {
      			box-sizing: border-box;
      			padding: 20px;
      		}

      		.content-block {
      			padding-bottom: 10px;
      			padding-top: 10px;
      		}

      		.footer {
      			clear: both;
      			margin-top: 10px;
      			text-align: center;
      			width: 100%;
      		}

      		.footer td,
      		.footer p,
      		.footer span,
      		.footer a {
      			color: #999999;
      			font-size: 12px;
      			text-align: center;
      		}

      		/* -------------------------------------
                    TYPOGRAPHY
                ------------------------------------- */
      		h1,
      		h2,
      		h3,
      		h4 {
      			color: #000000;
      			font-family: sans-serif;
      			font-weight: 400;
      			line-height: 1.4;
      			margin: 0;
      			margin-bottom: 30px;
      		}

      		h1 {
      			font-size: 35px;
      			font-weight: 300;
      			text-align: center;
      			text-transform: capitalize;
      		}

      		p,
      		ul,
      		ol {
      			font-family: sans-serif;
      			font-size: 14px;
      			font-weight: normal;
      			margin: 0;
      			margin-bottom: 15px;
      		}

      		p li,
      		ul li,
      		ol li {
      			list-style-position: inside;
      			margin-left: 5px;
      		}

      		a {
      			color: #3498db;
      			text-decoration: underline;
      		}

      		/* -------------------------------------
                    BUTTONS
                ------------------------------------- */
      		.btn {
      			box-sizing: border-box;
      			width: 100%;
      		}

      		.btn>tbody>tr>td {
      			padding-bottom: 15px;
      		}

      		.btn table {
      			width: auto;
      		}

      		.btn table td {
      			background-color: #ffffff;
      			border-radius: 5px;
      			text-align: center;
      		}

      		.btn a {
      			background-color: #ffffff;
      			border: solid 1px #3498db;
      			border-radius: 5px;
      			box-sizing: border-box;
      			color: #3498db;
      			cursor: pointer;
      			display: inline-block;
      			font-size: 14px;
      			font-weight: bold;
      			margin: 0;
      			padding: 12px 25px;
      			text-decoration: none;
      			text-transform: capitalize;
      		}

      		.btn-primary table td {
      			background-color: #3498db;
      		}

      		.btn-primary a {
      			background-color: #3498db;
      			border-color: #3498db;
      			color: #ffffff;
      		}

      		/* -------------------------------------
                    OTHER STYLES THAT MIGHT BE USEFUL
                ------------------------------------- */
      		.last {
      			margin-bottom: 0;
      		}

      		.first {
      			margin-top: 0;
      		}

      		.align-center {
      			text-align: center;
      		}

      		.align-right {
      			text-align: right;
      		}

      		.align-left {
      			text-align: left;
      		}

      		.clear {
      			clear: both;
      		}

      		.mt0 {
      			margin-top: 0;
      		}

      		.mb0 {
      			margin-bottom: 0;
      		}

      		.preheader {
      			color: transparent;
      			display: none;
      			height: 0;
      			max-height: 0;
      			max-width: 0;
      			opacity: 0;
      			overflow: hidden;
      			mso-hide: all;
      			visibility: hidden;
      			width: 0;
      		}

      		.powered-by a {
      			text-decoration: none;
      		}

      		hr {
      			border: 0;
      			border-bottom: 1px solid #f6f6f6;
      			margin: 20px 0;
      		}

      		/* -------------------------------------
                    RESPONSIVE AND MOBILE FRIENDLY STYLES
                ------------------------------------- */
      		@media only screen and (max-width: 620px) {
      			table[class=body] h1 {
      				font-size: 28px !important;
      				margin-bottom: 10px !important;
      			}

      			table[class=body] p,
      			table[class=body] ul,
      			table[class=body] ol,
      			table[class=body] td,
      			table[class=body] span,
      			table[class=body] a {
      				font-size: 16px !important;
      			}

      			table[class=body] .wrapper,
      			table[class=body] .article {
      				padding: 10px !important;
      			}

      			table[class=body] .content {
      				padding: 0 !important;
      			}

      			table[class=body] .container {
      				padding: 0 !important;
      				width: 100% !important;
      			}

      			table[class=body] .main {
      				border-left-width: 0 !important;
      				border-radius: 0 !important;
      				border-right-width: 0 !important;
      			}

      			table[class=body] .btn table {
      				width: 100% !important;
      			}

      			table[class=body] .btn a {
      				width: 100% !important;
      			}

      			table[class=body] .img-responsive {
      				height: auto !important;
      				max-width: 100% !important;
      				width: auto !important;
      			}
      		}

      		/* -------------------------------------
                    PRESERVE THESE STYLES IN THE HEAD
                ------------------------------------- */
      		@media all {
      			.ExternalClass {
      				width: 100%;
      			}

      			.ExternalClass,
      			.ExternalClass p,
      			.ExternalClass span,
      			.ExternalClass font,
      			.ExternalClass td,
      			.ExternalClass div {
      				line-height: 100%;
      			}

      			.apple-link a {
      				color: inherit !important;
      				font-family: inherit !important;
      				font-size: inherit !important;
      				font-weight: inherit !important;
      				line-height: inherit !important;
      				text-decoration: none !important;
      			}

      			#MessageViewBody a {
      				color: inherit;
      				text-decoration: none;
      				font-size: inherit;
      				font-family: inherit;
      				font-weight: inherit;
      				line-height: inherit;
      			}

      			.btn-primary table td:hover {
      				background-color: #34495e !important;
      			}

      			.btn-primary a:hover {
      				background-color: #34495e !important;
      				border-color: #34495e !important;
      			}
      		}
      	</style>
      </head>

      <body class="">
      	<span class="preheader">This is preheader text. Some clients will show this text as a preview.</span>
      	<table role="presentation" border="0" cellpadding="0" cellspacing="0" class="body">
      		<tr>
      			<td>&nbsp;</td>
      			<td class="container">
      				<div class="content">

      					<!-- START CENTERED WHITE CONTAINER -->
      					<table role="presentation" class="main">

      						<!-- START MAIN CONTENT AREA -->
      						<tr>
      							<td class="wrapper">
      								<table role="presentation" border="0" cellpadding="0" cellspacing="0">
      									<tr>
      										<td>
      											<p>Hai '.strtoupper($users['fullname']).',</p>
      											<p>Selamat, dokumen anda telah disetujui.</p><br>
      											<p>Silahkan login pada sistem kami untuk melihat petunjuk selanjutnya.</p><br>
      											<table role="presentation" border="0" cellpadding="0" cellspacing="0" align="center">
      												<tbody>
      													<tr>
      														<td align="center">
      															<table role="presentation" border="0" cellpadding="0" cellspacing="0"
      																align="center">
      																<tbody>
      																	<tr>
      																		<td></td>
      																	</tr>
      																</tbody>
      															</table>
      														</td>
      													</tr>
      												</tbody>
      											</table>
      										</td>
      									</tr>
      								</table>
      							</td>
      						</tr>

      						<!-- END MAIN CONTENT AREA -->
      					</table>
      					<!-- END CENTERED WHITE CONTAINER -->

      					<!-- START FOOTER -->
      					<div class="footer">
      						<table role="presentation" border="0" cellpadding="0" cellspacing="0">
      							<tr>
      								<td class="content-block">
      									<span class="apple-link">Rutan Kelas I Bandung '.date("Y").'</span>
      								</td>
      							</tr>
      							<tr>
      								<td class="content-block powered-by">
      									Powered by <a href="https://greetings.id">Greetings Indonesia</a>
      								</td>
      							</tr>
      						</table>
      					</div>
      					<!-- END FOOTER -->

      				</div>
      			</td>
      			<td>&nbsp;</td>
      		</tr>
      	</table>
      </body>

      </html>';

      $this->load->library('email');
      $this->email->initialize($config);
      // $mail->setFrom('no-reply@rutankelas1bdg.com', 'no-reply@rutankelas1bdg.com');
      // $mail->addReplyTo('no-reply@rutankelas1bdg.com', 'no-reply@rutankelas1bdg.com'); //user email
      // $mail->addAddress($users['email']);
      // // $this->email->attach('#');
      // $mail->Subject = 'Dokumen "'.$file_id.'" anda sedang diproses';
      // $mail->isHTML(true);
      // $mail->Body = $body_email;

      $this->email->from('no-reply@rutankelas1bdg.com', 'no-reply@rutankelas1bdg.com');
      $this->email->to($users['email']);
      $this->email->subject('Dokumen "'.$file_id.'" anda telah disetujui');
      $this->email->message($body_verif);

      if ($this->email->send()) {
      $log = [
      'logSurat_id' => '',
      'users_id' => $users_id,
      'log_title' => 'Berkas anda telah disetujui',
      'log_desc' => 'Harap melihat deskripsi di panel petunjuk pada halaman dashboard anda untuk mengirimkan dokumen
      asli',
      'log_time' => date('Y-m-d H:i:s'),
      ];
      $this->db->query("UPDATE file SET status = 'Verified', review = 'True' WHERE file_id = '$file_id'");
      $this->admin_model->log($log, 'logSurat');
      $this->session->set_flashdata('message', '<div class="alert alert-success" id="success-alert" role="alert">
      	Dokumen yang anda pilih telah disetujui<button type="button" class="close" data-dismiss="alert"
      		aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
      redirect(base_url("admin/main/dokumen"));
      } else {
      echo $this->email->print_debugger();
      $this->session->set_flashdata('message', '<div class="alert alert-danger" id="danger-alert" role="alert">Dokumen
      	yang anda pilih gagal disetujui<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
      			aria-hidden="true">&times;</span></button></div>');
      redirect(base_url("admin/main/dokumen"));
      }
      // STATUS PENDING
      } else{
      $this->session->set_flashdata('message', '<div class="alert alert-danger" id="danger-alert" role="alert">Dokumen
      	tidak boleh diganti menjadi pending kembali<button type="button" class="close" data-dismiss="alert"
      		aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
      redirect(base_url("admin/main/dokumen"));
      }

		}else{
            $this->session->set_flashdata('message', '<div class="alert alert-danger" id="danger-alert" role="alert">Dokumen yang anda pilih gagal ditanggapi<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
			redirect(base_url("admin/main/dokumen"));
		}
	}
}
