<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

require APPPATH.'libraries/phpmailer/src/Exception.php';
require APPPATH.'libraries/phpmailer/src/PHPMailer.php';
require APPPATH.'libraries/phpmailer/src/SMTP.php';

class Main extends CI_Controller {

	public function __construct()
    {
		parent::__construct();
		$this->load->model('LoginModel');
	}

	public function index()
	{
		$this->load->view('index');
	}

	public function login()
	{
		$this->load->view('login');
	}

	public function track()
	{
		$file_id = strtoupper($this->input->post('file_id'));
		$data['logs'] = $this->db->query("SELECT logSurat.log_title, logSurat.log_desc, logSurat.log_time FROM logSurat JOIN file ON logSurat.users_id = file.users_id WHERE file.file_id = '$file_id' ORDER BY logSurat.logSurat_id DESC")->result();
		$this->load->view('track', $data);
	}

	public function login_process() {
		// $this->load->library('encryption');
		$this->form_validation->set_rules('email', 'Email', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');

		if($this->form_validation->run() == TRUE) {
			$data = array(
				'email' => $this->input->post('email'),
				'password' => md5($this->input->post('password'))
			);

			$result = $this->LoginModel->login($data);
			// var_dump($result->row_array());
			// die();
			if(count($result->result()) > 0) { 
				$arr = $result->row_array(); 
				if($arr['level'] == 'Admin') {
					date_default_timezone_set('Asia/Jakarta');
					$data_session = array(
						'users_id' => $arr['users_id'],
						'nama' => $arr['fullname'],
						'level' => $arr['level'],
						'email' => $arr['email'],
						'status' => True
						);
					$date = date('Y-m-d H:i:s');
					$this->session->set_userdata($data_session);
					$this->db->query("UPDATE users SET last_login = '$date' WHERE users_id = '$arr[users_id]'");
					redirect(base_url("admin/main"));
				} else if ($arr['isActive'] == 'True') {
					date_default_timezone_set('Asia/Jakarta');
					$data_session = array(
						'users_id' => $arr['users_id'],
						'nama' => $arr['fullname'],
						'level' => $arr['level'],
						'email' => $arr['email'],
						'status' => True
						);
					$date = date('Y-m-d H:i:s');
					$this->db->query("UPDATE users SET last_login = '$date' WHERE users_id = '$arr[users_id]' ");
					$this->session->set_userdata($data_session);
					redirect(base_url("dashboard"));
					// var_dump($data_session);
					// die();
				} else {
					echo '<script type="text/javascript">alert("Akun anda belum aktif! Silahkan periksa email anda (termasuk folder spam) dan masukan kode aktifasi disini."); window.location = "'.base_url('aktifasi').'";</script>';
				}
			}else{
				echo '<script type="text/javascript">alert("Email atau Kata Sandi anda salah!"); window.location = "'.base_url('login').'";</script>';
				// redirect('main/login');
			}
		}else{
			echo '<script type="text/javascript">alert("Email atau Kata Sandi anda salah!"); window.location = "'.base_url('login').'";</script>';
			// redirect('main/login');
		}

	}

	public function register() {
		$data['GetAllGender'] = $this->LoginModel->GetAllGender();
		$data['kode'] = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(5/strlen($x)) )),1,5);
		$this->load->view('register', $data);
	}

	public function activation() {
		$this->load->view('activation');
	}

	public function form_lupakatasandi() {
		$this->load->view('form_katasandi');
	}

	public function lupakatasandi() {
		$data['kode'] = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(5/strlen($x)) )),1,5);
		$this->load->view('lupa',$data);
	}

	public function activate() {
		$this->form_validation->set_rules('u_reg', 'Kode Aktifasi', 'trim|required');
		if($this->form_validation->run() == TRUE) {
			$kode = $this->input->post('u_reg');

			$this->db->query("UPDATE users SET isActive = 'True' WHERE u_reg = '$kode'");
			echo '<script type="text/javascript">alert("Akun anda telah aktif, silahkan masuk"); window.location = "'.base_url('login').'";</script>';
		}
	}

	public function proses_password() {
		$this->form_validation->set_rules('u_reg', 'Kode Aktifasi', 'trim|required');
		$this->form_validation->set_rules('password', 'Password Baru', 'trim|required');
		$this->form_validation->set_rules('password2', 'Konfirmasi Password Baru', 'trim|required');
		if($this->form_validation->run() == TRUE) {
			$kode = $this->input->post('u_reg');
			$password = md5($this->input->post('password'));
			$password2 = md5($this->input->post('password2'));
			// $cek = $this->db->query("SELECT password FROM users WHERE u_reg = '$kode'")->row_array();

			if($password == $password2){
				$this->db->query("UPDATE users SET password = '$password', isActive = 'True' WHERE u_reg = '$kode'");
				echo '<script type="text/javascript">
					alert("Password anda telah diperbaharui, silahkan masuk");
					window.location = "'.base_url('login').'";
				</script>';
			} else {
				echo '<script type="text/javascript">
					alert("Password baru dan konfirmasi password baru tidak sama!");
					window.location = "'.base_url('form_lupakatasandi').'";
				</script>';
			}
		}
	}

	public function proses_lupakatasandi() {

		$this->form_validation->set_rules('email', 'Email', 'trim|required');
		$this->form_validation->set_rules('u_reg', 'Kode Aktifasi', 'trim|required');

		if($this->form_validation->run() == TRUE) {
			$email = $this->input->post('email');
			$u_reg = $this->input->post('u_reg');

			$cek = $this->db->query("SELECT email, tel FROM users WHERE email = '$email'")->row_array();
			// $config = [
			// 	'mailtype'  => 'html',
			// 	'charset'   => 'utf-8',
			// 	'protocol'  => 'smtp',
			// 	'smtp_host' => 'smtp.googlemail.com',
			// 	'smtp_user' => 'stormxzero1@gmail.com',  // Email gmail
			// 	'smtp_pass'   => 'minuman123',  // Password gmail
			// 	'smtp_crypto' => 'ssl',
			// 	'smtp_port'   => 465,
			// 	'crlf'    => "\r\n",
			// 	'newline' => "\r\n"
			// ];

			$response = false;
            $mail = new PHPMailer(true);
            $mail->isSMTP();
            $mail->Mailer = "smtp";
            $mail->Host     = 'mail.greetings.id'; //sesuaikan sesuai nama domain hosting/server yang digunakan
            $mail->SMTPAuth = true;
            $mail->Username = 'rizkyabdul@greetings.id'; // user email
            $mail->Password = 'rizkyghani16021998'; // password email
            $mail->Priority = 1;
            // $mail->SMTPAutoTLS = false; 
            $mail->Port     = 587;

			$body_email = '<!doctype html>
			<html>
			  <head>
				<meta name="viewport" content="width=device-width" />
				<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
				<title>Notifikasi Sistem Surat Penjamin Rutan Kelas 1 Bandung</title>
				<style>
				  /* -------------------------------------
					  GLOBAL RESETS
				  ------------------------------------- */
				  
				  /*All the styling goes here*/
				  
				  img {
					border: none;
					-ms-interpolation-mode: bicubic;
					max-width: 100%; 
				  }
			
				  body {
					background-color: #f6f6f6;
					font-family: sans-serif;
					-webkit-font-smoothing: antialiased;
					font-size: 14px;
					line-height: 1.4;
					margin: 0;
					padding: 0;
					-ms-text-size-adjust: 100%;
					-webkit-text-size-adjust: 100%; 
				  }
			
				  table {
					border-collapse: separate;
					mso-table-lspace: 0pt;
					mso-table-rspace: 0pt;
					width: 100%; }
					table td {
					  font-family: sans-serif;
					  font-size: 14px;
					  vertical-align: top; 
				  }
			
				  /* -------------------------------------
					  BODY & CONTAINER
				  ------------------------------------- */
			
				  .body {
					background-color: #f6f6f6;
					width: 100%; 
				  }
			
				  /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
				  .container {
					display: block;
					margin: 0 auto !important;
					/* makes it centered */
					max-width: 580px;
					padding: 10px;
					width: 580px; 
				  }
			
				  /* This should also be a block element, so that it will fill 100% of the .container */
				  .content {
					box-sizing: border-box;
					display: block;
					margin: 0 auto;
					max-width: 580px;
					padding: 10px; 
				  }
			
				  /* -------------------------------------
					  HEADER, FOOTER, MAIN
				  ------------------------------------- */
				  .main {
					background: #ffffff;
					border-radius: 3px;
					width: 100%; 
				  }
			
				  .wrapper {
					box-sizing: border-box;
					padding: 20px; 
				  }
			
				  .content-block {
					padding-bottom: 10px;
					padding-top: 10px;
				  }
			
				  .footer {
					clear: both;
					margin-top: 10px;
					text-align: center;
					width: 100%; 
				  }
					.footer td,
					.footer p,
					.footer span,
					.footer a {
					  color: #999999;
					  font-size: 12px;
					  text-align: center; 
				  }
			
				  /* -------------------------------------
					  TYPOGRAPHY
				  ------------------------------------- */
				  h1,
				  h2,
				  h3,
				  h4 {
					color: #000000;
					font-family: sans-serif;
					font-weight: 400;
					line-height: 1.4;
					margin: 0;
					margin-bottom: 30px; 
				  }
			
				  h1 {
					font-size: 35px;
					font-weight: 300;
					text-align: center;
					text-transform: capitalize; 
				  }
			
				  p,
				  ul,
				  ol {
					font-family: sans-serif;
					font-size: 14px;
					font-weight: normal;
					margin: 0;
					margin-bottom: 15px; 
				  }
					p li,
					ul li,
					ol li {
					  list-style-position: inside;
					  margin-left: 5px; 
				  }
			
				  a {
					color: #3498db;
					text-decoration: underline; 
				  }
			
				  /* -------------------------------------
					  BUTTONS
				  ------------------------------------- */
				  .btn {
					box-sizing: border-box;
					width: 100%; }
					.btn > tbody > tr > td {
					  padding-bottom: 15px; }
					.btn table {
					  width: auto; 
				  }
					.btn table td {
					  background-color: #ffffff;
					  border-radius: 5px;
					  text-align: center; 
				  }
					.btn a {
					  background-color: #ffffff;
					  border: solid 1px #3498db;
					  border-radius: 5px;
					  box-sizing: border-box;
					  color: #3498db;
					  cursor: pointer;
					  display: inline-block;
					  font-size: 14px;
					  font-weight: bold;
					  margin: 0;
					  padding: 12px 25px;
					  text-decoration: none;
					  text-transform: capitalize; 
				  }
			
				  .btn-primary table td {
					background-color: #3498db; 
				  }
			
				  .btn-primary a {
					background-color: #3498db;
					border-color: #3498db;
					color: #ffffff; 
				  }
			
				  /* -------------------------------------
					  OTHER STYLES THAT MIGHT BE USEFUL
				  ------------------------------------- */
				  .last {
					margin-bottom: 0; 
				  }
			
				  .first {
					margin-top: 0; 
				  }
			
				  .align-center {
					text-align: center; 
				  }
			
				  .align-right {
					text-align: right; 
				  }
			
				  .align-left {
					text-align: left; 
				  }
			
				  .clear {
					clear: both; 
				  }
			
				  .mt0 {
					margin-top: 0; 
				  }
			
				  .mb0 {
					margin-bottom: 0; 
				  }
			
				  .preheader {
					color: transparent;
					display: none;
					height: 0;
					max-height: 0;
					max-width: 0;
					opacity: 0;
					overflow: hidden;
					mso-hide: all;
					visibility: hidden;
					width: 0; 
				  }
			
				  .powered-by a {
					text-decoration: none; 
				  }
			
				  hr {
					border: 0;
					border-bottom: 1px solid #f6f6f6;
					margin: 20px 0; 
				  }
			
				  /* -------------------------------------
					  RESPONSIVE AND MOBILE FRIENDLY STYLES
				  ------------------------------------- */
				  @media only screen and (max-width: 620px) {
					table[class=body] h1 {
					  font-size: 28px !important;
					  margin-bottom: 10px !important; 
					}
					table[class=body] p,
					table[class=body] ul,
					table[class=body] ol,
					table[class=body] td,
					table[class=body] span,
					table[class=body] a {
					  font-size: 16px !important; 
					}
					table[class=body] .wrapper,
					table[class=body] .article {
					  padding: 10px !important; 
					}
					table[class=body] .content {
					  padding: 0 !important; 
					}
					table[class=body] .container {
					  padding: 0 !important;
					  width: 100% !important; 
					}
					table[class=body] .main {
					  border-left-width: 0 !important;
					  border-radius: 0 !important;
					  border-right-width: 0 !important; 
					}
					table[class=body] .btn table {
					  width: 100% !important; 
					}
					table[class=body] .btn a {
					  width: 100% !important; 
					}
					table[class=body] .img-responsive {
					  height: auto !important;
					  max-width: 100% !important;
					  width: auto !important; 
					}
				  }
			
				  /* -------------------------------------
					  PRESERVE THESE STYLES IN THE HEAD
				  ------------------------------------- */
				  @media all {
					.ExternalClass {
					  width: 100%; 
					}
					.ExternalClass,
					.ExternalClass p,
					.ExternalClass span,
					.ExternalClass font,
					.ExternalClass td,
					.ExternalClass div {
					  line-height: 100%; 
					}
					.apple-link a {
					  color: inherit !important;
					  font-family: inherit !important;
					  font-size: inherit !important;
					  font-weight: inherit !important;
					  line-height: inherit !important;
					  text-decoration: none !important; 
					}
					#MessageViewBody a {
					  color: inherit;
					  text-decoration: none;
					  font-size: inherit;
					  font-family: inherit;
					  font-weight: inherit;
					  line-height: inherit;
					}
					.btn-primary table td:hover {
					  background-color: #34495e !important; 
					}
					.btn-primary a:hover {
					  background-color: #34495e !important;
					  border-color: #34495e !important; 
					} 
				  }
			
				</style>
			  </head>
			  <body class="">
				<span class="preheader">This is preheader text. Some clients will show this text as a preview.</span>
				<table role="presentation" border="0" cellpadding="0" cellspacing="0" class="body">
				  <tr>
					<td>&nbsp;</td>
					<td class="container">
					  <div class="content">
			
						<!-- START CENTERED WHITE CONTAINER -->
						<table role="presentation" class="main">
			
						  <!-- START MAIN CONTENT AREA -->
						  <tr>
							<td class="wrapper">
							  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
								<tr>
								  <td>
									<p>Kode unik anda adalah :</p><br>
									<h4>'.$u_reg.'</h4><br>
									<p>Masukan kode unik anda pada link dibawah</p><br>
									<p>'.base_url("form_lupakatasandi").'</p>
									<table role="presentation" border="0" cellpadding="0" cellspacing="0" align="center">
									  <tbody>
										<tr>
										  <td align="center">
											<table role="presentation" border="0" cellpadding="0" cellspacing="0" align="center">
											  <tbody>
												<tr>
												  <td></td>
												</tr>
											  </tbody>
											</table>
										  </td>
										</tr>
									  </tbody>
									</table>
								  </td>
								</tr>
							  </table>
							</td>
						  </tr>
			
						<!-- END MAIN CONTENT AREA -->
						</table>
						<!-- END CENTERED WHITE CONTAINER -->
			
						<!-- START FOOTER -->
						<div class="footer">
						  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
							<tr>
							  <td class="content-block">
								<span class="apple-link">Rutan Kelas I Bandung '.date("Y").'</span>
							  </td>
							</tr>
							<tr>
							  <td class="content-block powered-by">
								Powered by <a href="https://greetings.id">Greetings Indonesia</a>
							  </td>
							</tr>
						  </table>
						</div>
						<!-- END FOOTER -->
			
					  </div>
					</td>
					<td>&nbsp;</td>
				  </tr>
				</table>
			  </body>
			</html>';

			$this->load->library('email');
			// $this->email->initialize($config);
			$mail->setFrom('no-reply@rutankelas1bdg.com', 'no-reply@rutankelas1bdg.com');
			$mail->addReplyTo('no-reply@rutankelas1bdg.com', 'no-reply@rutankelas1bdg.com'); //user email
			$mail->addAddress($this->input->post('email'));
			// $this->email->attach('#');
			$mail->Subject = 'Lupa kata sandi akun Surat Penjamin Rutan Kelas 1 Bandung';
			$mail->isHTML(true);
			$mail->Body = $body_email;

			// $this->email->from('no-reply@rutankelas1bdg.com', 'no-reply@rutankelas1bdg.com');
			// $this->email->to($this->input->post('email'));
			// $this->email->subject('Lupa kata sandi akun Surat Penjamin Rutan Kelas 1 Bandung');
			// $this->email->message($body_email);

			$kirim = $mail->send();
			// var_dump($kirim);
			// echo $this->email->print_debugger();
			// die();
			
			$tel = $cek['tel'];
			$message = 'Kode unik anda adalah : *'.$u_reg.'*';
			$phone = '62'.substr($tel, 1);

			$wa = json_encode(
				array(
					'chatId'=>$phone.'@c.us',
					'body'=>$message
				)
			);
			$url = apiURL.'message?token='.token;
			$options = stream_context_create(
				array('http' =>
					array(
						'method'  => 'POST',
						'header'  => 'Content-type: application/json',
						'content' => $wa
					)
				)
			);
			
			
			if(!empty($cek['email'])){
				if($kirim){
					$this->db->query("UPDATE users SET u_reg = '$u_reg' WHERE email = '$email'");
					file_get_contents($url,false,$options);
					echo '<script type="text/javascript">alert("Silahkan cek email anda untuk melihat kode unik"); window.location = "'.base_url('form_lupakatasandi').'";</script>';
				} else {
					$output = $this->email->print_debugger();
					echo '<script type="text/javascript">alert("Periksa koneksi internet anda"); window.location = "'.base_url('lupa-kata-sandi').'";</script>';
				}
			} else {
				echo '<script type="text/javascript">alert("Email yang anda masukan tidak terdaftar di sistem kami"); window.location = "'.base_url('lupa-kata-sandi').'";</script>';
			}
		}
	}



	// public function generateRandomString($length = 5) {
	// 	return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
	// }
	
	// echo  generateRandomString();

	public function register_process() {

		$this->form_validation->set_rules('fullname', 'Nama Lengkap', 'trim|required');
		$this->form_validation->set_rules('gender', 'Jenis Kelamin', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required');
		$this->form_validation->set_rules('password', 'Kata Sandi', 'trim|required');
		$this->form_validation->set_rules('password2', 'Konfirmasi Kata Sandi', 'trim|required');
		$this->form_validation->set_rules('address', 'Alamat', 'trim|required');
		$this->form_validation->set_rules('job', 'Pekerjaan', 'trim|required');
		$this->form_validation->set_rules('age', 'Umur', 'trim|required');
		$this->form_validation->set_rules('tel', 'No. Telp / HP', 'trim|required');

		if($this->form_validation->run() == TRUE) {
			if($this->input->post('password') == $this->input->post('password2')) {
	
				$data = array(
					'users_id' => '',
					'fullname' => $this->input->post('fullname'),
					'email' => $this->input->post('email'),
					'password' => md5($this->input->post('password')),
					'gender' => $this->input->post('gender'),
					'address' => $this->input->post('address'),
					'job' => $this->input->post('job'),
					'age' => $this->input->post('age'),
					'tel' => $this->input->post('tel'),
					'reg_time' => date('Y-m-d H:i:s'),
					'up_time' => '',
					'level' => 'User',
					'u_reg' => $this->input->post('u_reg'),
					'isActive' => 'False'
				);

				$config = [
					'mailtype'  => 'html',
					'charset'   => 'utf-8',
					'protocol'  => 'smtp',
					'smtp_host' => 'smtp.googlemail.com',
					'smtp_user' => 'stormxzero1@gmail.com',  // Email gmail
					'smtp_pass'   => 'minuman123',  // Password gmail
					'smtp_crypto' => 'ssl',
					'smtp_port'   => 465,
					'crlf'    => "\r\n",
					'newline' => "\r\n"
				];
	
				// $response = false;
				// $mail = new PHPMailer(true);
				// $mail->isSMTP();
				// $mail->Mailer = "smtp";
				// $mail->Host     = 'mail.greetings.id'; //sesuaikan sesuai nama domain hosting/server yang digunakan
				// $mail->SMTPAuth = true;
				// $mail->Username = 'rizkyabdul@greetings.id'; // user email
				// $mail->Password = 'rizkyghani16021998'; // password email
				// $mail->Priority = 1;
				// // $mail->SMTPAutoTLS = false; 
				// $mail->Port     = 587;
	
				$body_email = '<!doctype html>
				<html>
				  <head>
					<meta name="viewport" content="width=device-width" />
					<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
					<title>Notifikasi Sistem Surat Penjamin Rutan Kelas 1 Bandung</title>
					<style>
					  /* -------------------------------------
						  GLOBAL RESETS
					  ------------------------------------- */
					  
					  /*All the styling goes here*/
					  
					  img {
						border: none;
						-ms-interpolation-mode: bicubic;
						max-width: 100%; 
					  }
				
					  body {
						background-color: #f6f6f6;
						font-family: sans-serif;
						-webkit-font-smoothing: antialiased;
						font-size: 14px;
						line-height: 1.4;
						margin: 0;
						padding: 0;
						-ms-text-size-adjust: 100%;
						-webkit-text-size-adjust: 100%; 
					  }
				
					  table {
						border-collapse: separate;
						mso-table-lspace: 0pt;
						mso-table-rspace: 0pt;
						width: 100%; }
						table td {
						  font-family: sans-serif;
						  font-size: 14px;
						  vertical-align: top; 
					  }
				
					  /* -------------------------------------
						  BODY & CONTAINER
					  ------------------------------------- */
				
					  .body {
						background-color: #f6f6f6;
						width: 100%; 
					  }
				
					  /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
					  .container {
						display: block;
						margin: 0 auto !important;
						/* makes it centered */
						max-width: 580px;
						padding: 10px;
						width: 580px; 
					  }
				
					  /* This should also be a block element, so that it will fill 100% of the .container */
					  .content {
						box-sizing: border-box;
						display: block;
						margin: 0 auto;
						max-width: 580px;
						padding: 10px; 
					  }
				
					  /* -------------------------------------
						  HEADER, FOOTER, MAIN
					  ------------------------------------- */
					  .main {
						background: #ffffff;
						border-radius: 3px;
						width: 100%; 
					  }
				
					  .wrapper {
						box-sizing: border-box;
						padding: 20px; 
					  }
				
					  .content-block {
						padding-bottom: 10px;
						padding-top: 10px;
					  }
				
					  .footer {
						clear: both;
						margin-top: 10px;
						text-align: center;
						width: 100%; 
					  }
						.footer td,
						.footer p,
						.footer span,
						.footer a {
						  color: #999999;
						  font-size: 12px;
						  text-align: center; 
					  }
				
					  /* -------------------------------------
						  TYPOGRAPHY
					  ------------------------------------- */
					  h1,
					  h2,
					  h3,
					  h4 {
						color: #000000;
						font-family: sans-serif;
						font-weight: 400;
						line-height: 1.4;
						margin: 0;
						margin-bottom: 30px; 
					  }
				
					  h1 {
						font-size: 35px;
						font-weight: 300;
						text-align: center;
						text-transform: capitalize; 
					  }
				
					  p,
					  ul,
					  ol {
						font-family: sans-serif;
						font-size: 14px;
						font-weight: normal;
						margin: 0;
						margin-bottom: 15px; 
					  }
						p li,
						ul li,
						ol li {
						  list-style-position: inside;
						  margin-left: 5px; 
					  }
				
					  a {
						color: #3498db;
						text-decoration: underline; 
					  }
				
					  /* -------------------------------------
						  BUTTONS
					  ------------------------------------- */
					  .btn {
						box-sizing: border-box;
						width: 100%; }
						.btn > tbody > tr > td {
						  padding-bottom: 15px; }
						.btn table {
						  width: auto; 
					  }
						.btn table td {
						  background-color: #ffffff;
						  border-radius: 5px;
						  text-align: center; 
					  }
						.btn a {
						  background-color: #ffffff;
						  border: solid 1px #3498db;
						  border-radius: 5px;
						  box-sizing: border-box;
						  color: #3498db;
						  cursor: pointer;
						  display: inline-block;
						  font-size: 14px;
						  font-weight: bold;
						  margin: 0;
						  padding: 12px 25px;
						  text-decoration: none;
						  text-transform: capitalize; 
					  }
				
					  .btn-primary table td {
						background-color: #3498db; 
					  }
				
					  .btn-primary a {
						background-color: #3498db;
						border-color: #3498db;
						color: #ffffff; 
					  }
				
					  /* -------------------------------------
						  OTHER STYLES THAT MIGHT BE USEFUL
					  ------------------------------------- */
					  .last {
						margin-bottom: 0; 
					  }
				
					  .first {
						margin-top: 0; 
					  }
				
					  .align-center {
						text-align: center; 
					  }
				
					  .align-right {
						text-align: right; 
					  }
				
					  .align-left {
						text-align: left; 
					  }
				
					  .clear {
						clear: both; 
					  }
				
					  .mt0 {
						margin-top: 0; 
					  }
				
					  .mb0 {
						margin-bottom: 0; 
					  }
				
					  .preheader {
						color: transparent;
						display: none;
						height: 0;
						max-height: 0;
						max-width: 0;
						opacity: 0;
						overflow: hidden;
						mso-hide: all;
						visibility: hidden;
						width: 0; 
					  }
				
					  .powered-by a {
						text-decoration: none; 
					  }
				
					  hr {
						border: 0;
						border-bottom: 1px solid #f6f6f6;
						margin: 20px 0; 
					  }
				
					  /* -------------------------------------
						  RESPONSIVE AND MOBILE FRIENDLY STYLES
					  ------------------------------------- */
					  @media only screen and (max-width: 620px) {
						table[class=body] h1 {
						  font-size: 28px !important;
						  margin-bottom: 10px !important; 
						}
						table[class=body] p,
						table[class=body] ul,
						table[class=body] ol,
						table[class=body] td,
						table[class=body] span,
						table[class=body] a {
						  font-size: 16px !important; 
						}
						table[class=body] .wrapper,
						table[class=body] .article {
						  padding: 10px !important; 
						}
						table[class=body] .content {
						  padding: 0 !important; 
						}
						table[class=body] .container {
						  padding: 0 !important;
						  width: 100% !important; 
						}
						table[class=body] .main {
						  border-left-width: 0 !important;
						  border-radius: 0 !important;
						  border-right-width: 0 !important; 
						}
						table[class=body] .btn table {
						  width: 100% !important; 
						}
						table[class=body] .btn a {
						  width: 100% !important; 
						}
						table[class=body] .img-responsive {
						  height: auto !important;
						  max-width: 100% !important;
						  width: auto !important; 
						}
					  }
				
					  /* -------------------------------------
						  PRESERVE THESE STYLES IN THE HEAD
					  ------------------------------------- */
					  @media all {
						.ExternalClass {
						  width: 100%; 
						}
						.ExternalClass,
						.ExternalClass p,
						.ExternalClass span,
						.ExternalClass font,
						.ExternalClass td,
						.ExternalClass div {
						  line-height: 100%; 
						}
						.apple-link a {
						  color: inherit !important;
						  font-family: inherit !important;
						  font-size: inherit !important;
						  font-weight: inherit !important;
						  line-height: inherit !important;
						  text-decoration: none !important; 
						}
						#MessageViewBody a {
						  color: inherit;
						  text-decoration: none;
						  font-size: inherit;
						  font-family: inherit;
						  font-weight: inherit;
						  line-height: inherit;
						}
						.btn-primary table td:hover {
						  background-color: #34495e !important; 
						}
						.btn-primary a:hover {
						  background-color: #34495e !important;
						  border-color: #34495e !important; 
						} 
					  }
				
					</style>
				  </head>
				  <body class="">
					<span class="preheader">This is preheader text. Some clients will show this text as a preview.</span>
					<table role="presentation" border="0" cellpadding="0" cellspacing="0" class="body">
					  <tr>
						<td>&nbsp;</td>
						<td class="container">
						  <div class="content">
				
							<!-- START CENTERED WHITE CONTAINER -->
							<table role="presentation" class="main">
				
							  <!-- START MAIN CONTENT AREA -->
							  <tr>
								<td class="wrapper">
								  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
									<tr>
									  <td>
										<p>Terimakasih telah registrasi di sistem kami,</p>
										<p>Kode aktifasi anda adalah :</p><br>
										<h4>'.$this->input->post('u_reg').'</h4><br>
										<table role="presentation" border="0" cellpadding="0" cellspacing="0" align="center">
										  <tbody>
											<tr>
											  <td align="center">
												<table role="presentation" border="0" cellpadding="0" cellspacing="0" align="center">
												  <tbody>
													<tr>
													  <td></td>
													</tr>
												  </tbody>
												</table>
											  </td>
											</tr>
										  </tbody>
										</table>
									  </td>
									</tr>
								  </table>
								</td>
							  </tr>
				
							<!-- END MAIN CONTENT AREA -->
							</table>
							<!-- END CENTERED WHITE CONTAINER -->
				
							<!-- START FOOTER -->
							<div class="footer">
							  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
								<tr>
								  <td class="content-block">
									<span class="apple-link">Rutan Kelas I Bandung '.date("Y").'</span>
								  </td>
								</tr>
								<tr>
								  <td class="content-block powered-by">
									Powered by <a href="https://greetings.id">Greetings Indonesia</a>
								  </td>
								</tr>
							  </table>
							</div>
							<!-- END FOOTER -->
				
						  </div>
						</td>
						<td>&nbsp;</td>
					  </tr>
					</table>
				  </body>
				</html>';
	
				$this->load->library('email');
				$this->email->initialize($config);
				// $mail->setFrom('no-reply@rutankelas1bdg.com', 'no-reply@rutankelas1bdg.com');
				// $mail->addReplyTo('no-reply@rutankelas1bdg.com', 'no-reply@rutankelas1bdg.com'); //user email
				// $mail->addAddress($this->input->post('email'));
				// // $this->email->attach('#');
				// $mail->Subject = 'Kode aktifasi akun Surat Penjamin Rutan Kelas 1 Bandung';
				// $mail->isHTML(true);
				// $mail->Body = $body_email;
	
				$this->email->from('no-reply@rutankelas1bdg.com', 'no-reply@rutankelas1bdg.com');
				$this->email->to($this->input->post('email'));
				$this->email->subject('Kode aktifasi akun Surat Penjamin Rutan Kelas 1 Bandung');
				$this->email->message($body_email);
				$email = $this->input->post('email');
				// var_dump($mail);
				// die();
				$kirim = $this->email->send();
				$tel = $this->input->post('tel');
				$message = 'Terimakasih telah registrasi pada sistem kami. Kode unik anda adalah : *'.$this->input->post('u_reg').'*';
				$phone = '62'.substr($tel, 1);

				$wa = json_encode(
					array(
						'chatId'=>$phone.'@c.us',
						'body'=>$message
					)
				);
				$url = apiURL.'message?token='.token;
				$options = stream_context_create(
					array('http' =>
						array(
							'method'  => 'POST',
							'header'  => 'Content-type: application/json',
							'content' => $wa
						)
					)
				);
				
				if ($kirim){
					$result = $this->LoginModel->register($data);
					if($result == TRUE) {
						file_get_contents($url,false,$options);
						$output = array(
							'status' => true,
							'message' => 'Registrasi Akun Berhasil! Silahkan periksa email anda (termasuk folder spam) dan masukan kode aktifasi disini.'
						);
					}else{
						$output = array(
							'status' => false,
							'message' => 'Registrasi Akun Gagal'
						);
					}
				} else {
					$output = array(
						'status' => false,
						'message' => 'Periksa kembali email anda'
					);
					echo "<script>console.log('Email: " . $email . "' );</script>";
				}
				
			}else{
				$output = array(
					'status' => false,
					'message' => 'Password Tidak Sama'
				);
			}
		}else{
			$output = array(
				'status' => false,
				'message' => 'Semua Wajib Dilengkapi'
			);
		}
		echo json_encode($output);
	}

	function logout(){
		$this->session->sess_destroy();
		redirect(base_url('login'));
	}
}
