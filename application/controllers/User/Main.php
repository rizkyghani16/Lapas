<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Main extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->library('Pdf');

        if ($this->session->userdata('level') == 'User') {
        } else {
            redirect(base_url('logout'));
        }
        $this->load->library('parser');
        $this->load->helper('directory');
    }

    public function index()
    {
        date_default_timezone_set('Asia/Jakarta');
        $users_id = $this->session->userdata('users_id');
        $data['nara'] = $this->db->query("SELECT * FROM narapidana WHERE users_id = '$users_id'")->row_array();
        $data['file'] = $this->db->query("SELECT * FROM file WHERE users_id = '$users_id'")->row_array();
        $data['resi'] = $this->db->query("SELECT * FROM resi WHERE users_id = '$users_id'")->row_array();
        $data['log'] = $this->db->query("SELECT * FROM logSurat WHERE users_id = '$users_id'")->row_array();
        $data['revisi'] = $this->db->query("SELECT * FROM revisi WHERE users_id = '$users_id' ORDER BY revisi_id DESC LIMIT 1")->row_array();
        $data['logs'] = $this->db->query("SELECT * FROM logSurat WHERE users_id = '$users_id'")->result();
        $data['permohonan'] = $this->db->query('SELECT * FROM permohonan')->result();
        $data['view'] = 'user/dashboard';
        $this->load->view('user/layout', $data);
    }

    public function profile()
    {
        date_default_timezone_set('Asia/Jakarta');
        $users_id = $this->session->userdata('users_id');
        $data['user'] = $this->db->query("SELECT * FROM users WHERE users_id = '$users_id'")->row_array();
        $data['file'] = $this->db->query("SELECT * FROM file WHERE users_id = '$users_id'")->row_array();
        $data['view'] = 'user/profile';
        $this->load->view('user/layout', $data);
    }

    public function add_nara()
    {
        $this->form_validation->set_rules('nama', 'Nama Narapidana', 'required|trim');
        $this->form_validation->set_rules('umur', 'Umur Narapidana', 'required|trim');
        $this->form_validation->set_rules('hubungan', 'Hubungan', 'required|trim');
        $this->form_validation->set_rules('jenis', 'Jenis Permohonan', 'required|trim');

        if ($this->form_validation->run() != false) {
            $nama = $this->input->post('nama');
            $umur = $this->input->post('umur');
            $hubungan = $this->input->post('hubungan');
            $jenis = $this->input->post('jenis');

            $data = [
                    'nara_id' => '',
                    'users_id' => $this->session->userdata('users_id'),
                    'nama' => $nama,
                    'umur' => $umur,
                    'hubungan' => $hubungan,
                    'jenis' => $jenis
                ];

            $this->user_model->add($data, 'narapidana');
            $this->session->set_flashdata('message', '<div class="alert alert-success" id="success-alert" role="alert">Data narapidana berhasil disimpan<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
            redirect(base_url('dashboard'));
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" id="danger-alert" role="alert">Data narapidana gagal disimpan<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
            redirect(base_url('dashboard'));
        }
    }

    public function add_resi()
    {
        $this->form_validation->set_rules('kode_resi', 'Kode Resi', 'required|trim');

        if ($this->form_validation->run() != false) {
            $kode_resi = $this->input->post('kode_resi');
            date_default_timezone_set('Asia/Jakarta');

            $data = [
                    'resi_id' => '',
                    'users_id' => $this->session->userdata('users_id'),
                    'kode_resi' => $kode_resi
                ];
            
            $log = [
                'logSurat_id' => '',
                'users_id' => $this->session->userdata('users_id'),
                'log_title' => 'Kode resi dokumen telah diterima',
                'log_desc' => 'Terimakasih telah mengirimkan dokumen untuk arsip Rutan Kelas 1 Bandung',
                'log_time' => date('Y-m-d H:i:s'),
            ];

            $this->user_model->add($data, 'resi');
            $this->user_model->log($log, 'logSurat');
            $this->session->set_flashdata('message', '<div class="alert alert-success" id="success-alert" role="alert">Kode resi dokumen berhasil disimpan<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
            redirect(base_url('dashboard'));
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" id="danger-alert" role="alert">Kode resi dokumen gagal disimpan<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
            redirect(base_url('dashboard'));
        }
    }

    public function update_pass()
    {
        $this->form_validation->set_rules('old_password', 'Password Lama', 'required|trim');
        $this->form_validation->set_rules('new_password', 'Password Baru', 'required|trim');
        $this->form_validation->set_rules('new_password2', 'Password Baru', 'required|trim');

        if ($this->form_validation->run() != false) {
            $old_password = $this->input->post('old_password');
            $new_password = $this->input->post('new_password');
            $new_password2 = $this->input->post('new_password2');
            $hash = md5($new_password);
            $hash2 = md5($old_password);
            $users_id = $this->session->userdata('users_id');
            $check = $this->db->query("SELECT password FROM users WHERE users_id = '$users_id'")->row_array();
            date_default_timezone_set('Asia/Jakarta');
            $date = date('Y-m-d H:i:s');

            if($check['password'] !== $hash2){
                $this->session->set_flashdata('message', '<div class="alert alert-warning" id="warning-alert" role="alert">Password lama anda salah<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
                redirect(base_url('profile'));
            }
            else if($check['password'] == $hash){
                $this->session->set_flashdata('message', '<div class="alert alert-warning" id="warning-alert" role="alert">Password baru tidak boleh sama dengan password lama<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
                redirect(base_url('profile'));
            } else{
                $this->db->query("UPDATE users SET password = '$hash', up_time = '$date' WHERE users_id = '$users_id'");
                $this->session->set_flashdata('message', '<div class="alert alert-success" id="success-alert" role="alert">Password anda telah diperbaharui<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
                redirect(base_url('profile'));
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" id="danger-alert" role="alert">Gagal memperbaharui password<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
            redirect(base_url('profile'));
        }
    }

    public function add_file()
    {
        $this->form_validation->set_rules('surat', 'Surat Penjamin', 'trim|xss_clean');
        $this->form_validation->set_rules('kk', 'Kartu Keluarga', 'trim|xss_clean');
        $this->form_validation->set_rules('foto', 'Foto Penjamin', 'trim|xss_clean');
        $this->form_validation->set_rules('ktp', 'KTP Penjamin', 'trim|xss_clean');

        if ($this->form_validation->run() != false) {
            $this->load->library('ciqrcode');
            $config = [];
            $config['upload_path'] = './uploads/surat/';
            $config['allowed_types'] = 'jpeg|jpg|png|pdf';
            $config['max_size'] = 2048;
            $config['encrypt_name'] = true;
            $this->load->library('upload', $config, 'suratUpload');
            $this->suratUpload->initialize($config);
            $surats = $this->suratUpload->do_upload('surat');

            $config2 = [];
            $config2['upload_path'] = './uploads/kk/';
            $config2['allowed_types'] = 'jpeg|jpg|png|pdf';
            $config2['max_size'] = 2048;
            $config2['encrypt_name'] = true;
            $this->load->library('upload', $config2, 'kkUpload');
            $this->kkUpload->initialize($config2);
            $kks = $this->kkUpload->do_upload('kk');

            $config3 = [];
            $config3['upload_path'] = './uploads/foto/';
            $config3['allowed_types'] = 'jpeg|jpg|png|pdf';
            $config3['max_size'] = 2048;
            $config3['encrypt_name'] = true;
            $this->load->library('upload', $config3, 'fotoUpload');
            $this->fotoUpload->initialize($config3);
            $fotos = $this->fotoUpload->do_upload('foto');

            $config4 = [];
            $config4['upload_path'] = './uploads/ktp/';
            $config4['allowed_types'] = 'jpeg|jpg|png|pdf';
            $config4['max_size'] = 2048;
            $config4['encrypt_name'] = true;
            $this->load->library('upload', $config4, 'ktpUpload');
            $this->ktpUpload->initialize($config4);
            $ktps = $this->ktpUpload->do_upload('ktp');

            if ($surats && $kks && $ktps && $fotos) {
                $result1 = $this->suratUpload->data();
                $result2 = $this->kkUpload->data();
                $result3 = $this->fotoUpload->data();
                $result4 = $this->ktpUpload->data();

                $result = ['surat' => $result1, 'kk' => $result2, 'foto' => $result3, 'ktp' => $result4];
                $getcode = $this->user_model->get_kode();
                date_default_timezone_set('Asia/Jakarta');

                $config['cacheable']    = true; 
                $config['cachedir']     = './uploads/';
                $config['errorlog']     = './uploads/'; 
                $config['imagedir']     = './uploads/qrcode/'; 
                $config['quality']      = true; 
                $config['size']         = '15'; 
                $config['black']        = array(224,255,255);
                $config['white']        = array(70,130,180);
                $this->ciqrcode->initialize($config);
                        
                $image_name=md5('FILE'.$getcode).'.png';

                $params['data'] = md5('FILE'.$getcode); 
                $params['level'] = 'H'; 
                $params['size'] = 15;
                $params['savename'] = FCPATH.$config['imagedir'].$image_name; 
                $this->ciqrcode->generate($params);

                $data = [
                    'file_id' => 'FILE'.$getcode,
                    'users_id' => $this->session->userdata('users_id'),
                    'surat' => $result['surat']['file_name'],
                    'kk' => $result['kk']['file_name'],
                    'ktp' => $result['ktp']['file_name'],
                    'foto' => $result['foto']['file_name'],
                    'qrcode' => $image_name,
                    'status' => 'Pending',
                    'review' => 'False'
                ];

                $log = [
                    'logSurat_id' => '',
                    'users_id' => $this->session->userdata('users_id'),
                    'log_title' => 'Berkas telah diterima oleh sistem',
                    'log_desc' => 'Menunggu konfirmasi dari petugas',
                    'log_time' => date('Y-m-d H:i:s'),
                ];

                $this->user_model->add($data, 'file');
                $this->user_model->log($log, 'logSurat');
                $this->session->set_flashdata('message', '<div class="alert alert-success" id="success-alert" role="alert">Berkas berhasil disimpan<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
                redirect(base_url('dashboard'));
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" id="danger-alert" role="alert">Semua berkas harus diisi!<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
                redirect(base_url('dashboard'));
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" id="danger-alert" role="alert">Berkas gagal disimpan<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
            redirect(base_url('dashboard'));
        }
    }

    public function edit_file()
    {
        $this->form_validation->set_rules('surat', 'Surat Penjamin', 'trim|xss_clean');
        $this->form_validation->set_rules('kk', 'Kartu Keluarga', 'trim|xss_clean');
        $this->form_validation->set_rules('foto', 'Foto Penjamin', 'trim|xss_clean');

        if ($this->form_validation->run() != false) {
            $config = [];
            $config['upload_path'] = './uploads/surat/';
            $config['allowed_types'] = 'jpeg|jpg|png|pdf';
            $config['max_size'] = 2048;
            $config['encrypt_name'] = true;
            $this->load->library('upload', $config, 'suratUpload');
            $this->suratUpload->initialize($config);
            $surats = $this->suratUpload->do_upload('surat');

            $config2 = [];
            $config2['upload_path'] = './uploads/kk/';
            $config2['allowed_types'] = 'jpeg|jpg|png|pdf';
            $config2['max_size'] = 2048;
            $config2['encrypt_name'] = true;
            $this->load->library('upload', $config2, 'kkUpload');
            $this->kkUpload->initialize($config2);
            $kks = $this->kkUpload->do_upload('kk');

            $config3 = [];
            $config3['upload_path'] = './uploads/foto/';
            $config3['allowed_types'] = 'jpeg|jpg|png|pdf';
            $config3['max_size'] = 2048;
            $config3['encrypt_name'] = true;
            $this->load->library('upload', $config3, 'fotoUpload');
            $this->fotoUpload->initialize($config3);
            $fotos = $this->fotoUpload->do_upload('foto');

            $config4 = [];
            $config4['upload_path'] = './uploads/ktp/';
            $config4['allowed_types'] = 'jpeg|jpg|png|pdf';
            $config4['max_size'] = 2048;
            $config4['encrypt_name'] = true;
            $this->load->library('upload', $config4, 'ktpUpload');
            $this->ktpUpload->initialize($config4);
            $ktps = $this->ktpUpload->do_upload('ktp');

            if ($surats && $kks && $ktps && $fotos) {
                $result1 = $this->suratUpload->data();
                $result2 = $this->kkUpload->data();
                $result3 = $this->fotoUpload->data();
                $result4 = $this->ktpUpload->data();

                $result = ['surat' => $result1, 'kk' => $result2, 'foto' => $result3, 'ktp' => $result4];
                date_default_timezone_set('Asia/Jakarta');

                $users_id = $this->session->userdata('users_id');
                $surat = $result['surat']['file_name'];
                $kk = $result['kk']['file_name'];
                $ktp = $result['ktp']['file_name'];
                $foto = $result['foto']['file_name'];

                $log = [
                    'logSurat_id' => '',
                    'users_id' => $this->session->userdata('users_id'),
                    'log_title' => 'Berkas telah diperbarui',
                    'log_desc' => 'Menunggu konfirmasi dari petugas',
                    'log_time' => date('Y-m-d H:i:s'),
                ];

                $this->db->query("UPDATE file SET surat = '$surat', kk = '$kk', ktp = '$ktp', foto = '$foto', review = 'False' WHERE users_id = '$users_id'");
                $this->user_model->log($log, 'logSurat');
                $this->session->set_flashdata('message', '<div class="alert alert-success" id="success-alert" role="alert">Berkas berhasil disimpan<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
                redirect(base_url('dashboard'));
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" id="danger-alert" role="alert">Semua berkas harus diisi!<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
                redirect(base_url('dashboard'));
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" id="danger-alert" role="alert">Berkas gagal disimpan<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
            redirect(base_url('dashboard'));
        }
    }

    public function cetakpdf()
    {
        date_default_timezone_set('Asia/Jakarta');
        $users_id = $this->session->userdata('users_id');
        $nara = $this->db->query("SELECT * FROM narapidana WHERE users_id = '$users_id'")->row_array();
        $user = $this->db->query("SELECT * FROM users WHERE users_id = '$users_id'")->row_array();
        $nama = $nara['nama'];
        $umur = $nara['umur'].' TAHUN';
        $hubungan = $nara['hubungan'];
        $jenis = $nara['jenis'];
        $tgl = date('d/m/Y');
        $fullname = $user['fullname'];
        $age = $user['age'].' TAHUN';
        $job = $user['job'];
        $tel = $user['tel'];
        $address = $user['address'];
        $pdf = new FPDF('p', 'mm', 'A4');

        $pdf->AddPage('portrait');
        $pdf->SetFont('Times', 'B', 16);

        // $pdf->Cell(71, 10, '', 0, 0);
        // $pdf->Cell(59, 5, 'Invoice', 0, 0);
        // $pdf->Cell(59, 10, '', 0, 1);

        //KOP SURAT
        $pdf->Cell(180, 7, 'SURAT JAMINAN KESANGGUPAN KELUARGA', 0, 1, 'C');
        $pdf->SetFont('Times', '', 10);
        $pdf->Cell(10, 7, '', 0, 0);

        $pdf->Cell(155, 7, 'No.                       / 2021', 0, 1, 'C');
        $pdf->Cell(10, 7, '', 0, 1);

        //ISI
        $pdf->SetFont('Times', '', 12);
        $pdf->Cell(10, 7, 'Yang bertanda tangan dibawah ini, kami :', 0, 1);
        $pdf->Cell(10, 1, '', 0, 1);

        // $pdf->cell(10, 7, 'Nama                                         : '.strtoupper($fullname), 0, 1);

        // $pdf->Cell(10, 7, 'Umur                                          : '.$age, 0, 1);
        // $pdf->Cell(10, 7, 'Pekerjaan / Jabatan                    : '.strtoupper($job), 0, 1);
        // $pdf->Cell(10, 7, 'Hubungan dgn Narapidana        : '.strtoupper($hubungan), 0, 1);
        // $pdf->Cell(10, 7, 'No. Telp / HP                             : '.$tel, 0, 1);
        // $pdf->Cell(10, 7, 'Alamat                                        : '.strtoupper($address), 0, 1);

        $pdf->Cell(10, 5, 'Nama ', 0, 0);
        $pdf->Cell(55, 5, '', 0, 0);  //spasi ke kanan
        $pdf->Cell(45, 5, ':    '.strtoupper($fullname), 0, 1);

        $pdf->Cell(10, 1, '', 0, 1);
        //spasi kosong

        $pdf->Cell(10, 5, 'Umur ', 0, 0);
        $pdf->Cell(55, 5, '', 0, 0);
        $pdf->Cell(45, 5, ':    '.$age, 0, 1);

        $pdf->Cell(10, 1, '', 0, 1);

        $pdf->Cell(10, 5, 'Pekerjaan / Jabatan ', 0, 0);
        $pdf->Cell(55, 5, '', 0, 0);
        $pdf->Cell(45, 5, ':    '.strtoupper($job), 0, 1);

        $pdf->Cell(10, 1, '', 0, 1);

        $pdf->Cell(10, 5, 'Hubungan dgn Narapidana ', 0, 0);
        $pdf->Cell(55, 5, '', 0, 0);
        $pdf->Cell(45, 5, ':    '.strtoupper($hubungan), 0, 1);

        $pdf->Cell(10, 1, '', 0, 1);

        $pdf->Cell(10, 5, 'No. Telp / HP   ', 0, 0);
        $pdf->Cell(55, 5, '', 0, 0);
        $pdf->Cell(45, 5, ':    '.$tel, 0, 1);

        $pdf->Cell(10, 1, '', 0, 1);

        $pdf->Cell(10, 5, 'Alamat ', 0, 0);
        $pdf->Cell(55, 5, '', 0, 0);
        $pdf->Cell(45, 5, ':    '.strtoupper($address), 0, 1);

        $pdf->Cell(10, 7, '', 0, 1);

        $pdf->Cell(10, 7, 'Adalah sebagai penjamin dari Narapidana:', 0, 1);
        $pdf->SetFont('Times', '', 12);
        // $pdf->Cell(10, 7, 'Nama                                         : '.strtoupper($nama), 0, 1);
        // $pdf->Cell(10, 7, 'Umur                                         : '.strtoupper($umur), 0, 1);
        // $pdf->Cell(10, 7, 'Menjalani Pidana di                  : Rutan Kelas I Bandung', 0, 1);

        $pdf->Cell(10, 5, 'Nama ', 0, 0);
        $pdf->Cell(55, 5, '', 0, 0);
        $pdf->Cell(45, 5, ':    '.strtoupper($nama), 0, 1);

        $pdf->Cell(10, 1, '', 0, 1);

        $pdf->Cell(10, 5, 'Umur ', 0, 0);
        $pdf->Cell(55, 5, '', 0, 0);
        $pdf->Cell(45, 5, ':    '.strtoupper($umur), 0, 1);

        $pdf->Cell(10, 7, '', 0, 1);

        $pdf->Cell(10, 7, 'Dengan ini menyatakan:', 0, 1);
        $pdf->Cell(10, 7, '   1. Sanggup menjamin yang bersangkutan tidak meninggalkan wilayah Negara Republik Indonesia.', 0, 1);

        //ADA BOLD DITENGAH
        $pdf->SetFont('Times', '', 12);
        $cell = '   2. Sanggup menjamin yang bersangkutan mentaati peryaratan pelaksanaan';
        $pdf->Cell($pdf->GetStringWidth($cell), 10, $cell, 7, 'L', 1);
        $pdf->SetFont('Times', 'B', 12);
        $boldCell = ' '.$jenis;
        $pdf->Cell($pdf->GetStringWidth($boldCell), 10, $boldCell, 7, 'C', 1);
        $pdf->SetFont('Times', '', 12);
        $pdf->Cell(10, 7, ' ', 0, 1);
        $pdf->Cell(10, 4, ' ', 0, 1);

        // $pdf->Cell(10, 7, '   2. Sanggup menjamin yang bersangkutan mentaati peryaratan pelaksanaan (isi).', 0, 1);

        $pdf->Cell(10, 7, '   3. Sanggup menjamin yang bersangkutan tidak melakukan perbuatan melanggar hukum.', 0, 1);

        //ADA BOLD DITENGAH
        $pdf->SetFont('Times', '', 12);
        $cell = '   4. Sanggup membantu mengawasi narapidana yang menjalani';
        $pdf->Cell($pdf->GetStringWidth($cell), 10, $cell, 7, 'L');
        $pdf->SetFont('Times', 'B', 12);
        $boldCell = ' '.$jenis;
        $pdf->Cell($pdf->GetStringWidth($boldCell), 10, $boldCell, 7, 'C', 1);
        $pdf->SetFont('Times', '', 12);
        $pdf->Cell(10, 7, '', 0, 1);

        // $pdf->Cell(10, 7, '   4. Sanggup membantu mengawasi narapidana yang menjalani (isi).', 0, 1);
        $pdf->Cell(10, 7, '', 0, 1);

        //TTD FOOTER
        $pdf->Cell(10, 7, 'Demikian surat jaminan ini dibuat dengan sesungguhnya untuk dipergunakan seperlunya.', 0, 1);
        $pdf->Cell(10, 7, '', 0, 1);
        $pdf->Cell(180, 7, 'Bandung, '.$tgl, 0, 1, 'C');
        $pdf->Cell(180, 7, 'Penjamin,', 0, 1, 'C');
        $pdf->Cell(10, 7, '', 0, 1);
        $pdf->Cell(10, 7, '', 0, 1);
        $pdf->Cell(10, 7, '', 0, 1);

        $pdf->Cell(180, 7, '.........................', 0, 1, 'C');
        $pdf->Cell(10, 7, '', 0, 1);

        $pdf->SetFont('times', '', 12);
        $pdf->Cell(71, 5, 'Kepala Rutan Kelas I Bandung,', 0, 0);
        $pdf->Cell(45, 5, '', 0, 0);
        $pdf->Cell(45, 5, 'Kepala Lurah / Desa: ..........                ', 0, 1);

        $pdf->Cell(10, 7, '', 0, 1);
        $pdf->Cell(10, 7, '', 0, 1);
        $pdf->Cell(10, 7, '', 0, 1);
        $pdf->Cell(10, 7, '', 0, 1);
        $pdf->Cell(10, 7, '', 0, 1);

        $pdf->Cell(71, 5, '      ..............................', 0, 0);
        $pdf->Cell(50, 5, '', 0, 0);
        $pdf->Cell(50, 5, '  .............................', 0, 1);

        $pdf->Output('', 'Surat Jaminan Kesanggupan');
    }
}
