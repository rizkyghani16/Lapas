-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 13, 2021 at 05:01 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 5.6.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `surat`
--

-- --------------------------------------------------------

--
-- Table structure for table `file`
--

CREATE TABLE `file` (
  `file_id` varchar(50) NOT NULL,
  `users_id` int(55) NOT NULL,
  `surat` text NOT NULL,
  `kk` text NOT NULL,
  `ktp` text NOT NULL,
  `foto` text NOT NULL,
  `qrcode` text NOT NULL,
  `status` varchar(15) NOT NULL,
  `review` enum('False','True') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `file`
--

INSERT INTO `file` (`file_id`, `users_id`, `surat`, `kk`, `ktp`, `foto`, `qrcode`, `status`, `review`) VALUES
('FILE130320210003', 2, '4d27af7a102ddfb73c9c27fa6dbb63c3.pdf', '359096d6f059086bfa58d5aca32f5087.pdf', '7393c01960a08ff5a6d09b7b8b3803fe.png', '27d7cb020ee4d8fb68fc50d130013937.png', 'e23f3464ed0f959b64e048f04325404d.png', 'Pending', 'False'),
('FILE250220210002', 3, '133e4d8c21063185014cc5f2cbe84ea0.pdf', 'c66c0b0c64f4654840f08ce7a5828a74.pdf', 'ac3d8675d8cab833d579ffea8e87814f.png', 'eb4c46d820d3ba3ea1c7edcdbbae77a5.png', '', 'Revision', 'False');

-- --------------------------------------------------------

--
-- Table structure for table `logSurat`
--

CREATE TABLE `logSurat` (
  `logSurat_id` int(100) NOT NULL,
  `users_id` int(55) NOT NULL,
  `log_title` text NOT NULL,
  `log_desc` text NOT NULL,
  `log_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logSurat`
--

INSERT INTO `logSurat` (`logSurat_id`, `users_id`, `log_title`, `log_desc`, `log_time`) VALUES
(13, 3, 'Berkas telah diterima oleh sistem', 'Menunggu konfirmasi dari petugas', '2021-02-25 11:38:49'),
(14, 3, 'Berkas telah diperbarui', 'Menunggu konfirmasi dari petugas', '2021-02-25 11:41:48'),
(17, 2, 'Berkas telah diterima oleh sistem', 'Menunggu konfirmasi dari petugas', '2021-03-13 00:44:40');

-- --------------------------------------------------------

--
-- Table structure for table `narapidana`
--

CREATE TABLE `narapidana` (
  `nara_id` int(55) NOT NULL,
  `users_id` int(55) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `umur` int(5) NOT NULL,
  `hubungan` varchar(50) NOT NULL,
  `jenis` varchar(35) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `narapidana`
--

INSERT INTO `narapidana` (`nara_id`, `users_id`, `nama`, `umur`, `hubungan`, `jenis`) VALUES
(1, 5, 'Briantama Rafliansyah', 23, 'Keluarga', 'Pembebasan Bersyarat'),
(2, 2, 'TRICHA DWINASTY', 23, 'Keluarga', 'Asimilasi'),
(3, 3, 'Rizky Abdul', 23, 'Keluarga', 'Justice Collaborator');

-- --------------------------------------------------------

--
-- Table structure for table `permohonan`
--

CREATE TABLE `permohonan` (
  `permo_id` int(50) NOT NULL,
  `nama_permohonan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permohonan`
--

INSERT INTO `permohonan` (`permo_id`, `nama_permohonan`) VALUES
(2, 'Pembebasan Bersyarat'),
(3, 'Cuti Menjelang Bebas'),
(4, 'Cuti Bersyarat'),
(5, 'Justice Collaborator'),
(7, 'Asimilasi');

-- --------------------------------------------------------

--
-- Table structure for table `resi`
--

CREATE TABLE `resi` (
  `resi_id` int(55) NOT NULL,
  `users_id` int(55) NOT NULL,
  `kode_resi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `revisi`
--

CREATE TABLE `revisi` (
  `revisi_id` int(50) NOT NULL,
  `users_id` int(55) NOT NULL,
  `revisi_desc` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `revisi`
--

INSERT INTO `revisi` (`revisi_id`, `users_id`, `revisi_desc`) VALUES
(1, 3, 'Silahkan unggah kembali dokumen surat penjamin yang sudah di tanda tangani');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `users_id` int(55) NOT NULL,
  `fullname` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(45) NOT NULL,
  `gender` enum('Laki - Laki','Perempuan') NOT NULL,
  `address` text NOT NULL,
  `job` text NOT NULL,
  `age` int(5) NOT NULL,
  `tel` varchar(20) DEFAULT NULL,
  `reg_time` datetime NOT NULL,
  `up_time` datetime DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `level` enum('Admin','User') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`users_id`, `fullname`, `email`, `password`, `gender`, `address`, `job`, `age`, `tel`, `reg_time`, `up_time`, `last_login`, `level`) VALUES
(2, 'Cobain Aja', 'rizkyghani16@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'Laki - Laki', 'test aja', 'Wiraswasta', 35, '088328294', '2021-02-12 07:43:08', '2021-02-26 17:00:28', '2021-03-12 23:42:32', 'User'),
(3, 'TRICHA DWINASTY', 'trdwinasty@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'Laki - Laki', 'Komplek Padasuka HU 94 Cimahi Tengah', 'WIRASWASTA', 24, '0896783748', '2021-02-25 05:35:18', '2021-03-12 19:17:02', '2021-02-28 16:31:06', 'User'),
(4, 'Admin', 'admin@admin.com', 'e10adc3949ba59abbe56e057f20f883e', 'Laki - Laki', 'Bandung', 'Admin', 23, '089789234', '2021-02-27 13:27:40', '0000-00-00 00:00:00', '2021-03-13 23:00:43', 'Admin'),
(5, 'Mohammad Rinaldy', 'rinaldy@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'Laki - Laki', 'Cimahi', 'Wiraswasta', 24, '0896783748', '2021-03-05 13:39:41', '0000-00-00 00:00:00', NULL, 'User');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `file`
--
ALTER TABLE `file`
  ADD PRIMARY KEY (`file_id`);

--
-- Indexes for table `logSurat`
--
ALTER TABLE `logSurat`
  ADD PRIMARY KEY (`logSurat_id`);

--
-- Indexes for table `narapidana`
--
ALTER TABLE `narapidana`
  ADD PRIMARY KEY (`nara_id`);

--
-- Indexes for table `permohonan`
--
ALTER TABLE `permohonan`
  ADD PRIMARY KEY (`permo_id`);

--
-- Indexes for table `resi`
--
ALTER TABLE `resi`
  ADD PRIMARY KEY (`resi_id`);

--
-- Indexes for table `revisi`
--
ALTER TABLE `revisi`
  ADD PRIMARY KEY (`revisi_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`users_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `logSurat`
--
ALTER TABLE `logSurat`
  MODIFY `logSurat_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `narapidana`
--
ALTER TABLE `narapidana`
  MODIFY `nara_id` int(55) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `permohonan`
--
ALTER TABLE `permohonan`
  MODIFY `permo_id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `resi`
--
ALTER TABLE `resi`
  MODIFY `resi_id` int(55) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `revisi`
--
ALTER TABLE `revisi`
  MODIFY `revisi_id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `users_id` int(55) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
